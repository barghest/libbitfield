// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//
/// @brief Testing BitField class
/// Notes: I apologize for all this #define in C++ programming, but Gtest is not
/// capable of providing a way to use both typed test and parameterized test.
/// So we have to do at least one of them by hand in order to fully test the class.
///

#include "unit_test_abitfield.hh"

/*
** Note: If you want to add gtest param test to ABitField_test_const_*, change
** "TEST_F" to "TEST_P" in GTEST_ABITFIELD_CONST_TEMPLATE_ and uncomment the second
** line in macro GTEST_INSTANTIATE_.
*/

/*
** TODO: convertir tout ce code avec google test
** TODO: Gerer les exceptions/overlap/flags/autre/sadique test/etc
** TODO: tester l'arithmetique native (sans unknown/undefined)
** TODO: tester tout les cas d'overlap
** TODO: splitter les test par class (Bitfield / ABItFIeld / BitFieldArithmetic
*/

# define GTEST_ABITFIELD_CONST_TEMPLATE_(class, fct, type)  \
  TEST_F(class##_const##_##type, fct)                       \
  {                                                         \
    EXPECT_NO_FATAL_FAILURE(this->fct());                   \
  }

# define GTEST_ABITFIELD_CONST_TEMPLATE_ALL_TYPE(class, fct)    \
  GTEST_ABITFIELD_CONST_TEMPLATE_(class, fct, int8_t)           \
  GTEST_ABITFIELD_CONST_TEMPLATE_(class, fct, int16_t)          \
  GTEST_ABITFIELD_CONST_TEMPLATE_(class, fct, int32_t)          \
  GTEST_ABITFIELD_CONST_TEMPLATE_(class, fct, int64_t)          \
  GTEST_ABITFIELD_CONST_TEMPLATE_(class, fct, uint8_t)          \
  GTEST_ABITFIELD_CONST_TEMPLATE_(class, fct, uint16_t)         \
  GTEST_ABITFIELD_CONST_TEMPLATE_(class, fct, uint32_t)         \
  GTEST_ABITFIELD_CONST_TEMPLATE_(class, fct, uint64_t)

/*!
** Generate gtest macro to combine all possibilites of policies in ABitField.
** --> templated type for ConstExprSize<T>
** and the one with variable.
*/
# define GTEST_(class, fct)                             \
  GTEST_ABITFIELD_CONST_TEMPLATE_ALL_TYPE(class, fct)   \
  TEST_P(class##_variable, fct)                         \
  {                                                     \
    EXPECT_NO_FATAL_FAILURE(this->fct());               \
  }

# define GTEST_INSTANTIATE_CONST_TYPE(class, values)                    \
  INSTANTIATE_TEST_CASE_P(class, class##_const##_##int8_t, values);     \
  INSTANTIATE_TEST_CASE_P(class, class##_const##_##int16_t, values);    \
  INSTANTIATE_TEST_CASE_P(class, class##_const##_##int32_t, values);    \
  INSTANTIATE_TEST_CASE_P(class, class##_const##_##int64_t, values);    \
  INSTANTIATE_TEST_CASE_P(class, class##_const##_##uint8_t, values);    \
  INSTANTIATE_TEST_CASE_P(class, class##_const##_##uint16_t, values);   \
  INSTANTIATE_TEST_CASE_P(class, class##_const##_##uint32_t, values);   \
  INSTANTIATE_TEST_CASE_P(class, class##_const##_##uint64_t, values);   \

# define GTEST_INSTANTIATE_(class, values)                              \
  INSTANTIATE_TEST_CASE_P(class, class##_variable, values);/*           \
                                                                        GTEST_INSTANTIATE_CONST_TYPE(class, values)*/

GTEST_(ABitField_test, testing_bitfield_resize)
GTEST_(ABitField_test, testing_bitState_merge)
GTEST_(ABitField_test, testing_set_get_is)
GTEST_(ABitField_test, testing_operator_prettyprint)
GTEST_(ABitField_test, testing_setrange)
GTEST_(ABitField_test, testing_copyrange)

GTEST_INSTANTIATE_(ABitField_test, ::testing::Range((size_t)0, (size_t)100))
