// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//

#include <iostream>    // std::cout // std::cerr
#include <cassert>    // TODO: to remove after full merge with google
#include <cstdlib>    // std::abs()
#include <limits>    // std::min // std::max
#include "unit_test_bitfield_arithmetic.hh"
#include "cCppNativeArithmeticWithEflags.hh"

template <typename T>
BitFieldArithmetic_test<T>::BitFieldArithmetic_test(unsigned int n)
  : _mem(n), _bfa(n)
{
  unsigned int    i;

  for (i = 0 ; i < this->_mem.size() ; i++)
    this->_bfa[i].setMem(this->_mem[i].data());
}

template <typename T>
BitFieldArithmetic_test<T>::~BitFieldArithmetic_test()
{
}

template <typename T>
template <typename U>
auto    BitFieldArithmetic_test<T>::_testing_operator_AS_S(std::function<uint16_t(T, T, T*)> native_op,
                                                           std::string const &op_name,
                                                           BitFieldArithmetic<T> &(BitFieldArithmetic<T>::*tested)(U),
                                                           T a, T b) -> void
{
  T        result;
  T        _T_var;

  SCOPED_TRACE(__PRETTY_FUNCTION__);
  {
    _bfa[0] = a;
    (_bfa[0].*tested)(b);
    _T_var = _bfa[0].toInt();
    native_op(a, b, &result);
    if (_T_var != result) // workaround gcc internal error
    {
      FAIL() << std::to_string(a) << " " << op_name << " " << std::to_string(b)
             << ": Expected: " << +result
             << " Actual: " << +_T_var;
    }
  }

  // std::cout << "Operator " << op_name << " OK" << std::endl;
  // // Returing a ::testing::AssertionResult and change "ASSERT_NO_FATAL_FAILURE"
  // // with EXPECT_TRUE will make gcc (4.7.2 -> 4.9.0) generate this error:
  // // ---
  // // main.cpp:xxx:y: internal compiler error: in make_decl_rtl, at varasm.c:1147
  // //    Please submit a full bug report,
  // //    with preprocessed source if appropriate.
  // //    See <http://bugzilla.redhat.com/bugzilla> for instructions.
  // //    Preprocessed source stored into /tmp/ccMEMlnK.out file, please attach this to your bugreport.
  // //    make: *** [main.o] Error 1
  // //    [test@localhost [20:37:52] 2007]
  // // ---
  // if (result == (T)(native_op(a, b)))
  //   return (::testing::AssertionFailure() << "TODO: a finir Testing failure");
  // return (::testing::AssertionSuccess());
  // assert(true);
}

template <typename T>
template <typename U>
auto    BitFieldArithmetic_test<T>::_testing_operator_AS_AS(std::function<uint16_t(T, T, T*)> native_op,
                                                            std::string const &op_name,
                                                            BitFieldArithmetic<T> &(BitFieldArithmetic<T>::*tested)(U),
                                                            T a, T b) -> void
{
}

template <typename T>
template <typename U>
auto    BitFieldArithmetic_test<T>::_testing_operator_AC_S(std::function<uint16_t(T, T, T*)> native_op,
                                                           std::string const &op_name,
                                                           BitFieldArithmetic<T> &(BitFieldArithmetic<T>::*tested)(U),
                                                           T a, T b) -> void
{
}

template <typename T>
template <typename U>
auto    BitFieldArithmetic_test<T>::_testing_operator_AC_AC(std::function<uint16_t(T, T, T*)> native_op,
                                                            std::string const &op_name,
                                                            BitFieldArithmetic<T> &(BitFieldArithmetic<T>::*tested)(U),
                                                            T a, T b) -> void
{
}

template <typename T>
template <typename U>
auto    BitFieldArithmetic_test<T>::_testing_operator_AS_S_eflags(std::function<uint16_t(T, T, T*)> native_op,
                                                                  std::string const &op_name,
                                                                  BitFieldArithmetic<T> &(BitFieldArithmetic<T>::*tested)(U, t_bitFieldEflags &),
                                                                  T a, T b) -> void
{
  T        result;
  T        _T_var;
  nativeType::t_eflags    eflags;
  t_bitFieldEflags    bitFieldEflags;
  auto        err_msg = [&](T exp, T act, std::string const &details) -> std::string
    {
      return (std::to_string(+a) + " " + op_name + " " + std::to_string(+b)
              + ": " + details + ". Expected: " + std::to_string(exp)
              + " Actual: " + std::to_string(act) + ".");
    };

  if (op_name == ">>" || op_name == "<<") // Remove error case from this test
    b = b & (sizeof(T)*8 - 1);
  eflags.word = native_op(a, b, &result);
  bitFieldEflags.clear();
  SCOPED_TRACE(__PRETTY_FUNCTION__);
  {
    _bfa[0] = a;
    (_bfa[0].*tested)(b, bitFieldEflags);
    _T_var = _bfa[0].toInt();
    if (_T_var != result) // workaround gcc internal error
      FAIL() << err_msg(result, _T_var, "Result error");
    // TODO: tester tout les cas d'undefined + tester plus en profondeur les shifts
    if (eflags.s_eflags.OF != bitFieldEflags.s_byte.OF && ((op_name != ">>" && op_name != "<<") || b <= 1))
      FAIL() << err_msg(eflags.s_eflags.OF, bitFieldEflags.s_byte.OF, "OF flag value error");
    if (eflags.s_eflags.CF != bitFieldEflags.s_byte.CF)
      FAIL() << err_msg(eflags.s_eflags.CF, bitFieldEflags.s_byte.CF, "CF flag value error");

    if (op_name != "*")
    {
      if (eflags.s_eflags.ZF != bitFieldEflags.s_byte.ZF)
        FAIL() << err_msg(eflags.s_eflags.ZF, bitFieldEflags.s_byte.ZF, "ZF flag value error");
      if (eflags.s_eflags.PF != bitFieldEflags.s_byte.PF)
        FAIL() << err_msg(eflags.s_eflags.PF, bitFieldEflags.s_byte.PF, "PF flag value error");
      if (eflags.s_eflags.SF != bitFieldEflags.s_byte.SF)
        FAIL() << err_msg(eflags.s_eflags.SF, bitFieldEflags.s_byte.SF, "SF flag value error");
      if (op_name != "&" && op_name != "|" && op_name != "^")
      {
        if (((op_name != ">>" && op_name != "<<") || b == 0) && eflags.s_eflags.AF != bitFieldEflags.s_byte.AF)
          FAIL() << err_msg(eflags.s_eflags.AF, bitFieldEflags.s_byte.AF, "AF flag value error");
      }
      else
        if (BitField::Undefined != bitFieldEflags.s_byte.AF)
          FAIL() << err_msg(BitField::Undefined, bitFieldEflags.s_byte.AF, "AF flag value error");
    }
    else // Operator mult "*"
    {
      if (BitField::Undefined != bitFieldEflags.s_byte.ZF)
        FAIL() << err_msg(BitField::Undefined, bitFieldEflags.s_byte.ZF, "ZF flag value error");
      if (BitField::Undefined != bitFieldEflags.s_byte.PF)
        FAIL() << err_msg(BitField::Undefined, bitFieldEflags.s_byte.PF, "PF flag value error");
      if (BitField::Undefined != bitFieldEflags.s_byte.SF)
        FAIL() << err_msg(BitField::Undefined, bitFieldEflags.s_byte.SF, "SF flag value error");
      if (BitField::Undefined != bitFieldEflags.s_byte.AF)
        FAIL() << err_msg(BitField::Undefined, bitFieldEflags.s_byte.AF, "AF flag value error");
    }
  }

  // std::cout << "Operator " << op_name << " OK" << std::endl;
  // // Returing a ::testing::AssertionResult and change "ASSERT_NO_FATAL_FAILURE"
  // // with EXPECT_TRUE will make gcc (4.7.2 -> 4.9.0) generate this error:
  // // ---
  // // main.cpp:xxx:y: internal compiler error: in make_decl_rtl, at varasm.c:1147
  // //    Please submit a full bug report,
  // //    with preprocessed source if appropriate.
  // //    See <http://bugzilla.redhat.com/bugzilla> for instructions.
  // //    Preprocessed source stored into /tmp/ccMEMlnK.out file, please attach this to your bugreport.
  // //    make: *** [main.o] Error 1
  // //    [test@localhost [20:37:52] 2007]
  // // ---
  // if (result == (T)(native_op(a, b)))
  //   return (::testing::AssertionFailure() << "TODO: a finir Testing failure");
  // return (::testing::AssertionSuccess());
  // assert(true);
}

template <typename T>
template <typename U>
auto    BitFieldArithmetic_test<T>::_testing_operator_AS_AS_eflags(std::function<uint16_t(T, T, T*)> native_op,
                                                                   std::string const &op_name,
                                                                   BitFieldArithmetic<T> &(BitFieldArithmetic<T>::*tested)(U, t_bitFieldEflags &),
                                                                   T a, T b) -> void
{
}

template <typename T>
template <typename U>
auto    BitFieldArithmetic_test<T>::_testing_operator_AC_S_eflags(std::function<uint16_t(T, T, T*)> native_op,
                                                                  std::string const &op_name,
                                                                  BitFieldArithmetic<T> &(BitFieldArithmetic<T>::*tested)(U, t_bitFieldEflags &),
                                                                  T a, T b) -> void
{
}

template <typename T>
template <typename U>
auto    BitFieldArithmetic_test<T>::_testing_operator_AC_AC_eflags(std::function<uint16_t(T, T, T*)> native_op,
                                                                   std::string const &op_name,
                                                                   BitFieldArithmetic<T> &(BitFieldArithmetic<T>::*tested)(U, t_bitFieldEflags &),
                                                                   T a, T b) -> void
{
}

template <typename T>
template <typename U>
auto    BitFieldArithmetic_test<T>::testing_operator(std::function<uint16_t(T, T, T*)> native_op,
                                                     std::string const &op_name,
                                                     BitFieldArithmetic<T> &(BitFieldArithmetic<T>::*tested)(U),
                                                     BitFieldArithmetic<T> &(BitFieldArithmetic<T>::*tested_eflags)(U, t_bitFieldEflags &),
                                                     T a, T b) -> void
{
  _testing_operator_AS_S(native_op, op_name, tested, a, b);
  // _testing_operator_AS_AS(native_op, op_name, tested, a, b);
  // _testing_operator_AC_S(native_op, op_name, tested, a, b);
  // _testing_operator_AC_AC(native_op, op_name, tested, a, b);

  _testing_operator_AS_S_eflags(native_op, op_name, tested_eflags, a, b);
  // _testing_operator_AS_AS_eflags(native_op, op_name, tested_eflags, a, b);
  // _testing_operator_AC_S_eflags(native_op, op_name, tested_eflags, a, b);
  // _testing_operator_AC_AC_eflags(native_op, op_name, tested_eflags, a, b);
}

template <typename T>
auto    BitFieldArithmetic_test<T>::testing_all_operator(T a, T b) -> void
{
  uint16_t    (*fct_and)(T, T, T*) = &nativeType::bitAnd<T>;
  uint16_t    (*fct_or)(T, T, T*) = &nativeType::bitOr<T>;
  uint16_t    (*fct_xor)(T, T, T*) = &nativeType::bitXor<T>;

  uint16_t    (*fct_lshift)(T, T, T*) = &nativeType::lshift<T>;
  uint16_t    (*fct_rshift)(T, T, T*) = &nativeType::rshift<T>;

  uint16_t    (*fct_plus)(T, T, T*) = &nativeType::add<T>;
  uint16_t    (*fct_minus)(T, T, T*) = &nativeType::sub<T>;
  uint16_t    (*fct_mult)(T, T, T*) = &nativeType::mul<T>;
  uint16_t    (*fct_div)(T, T, T*, T*) = &nativeType::div<T>;

  SCOPED_TRACE(__PRETTY_FUNCTION__);
  ASSERT_NO_FATAL_FAILURE((testing_operator<unsigned int>(fct_lshift, "<<",
                                                          &BitFieldArithmetic<T>::operator<<=,
                                                          &BitFieldArithmetic<T>::operatorLShiftEqual,
                                                          a, std::abs(b))));
  ASSERT_NO_FATAL_FAILURE((testing_operator<unsigned int>(fct_rshift, ">>",
                                                          &BitFieldArithmetic<T>::operator>>=,
                                                          &BitFieldArithmetic<T>::operatorRShiftEqual,
                                                          a, std::abs(b))));

  ASSERT_NO_FATAL_FAILURE(testing_operator<T>(fct_and, "&", &BitFieldArithmetic<T>::operator&=,
                                              &BitFieldArithmetic<T>::operatorAndEqual, a, b));
  ASSERT_NO_FATAL_FAILURE(testing_operator<T>(fct_or, "|", &BitFieldArithmetic<T>::operator|=,
                                              &BitFieldArithmetic<T>::operatorOrEqual, a, b));
  ASSERT_NO_FATAL_FAILURE(testing_operator<T>(fct_xor, "^", &BitFieldArithmetic<T>::operator^=,
                                              &BitFieldArithmetic<T>::operatorXorEqual, a, b));

  ASSERT_NO_FATAL_FAILURE(testing_operator<T>(fct_plus, "+", &BitFieldArithmetic<T>::operator+=,
                                              &BitFieldArithmetic<T>::operatorAddEqual, a, b));
  ASSERT_NO_FATAL_FAILURE(testing_operator<T>(fct_minus, "-", &BitFieldArithmetic<T>::operator-=,
                                              &BitFieldArithmetic<T>::operatorSubEqual, a, b));

  ASSERT_NO_FATAL_FAILURE(testing_operator<T>(fct_mult, "*", &BitFieldArithmetic<T>::operator*=,
                                              &BitFieldArithmetic<T>::operatorMultEqual, a, b));
  // TODO: Refaire les proto de divisions par le "high" et le modulo pour le reste
  // ASSERT_NO_FATAL_FAILURE(testing_operator<T>(fct_div, "/", &BitFieldArithmetic<T>::operator/=, a, (b ? : 1)));
}

template <typename T>
auto    BitFieldArithmetic_test<T>::testing_operator_limits() -> void
{
  unsigned int        e;
  unsigned int        nb_it;
  T            nb[2*4];

  nb_it = 0;
  SCOPED_TRACE(__PRETTY_FUNCTION__);
  // std::cout << +rand_nb[0] << ":" << +rand_nb[1] << std::endl;
  /* Testing with limits */
  nb[nb_it++] = std::numeric_limits<T>::max();
  nb[nb_it++] = std::numeric_limits<T>::max();

  nb[nb_it++] = std::numeric_limits<T>::max();
  nb[nb_it++] = std::numeric_limits<T>::min();

  nb[nb_it++] = std::numeric_limits<T>::min();
  nb[nb_it++] = std::numeric_limits<T>::max();

  nb[nb_it++] = std::numeric_limits<T>::min();
  nb[nb_it++] = std::numeric_limits<T>::min();

  for (e = 0 ; e < (sizeof(nb) / sizeof(*nb)) ; e += 2)
  {
    ASSERT_NO_FATAL_FAILURE(testing_all_operator(nb[e], nb[e + 1]));
  }
}

template <typename T>
auto    BitFieldArithmetic_test<T>::testing_scalar_arithmetic() -> void
{
  unsigned int  i;
  unsigned int  e;
  unsigned int  nb_it;
  T             nb[2*5];
  T             rand_nb[2];

  // Fixed seed, always generate the same suite of number. Ok for unit test.
  // For fuzzing see the fuzz test.

  SCOPED_TRACE(__PRETTY_FUNCTION__);
  for (i = 0; i < 1000; i++)
  {
    nb_it = 0;
    rand_nb[0] = _gen_rand_nb(); /* Can be positive and negative */
    rand_nb[1] = _gen_rand_nb(); /* ---------------------------- */
    // std::cout << +rand_nb[0] << ":" << +rand_nb[1] << std::endl;

    /* Testing limits with a random number */
    nb[nb_it++] = std::numeric_limits<T>::max();
    nb[nb_it++] = rand_nb[0];

    nb[nb_it++] = std::numeric_limits<T>::min();
    nb[nb_it++] = rand_nb[0];

    nb[nb_it++] = rand_nb[0];
    nb[nb_it++] = std::numeric_limits<T>::max();

    nb[nb_it++] = rand_nb[0];
    nb[nb_it++] = std::numeric_limits<T>::min();

    nb[nb_it++] = rand_nb[0];
    nb[nb_it++] = rand_nb[1];

    e = 0;
    while (e < (sizeof(nb) / sizeof(*nb)))
    {
      testing_all_operator(nb[e], nb[e + 1]);
      e = e + 2;
    }
  }
}

template <typename T>
auto    BitFieldArithmetic_test<T>::testing_sign_bit() -> void
{
  unsigned int  i;
  T             _T_var;

  SCOPED_TRACE(__PRETTY_FUNCTION__);
  for (i = 0; i < 10000; i++)
  {
    _T_var = _gen_rand_nb() % (std::numeric_limits<T>::max());
    _bfa[0] = _T_var;
    ASSERT_EQ(_bfa[0].getSignBitPos(), (T)((sizeof(T) * 8) - 1));
    if ((_T_var >> (sizeof(T) * 8 - 1)) & 1)
      ASSERT_EQ(_bfa[0].getSignBit(), (T)1);
    else
      ASSERT_EQ(_bfa[0].getSignBit(), (T)0);
  }
}

template <typename T>
auto    BitFieldArithmetic_test<T>::testing_comparaison() -> void
{
  SCOPED_TRACE(__PRETTY_FUNCTION__);
  _bfa[0] = std::numeric_limits<T>::min();
  _bfa[1] = std::numeric_limits<T>::min();

  ASSERT_EQ((_bfa[1] > _bfa[0]), (T)BitField::Zero);
  ASSERT_EQ((_bfa[1] >= _bfa[0]), (T)BitField::One);
  ASSERT_EQ((_bfa[1] < _bfa[0]), (T)BitField::Zero);
  ASSERT_EQ((_bfa[1] <= _bfa[0]), (T)BitField::One);
  _bfa[1] += (T)1;

  ASSERT_EQ((_bfa[1] > _bfa[0]), (T)BitField::One);
  ASSERT_EQ((_bfa[1] >= _bfa[0]), (T)BitField::One);
  ASSERT_EQ((_bfa[1] < _bfa[0]), (T)BitField::Zero);
  // std::cout << (b <= a) << std::endl;
  ASSERT_EQ((_bfa[1] <= _bfa[0]), (T)BitField::Zero);
  _bfa[1] = (T)1;
  ASSERT_TRUE((_bfa[1] > _bfa[0]) == BitField::One && (_bfa[0] > _bfa[1]) == BitField::Zero
              && (_bfa[1] >= _bfa[0]) == BitField::One && (_bfa[0] >= _bfa[1]) == BitField::Zero);
  _bfa[1] = std::numeric_limits<T>::max();
  ASSERT_TRUE((_bfa[1] > _bfa[0]) == BitField::One && (_bfa[0] < _bfa[1]) == BitField::One
              && (_bfa[1] < _bfa[0]) == BitField::Zero && (_bfa[1] <= _bfa[0]) == BitField::Zero);
}

template <typename T>
auto    BitFieldArithmetic_test<T>::testing_overlap() -> void
{
  unsigned int                  i;
  char                          var[128];
  BitFieldArithmetic<uint32_t>  a;
  BitFieldArithmetic<uint32_t>  b;

  SCOPED_TRACE(__PRETTY_FUNCTION__);
  a.setMem(&var[0]);
  for (i = 0 ; i < (sizeof(var) - (sizeof(uint32_t) * 2)) ; i++)
  {
    b.setMem(&var[i]);
    ASSERT_EQ(overlap(a, b), (T)(i < (sizeof(uint32_t) * 2)));
    ASSERT_EQ(overlap(b, a), (T)(i < (sizeof(uint32_t) * 2)));
  }
}


template <typename T>
auto    BitFieldArithmetic_test<T>::testing_overlap_arithmetic() -> void
{
  unsigned int            i;
  // T                mem[4];
  BitFieldArithmetic<T>        a;
  BitFieldArithmetic<T>        b;

  SCOPED_TRACE(__PRETTY_FUNCTION__);
  for (i = 0; i < 10000; i++)
  {
// TODO: completer avec les valeurs en dur et les unknown/undefined
    // cse = random() % a.emulatedSize();
    // a.set(cse, value);
    // ASSERT_EQ(a.get(cse), (T)value && a.is(cse, value));
  }
}

template <typename T>
auto    BitFieldArithmetic_test<T>::testing_packbits() -> void
{
  unsigned int  i;
  T             nb;

  SCOPED_TRACE(__PRETTY_FUNCTION__);
  nb = std::numeric_limits<T>::max();
  _bfa[0] = nb;
  ASSERT_EQ(_bfa[0].toInt(), (T)nb);
  nb = std::numeric_limits<T>::min();
  _bfa[0] = nb;
  ASSERT_EQ(_bfa[0].toInt(), (T)nb);
  for (i = 0; i < 10000; i++)
  {
    _bfa[0] = nb = _gen_rand_nb();
    ASSERT_EQ(_bfa[0].toInt(), (T)nb);
  }
}

template <typename T>
auto    BitFieldArithmetic_test<T>::testing_complements() -> void
{
  unsigned int  i;
  T             nb;

  SCOPED_TRACE(__PRETTY_FUNCTION__);
  for (i = 0; i < 10000; i++)
  {
    nb = _gen_rand_nb();
    _bfa[0] = nb;
    _bfa[0].applyOneComplement();
    ASSERT_EQ(_bfa[0].toInt(), ((T)~nb));

    _bfa[0] = nb;
    _bfa[0].applyTwoComplement();
    ASSERT_EQ(_bfa[0].toInt(), (T)((~nb) + 1));
  }
}

template class BitFieldArithmetic_test <uint8_t>;
template class BitFieldArithmetic_test <uint16_t>;
template class BitFieldArithmetic_test <uint32_t>;

template class BitFieldArithmetic_test <int8_t>;
template class BitFieldArithmetic_test <int16_t>;
template class BitFieldArithmetic_test <int32_t>;

#if defined(__x86_64__) || defined(__ia64__) || defined(_WIN64)
template class BitFieldArithmetic_test <uint64_t>;
template class BitFieldArithmetic_test <int64_t>;
#endif /* !64b */
