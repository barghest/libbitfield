// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//

#include <iostream> // std::cout // std::cerr
#include <cassert>  // TODO: to remove after full merge with google
#include <cstdlib>  // std::abs()
#include <limits>   // std::min // std::max
#include "unit_test_abitfield.hh"

/*
** TODO: faire des tests unitaire pour BITFIELD
*/
template <typename T>
auto    ABitField_test<T>::testing_bitfield_resize() -> void
{
  size_t    i;
  size_t    e;
  size_t    tmp;
  BitField::BitFieldAlloc    a(50);

  SCOPED_TRACE(__PRETTY_FUNCTION__);
  for (i = 0; i < a.emulatedSize() ; i++)
    ASSERT_TRUE(a.is(i, BitField::Undefined));

  for (i = 0; i < 100 ; ++i)
  {
    tmp = _gen_rand_nb() % 1000;
    a.resize(tmp);
    ASSERT_TRUE(a.size() == tmp*2 && a.emulatedSize() == tmp);
    for (e = 0; e < a.emulatedSize() ; e++)
      ASSERT_TRUE(a.is(e, BitField::Undefined));
  }


  a.set(0, BitField::Zero);
  a.set(1, BitField::One);
  a.set(2, BitField::Unknown);
  for (i = 0; i < 10000 ; ++i)
  {
    tmp = (_gen_rand_nb() % 100) + 3;
    a.resize(tmp);
    ASSERT_TRUE(a.size() == tmp*2 && a.emulatedSize() == tmp);
    ASSERT_TRUE(a.isZero(0) && a.isOne(1) && a.isUnknown(2));
    for (e = 3; e < a.emulatedSize() ; e++)
      ASSERT_TRUE(a.is(e, BitField::Undefined));
  }

  a.setAllToZero();
  for (i = 0; i < a.emulatedSize() ; i++)
    ASSERT_TRUE(a.is(i, BitField::Zero));
  tmp = a.emulatedSize();
  a.resize(tmp + 1);
  ASSERT_TRUE(a.isZero((8 * tmp) - 1)
              && a.isUndefined(8 * tmp) /* Is immediate next byte Undefined ? */
              && a.isUndefined((8 * (tmp + 1)) - 1)); /* Testing last byte */
  // std::cout << a << std::endl;
}

template <typename T>
auto    ABitField_test<T>::testing_set_get_is() -> void
{
  unsigned int          i;
  BitField::e_bitState  value;

  std::cout << "_abf size ==> " << _abf.size() << std::endl;
  SCOPED_TRACE(__PRETTY_FUNCTION__);
  for (i = 0; i < this->_abf.emulatedSize(); i++)
  {
    value = (BitField::e_bitState)(_gen_rand_nb() % 4);
    this->_abf.set(i, value);
    ASSERT_EQ(this->_abf.get(i), value);
    ASSERT_TRUE(this->_abf.is(i, value));
  }
}

/*
** TODO: voir comment tester ce truc
*/
template <typename T>
auto    ABitField_test<T>::testing_operator_prettyprint() -> void
{
  BitField::BitFieldAlloc    a(16);

  SCOPED_TRACE(__PRETTY_FUNCTION__);
  a.setAllTo(BitField::Unknown);
  a.setAllToZero();
  a.set(15, BitField::Zero);
  a.set(16, BitField::One);
  a.set(17, BitField::Undefined);
  a.set(18, BitField::Zero);

  a.set(0, BitField::Zero);
  a.set(1, BitField::One);
  a.set(2, BitField::Unknown);
  a.set(3, BitField::Undefined);

  a.set(4, BitField::One);
  a.set(5, BitField::Zero);
  a.set(6, BitField::Unknown);
  a.set(7, BitField::One);

  a.set(8, BitField::One);
  a.set(9, BitField::One);
  a.set(10, BitField::One);
  a.set(11, BitField::One);

  std::cout << a << std::endl;
}

template <typename T>
auto    ABitField_test<T>::testing_setrange() -> void
{
  int        i;

  SCOPED_TRACE(__PRETTY_FUNCTION__);
  i = _abf.emulatedSize() - 2;
  if (i >= 0) // Need at least 2 byte
  {
    _abf.setAllToUndefined();
    _abf.setRangeTo(1, 14 + i*8, BitField::One);
    _abf.setRangeTo(2, 12 + i*8, BitField::Zero);
    _abf.setRangeTo(3, 10 + i*8, BitField::Unknown);
    _abf.setRangeTo(4, 8 + i*8, BitField::Undefined);
    _abf.setRangeTo(5, 6 + i*8, BitField::Zero);
    _abf.setRangeTo(6, 4 + i*8, BitField::One);
    _abf.setRangeTo(7, 2 + i*8, BitField::Unknown);
    /* 0x0000:  U10?-U01? [?]{8}* ?10U-?01U */
    // std::cout << a << std::endl;
    ASSERT_TRUE(_abf.isUndefined(0) && _abf.isUndefined(15 + i*8)
                && _abf.isOne(1) && _abf.isOne(14 + i*8)
                && _abf.isZero(2) && _abf.isZero(13 + i*8)
                && _abf.isUnknown(3) && _abf.isUnknown(12 + i*8)
                && _abf.isUndefined(4) && _abf.isUndefined(11 + i*8)
                && _abf.isZero(5) && _abf.isZero(10 + i*8)
                && _abf.isOne(6) && _abf.isOne(9 + i*8)
                && _abf.isUnknown(7) && _abf.isUnknown(8 + i*8));
    // }
  }
}

/*
** TODO: tester que les bit sont bien inchange dans le cas d'un out of range
*/

template <typename T>
auto    ABitField_test<T>::testing_copyrange() -> void
{
  int        i;
  int        e;
  BitField::BitFieldAlloc    a(2);
  BitField::BitFieldAlloc    b(2);
  std::string    str("01?U01?U01?U01?U01?U01?U");
  std::string    str1;

  SCOPED_TRACE(__PRETTY_FUNCTION__);
  i = 0;
  a.setAllToOne();
  for (i = 0 ; i < 16 ; i = i + 4)
  {
    a.set(i, BitField::Zero);
    a.set(i + 1, BitField::One);
    a.set(i + 2, BitField::Unknown);
    a.set(i + 3, BitField::Undefined);
  }
  for (i = 0 ; i < 16 ; ++i)
  {
    for (e = 0 ; e < (16 - i) ; e++)
    {
      b.setAllToOne();
      b.copyRange(a.getStructMem(), i, i, e);
      str1 = b.prettyPrintMemory(false, false, 0, false, false);
      ASSERT_TRUE(strncmp(&str1.c_str()[i], &str.c_str()[i], e) == 0);
    }
  }
}

template <typename T>
auto    ABitField_test<T>::testing_bitState_merge(void) -> void
{
  SCOPED_TRACE(__PRETTY_FUNCTION__);
  EXPECT_TRUE(BitField::merge(BitField::Zero, BitField::Zero) == BitField::Zero);
  EXPECT_TRUE(BitField::merge(BitField::Zero, BitField::One) == BitField::Unknown);
  EXPECT_TRUE(BitField::merge(BitField::Zero, BitField::Unknown) == BitField::Unknown);
  EXPECT_TRUE(BitField::merge(BitField::Zero, BitField::Undefined) == BitField::Undefined);

  EXPECT_TRUE(BitField::merge(BitField::One, BitField::Zero) == BitField::Unknown);
  EXPECT_TRUE(BitField::merge(BitField::One, BitField::One) == BitField::One);
  EXPECT_TRUE(BitField::merge(BitField::One, BitField::Unknown) == BitField::Unknown);
  EXPECT_TRUE(BitField::merge(BitField::One, BitField::Undefined) == BitField::Undefined);

  EXPECT_TRUE(BitField::merge(BitField::Unknown, BitField::Zero) == BitField::Unknown);
  EXPECT_TRUE(BitField::merge(BitField::Unknown, BitField::One) == BitField::Unknown);
  EXPECT_TRUE(BitField::merge(BitField::Unknown, BitField::Unknown) == BitField::Unknown);
  EXPECT_TRUE(BitField::merge(BitField::Unknown, BitField::Undefined) == BitField::Undefined);

  EXPECT_TRUE(BitField::merge(BitField::Undefined, BitField::Zero) == BitField::Undefined);
  EXPECT_TRUE(BitField::merge(BitField::Undefined, BitField::One) == BitField::Undefined);
  EXPECT_TRUE(BitField::merge(BitField::Undefined, BitField::Unknown) == BitField::Undefined);
  EXPECT_TRUE(BitField::merge(BitField::Undefined, BitField::Undefined) == BitField::Undefined);
}

template class ABitField_test <BitField::Policies::VariableSize>;
template class ABitField_test <BitField::Policies::ConstExprSize<int8_t> >;
template class ABitField_test <BitField::Policies::ConstExprSize<int16_t> >;
template class ABitField_test <BitField::Policies::ConstExprSize<int32_t> >;
template class ABitField_test <BitField::Policies::ConstExprSize<uint8_t> >;
template class ABitField_test <BitField::Policies::ConstExprSize<uint16_t> >;
template class ABitField_test <BitField::Policies::ConstExprSize<uint32_t> >;
# if defined(__x86_64__) || defined(__ia64__) || defined(_WIN64)
template class ABitField_test <BitField::Policies::ConstExprSize<int64_t> >;
template class ABitField_test <BitField::Policies::ConstExprSize<uint64_t> >;
# endif /* !64b */
