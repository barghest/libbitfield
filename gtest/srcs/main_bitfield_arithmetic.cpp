// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//
/// @brief Testing BitField class
///

#include "unit_test_bitfield_arithmetic.hh"

/*
** TODO: convertir tout ce code avec google test
** TODO: Gerer les exceptions/overlap/flags/autre/sadique test/etc
** TODO: tester l'arithmetique native (sans unknown/undefined)
** TODO: tester tout les cas d'overlap
** TODO: splitter les test par class (Bitfield / ABItFIeld / BitFieldArithmetic
*/

typedef testing::Types<uint8_t, int8_t, uint16_t, int16_t,
                       uint32_t, int32_t, uint64_t, int64_t>    Implementations;
// typedef testing::Types<uint8_t, int8_t>    Implementations;

# define GTEST_(class, fct) TYPED_TEST(class, fct)  \
  {                                                 \
    EXPECT_NO_FATAL_FAILURE(this->fct());           \
  }

TYPED_TEST_CASE(BitFieldArithmetic_test, Implementations);

GTEST_(BitFieldArithmetic_test, testing_operator_limits)

GTEST_(BitFieldArithmetic_test, testing_scalar_arithmetic)
GTEST_(BitFieldArithmetic_test, testing_sign_bit)
GTEST_(BitFieldArithmetic_test, testing_comparaison)
GTEST_(BitFieldArithmetic_test, testing_overlap_arithmetic)

GTEST_(BitFieldArithmetic_test, testing_packbits)
GTEST_(BitFieldArithmetic_test, testing_complements)
