// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//
/// @brief Header file for the BitFieldArithmetic class
///

#ifndef UNIT_TEST_BITFIELD_ARITHMETIC_HH_
# define UNIT_TEST_BITFIELD_ARITHMETIC_HH_

# include <array>
# include <random>          // mersenne twister random algorithm
# include <vector>          // std::vector
# include <gtest/gtest.h>   // google test
# include <functional>      // std::function / std::plus / std::minus / etc
# include "BitFieldAlloc.hh"
# include "BitFieldArithmetic.hh"

/*!
** @param S The number of allocated BitFieldArithmetic to use.
** @warning With google test it is not possible to template with more than 1 parameters.
*/
template <typename T>
class BitFieldArithmetic_test : public testing::Test
{
private:
  std::vector<std::array<T, 2> >    _mem;
  std::vector<BitFieldArithmetic<T> >    _bfa;
  std::mt19937_64            _gen_rand_nb;

# ifdef TESTING_OPERATOR__
#  error "TESTING_OPERATOR__ is already defined."
# endif
# define TESTING_OPERATOR__(fct_name)                                   \
  template <typename U>                                                 \
  auto    fct_name(std::function<uint16_t(T, T, T*)> native,            \
                   std::string const &op_name,                          \
                   BitFieldArithmetic<T> &(BitFieldArithmetic<T>::*tested)(U), \
                   T a, T b) -> void;                                   \
  template <typename U>                                                 \
  auto    fct_name##_eflags(std::function<uint16_t(T, T, T*)> native,   \
                            std::string const &op_name,                 \
                            BitFieldArithmetic<T> &(BitFieldArithmetic<T>::*tested)(U, t_bitFieldEflags &), \
                            T a, T b) -> void

  TESTING_OPERATOR__(_testing_operator_AS_S);
  TESTING_OPERATOR__(_testing_operator_AS_AS);
  TESTING_OPERATOR__(_testing_operator_AC_S);
  TESTING_OPERATOR__(_testing_operator_AC_AC);

# undef TESTING_OPERATOR__

  /*
  ** Call function 'native' (with pretty print name 'op_name') with 'a' and 'b',
  ** and compare the result with the corresponding function in BitFieldArithmetic<T>.
  ** An assertion is performed if there are not equal.
  */
  template <typename U>
  auto    testing_operator(std::function<uint16_t(T, T, T*)> native,
                           std::string const &op_name,
                           BitFieldArithmetic<T> &(BitFieldArithmetic<T>::*)(U),
                           BitFieldArithmetic<T> &(BitFieldArithmetic<T>::*)(U, t_bitFieldEflags &),
                           T a, T b) -> void;

  /*!
  ** Test all the arithmetic operation between A and B.
  ** (a + b, a - b, a * b, a >> b, ...)
  */
  auto    testing_all_operator(T a, T b) -> void;

protected:
  /*!
  ** @param n Number of BitFieldArithmetic to reserve
  */
  BitFieldArithmetic_test(unsigned int n = 10);
  ~BitFieldArithmetic_test();

  auto    testing_operator_limits(void) -> void;
  auto    testing_scalar_arithmetic(void) -> void;
  auto    testing_sign_bit(void) -> void;
  auto    testing_comparaison(void) -> void;

  /*!
  ** Test the overlap() method of BitFieldArithmetic_test<T>.
  */
  auto    testing_overlap(void) -> void;

  /*!
  ** Test scalar arithmetic with 2 BitFieldArithmetic_test<T> sharing memories.
  ** This is done in order to ensure that expression likes "var += var;" is working.
  */
  auto    testing_overlap_arithmetic(void) -> void;

  /*!
  ** Test Emulated -> native type conversion
  */
  auto    testing_packbits(void) -> void;
  /*!
  ** Testing one and two binary bits complement
  */
  auto    testing_complements(void) -> void;

}; // ! class BitFieldArithmetic_test

#endif /* ! UNIT_TEST_BITFIELD_ARITHMETIC_HH_ */
