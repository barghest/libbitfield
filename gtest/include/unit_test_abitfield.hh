// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//

#ifndef UNIT_TEST_ABITFIELD_HH_
# define UNIT_TEST_ABITFIELD_HH_

// # include <array>    // std::array
# include <random>    // mersenne twister random algorithm
# include <vector>    // std::vector
# include <gtest/gtest.h> // google test
// # include <functional>  // std::function / std::plus / std::minus / etc
# include "ABitField.hh"
# include "BitFieldAlloc.hh"

template <typename T>
class ABitField_test
{
protected:
  std::vector<uint8_t>   _mem;
  std::mt19937_64        _gen_rand_nb;
  /* _abf here does not matter as child class will use there own */
  ABitField<T>    _abf;
  ABitField_test()  {};
  ~ABitField_test() {};
public:
  /*!
  ** @param n Number of BitFieldArithmetic to reserve
  */

  auto    testing_bitfield_resize(void) -> void;
  auto    testing_bitState_merge(void) -> void;

  auto    testing_set_get_is(void) -> void;
  auto    testing_operator_prettyprint(void) -> void;
  auto    testing_setrange(void) -> void;
  auto    testing_copyrange(void) -> void;

}; // ! class ABitField_test

# define ABITFIELD_TEST_CONST_(type)                                    \
                                                                        \
  class ABitField_test_const_##type : public ABitField_test<BitField::Policies::ConstExprSize<type> >, \
                                      public testing::Test              \
  {                                                                     \
    protected:                                                          \
    ABitField_test_const_##type()                                       \
    {                                                                   \
      _mem.resize(sizeof(type) * 2);                                    \
      _abf.setMem(_mem.data());                                         \
      _abf.setAllToZero();                                              \
    }                                                                   \
  };

ABITFIELD_TEST_CONST_(int8_t)
ABITFIELD_TEST_CONST_(int16_t)
ABITFIELD_TEST_CONST_(int32_t)
ABITFIELD_TEST_CONST_(int64_t)
ABITFIELD_TEST_CONST_(uint8_t)
ABITFIELD_TEST_CONST_(uint16_t)
ABITFIELD_TEST_CONST_(uint32_t)
ABITFIELD_TEST_CONST_(uint64_t)

// TODO: crash test if size is < 0 or too big
class ABitField_test_variable : public ABitField_test<BitField::Policies::VariableSize>,
                                public testing::Test,
                                public testing::WithParamInterface<size_t>
{
protected:
  ABitField_test_variable()
  // : _abf(GetParam()) // Note: use this when testing bitField constructor
  {
    _mem.resize(GetParam());
    _abf.setMem(_mem.data());
    _abf.setSize(GetParam());
    _abf.setAllToZero();
  }
};

#endif /* ! UNIT_TEST_ABITFIELD_HH_ */
