# BitField

## What is bitField

BitField provides a (per bit) representation of values stored in memory, and
operations to apply on it (+, -, *, /, %, <<, >>, &, |).

The representation uses 4 states to represent a single bit:

* [0] -> Zero
    * This bit is always equal to zero.
* [1] -> One
    * This bit is always equal to one.
* [?] -> Unknown
    * The value of this bit is unknown, but it has been set by the user.
* [U] -> Undefined
    * Same as above, but it is not set at all (undefined)

Practical example:


```
#!c

int main()
{
    char    c; /* Here c is represented like this => [uuuu, uuuu] */

    /* c => [uuuu, uuuu] (Fully undefined) */
    c <<= 6;
    /* c => [uu00, 0000] (Partially undefined) */
    c = random() % 4;
    /* c => [0000, 00??] (Defined, but with multiple values) */
    c <<= 2;
    /* c => [0000, ??00] (---------------------------------) */
    c = c + 1;
    /* c => [0000, ??01] (---------------------------------) */
    return (c);
}

```

## Context

BitField is developed as part of the Barghest project (barghest.org).

## Features in development/incomplete
* The division/modulo operation
* The extended right shift
* More unit test on composite arithmetic