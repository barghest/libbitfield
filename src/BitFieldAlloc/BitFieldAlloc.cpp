// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//

#include <cstring>
#include <cassert>
#include <iostream>
#include <iomanip>
#include <type_traits>
#include "bitField/BitFieldAlloc/BitFieldAlloc.hh"

namespace bitField
{
  namespace BitFieldAlloc
  {

    BitFieldAlloc::BitFieldAlloc(size_t size)
    {
      this->_memory = nullptr;
      this->setSize(0);
      this->resize(size);
    }

    BitFieldAlloc::BitFieldAlloc(BitFieldAlloc const &other)
    {
      this->_memory = nullptr;
      this->setSize(0);
      (void)this->operator=(other);
    }


    auto    BitFieldAlloc::operator=(BitFieldAlloc const &other) -> BitFieldAlloc &
    {
      if (other.size())
      {
        if (this->size() != other.size())
          this->resize(other.emulatedSize());
        std::memcpy(this->getMem(), other.getMem(), this->size());
      }
      else
      {
        this->setSize(0);
        if (this->_memory)
          delete [] this->_memory;
        this->_memory = nullptr;
      }
      return (*this);
    }

    BitFieldAlloc::~BitFieldAlloc()
    {
      this->setSize(0);
      if (this->_memory)
        delete [] this->_memory;
    }

/******************************/

// auto    BitFieldAlloc::emulatedSize() const -> uint32_t {return (this->size() / 2);}
// auto    BitFieldAlloc::size() const -> uint32_t {return (this->_real_size);}

/******************************/

    auto    BitFieldAlloc::resize(size_t emulated_size) -> void
    {
      size_t                    min;
      bitField::t_byte          value;
      size_t                    new_real_size;
      decltype(this->_memory)   new_memory;

      if (emulated_size)
      {
        min = 0;
        new_real_size = emulated_size * 2;
        value.s_byte.b0 = value.s_byte.b1 = value.s_byte.b2 = value.s_byte.b3 = bitField::Undefined;
        new_memory = new std::remove_pointer<decltype(this->_memory)>::type [new_real_size];
        if (this->_memory)
        {
          min = (new_real_size < this->size() ?
                 new_real_size : this->size());
          std::memcpy(new_memory, this->_memory, min);
          delete [] this->_memory;
        }
        std::memset(&new_memory[min], value.byte, new_real_size - min);
      }
      else
      {
        new_real_size = 0;
        if (this->_memory)
          delete [] this->_memory;
        new_memory = nullptr;
      }
      this->setSize(new_real_size);
      this->_memory = new_memory;
    }

/******************************/
  } // !namespace BitFieldAlloc
} //! namespace bitField

/*
** Pretty print the memory hosted by BitFieldAlloc.
*/
std::ostream   &operator<<(std::ostream &a,
                           bitField::BitFieldAlloc::BitFieldAlloc const &other)
{
  // Use little for memory printing (easier to read)
  return (other.prettyPrintMemory(a, false, false, true, true));
}
