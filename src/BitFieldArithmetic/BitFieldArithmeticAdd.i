// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//

#ifndef BITFIELD_ARITHMETIC_PART_HH_
# error "Only <BitFieldArithmetic.hh> can be included directly."
#endif /* !BITFIELD_ARITHMETIC_PART_HH_*/

/*!
** TODO: optimisation: possibilité de faire un table de verité sur plusieurs bit ? (vectorielle)
** Truth table for
** the + operator (OP1 and OP2)
** -------------------
** + | 0 | 1 | ? | U |    0 ==> Zero
** -------------------    1 ==> One
**   |   |   |   |   |    ? ==> Unknown
** 0 | 0 | 1 | ? | U |    U ==> Undefined
** -------------------
**   |///|  1|  ?|  u| (carry)
** 1 |///| 0 | ? | U |
** -------------------
**   |///|///|  ?|  u| (carry)
** ? |///|///| ? | U |
** -------------------
**   |///|///|///|  u| (carry)
** U |///|///|///| U |
** -------------------
**
** Formula: (src + dst) + carry
** OP2 is simply ignoring the carry (as (1)+1 case is impossible)
** this prevent case like ((U)+U) or ((?)+U) where this would
** generate an extra carry and make the result invalid.
*/

static bitField::e_bitState const _addTruthTable[][2] =
{
  /* {add result value, carry} */
  {bitField::Zero, bitField::Zero}, /* 00 .. */
  {bitField::One, bitField::Zero},
  {bitField::Unknown, bitField::Zero},
  {bitField::Undefined, bitField::Zero},

  {bitField::One, bitField::Zero}, /* 01 .. */
  {bitField::Zero, bitField::One},
  {bitField::Unknown, bitField::Unknown},
  {bitField::Undefined, bitField::Undefined},

  {bitField::Unknown, bitField::Zero}, /* 10 .. */
  {bitField::Unknown, bitField::Unknown},
  {bitField::Unknown, bitField::Unknown},
  {bitField::Undefined, bitField::Undefined},

  {bitField::Undefined, bitField::Zero}, /* 11 .. */
  {bitField::Undefined, bitField::Undefined},
  {bitField::Undefined, bitField::Undefined},
  {bitField::Undefined, bitField::Undefined},
};

template <typename T>
auto BitFieldArithmetic<T>::_getAddVal(bitField::e_bitState op1,
                                       bitField::e_bitState op2) const -> bitField::e_bitState
{
  return (_addTruthTable[(op1 << 2) | op2][0]);
}

template <typename T>
auto BitFieldArithmetic<T>::_getAddCarry(bitField::e_bitState op1,
                                         bitField::e_bitState op2) const -> bitField::e_bitState
{
  return (_addTruthTable[(op1 << 2) | op2][1]);
}

/*!
** Below is an ascii art representation of how an addition
** between 2 bit is done. (using a 4 state bit).
**
** (short version) bit(n) = bit1(n) + bit2(n) + carry(n - 1)
** (complete version)
**
** dst bit -\        /--> Result1 --\        /--> Result2
**           >|<OP1>|    OldCarry --->|<OP2>|
** src bit -/        \                       \--> Carry2
**                    \--> Carry1                   /
**                           \------->|<OP3>|<-----/
**                                       |
**                         Definitive carry (New OldCarry)
**
** 1/ OP1 = (dst bit + src bit)
** 2/ (dst bit) = OP2 = (Result1 + OldCarry)
** 3/ (OldCarry) = OP3 = (Carry1 + Carry2)
*/
template <typename T>
BitFieldArithmetic<T> &BitFieldArithmetic<T>::operator+=(BitFieldArithmetic<T> const &src)
{
  t_bitFieldEflags    flags;

  flags.word = 0;
  return (operatorAddEqual(src, flags));
}

template <typename T>
BitFieldArithmetic<T> &BitFieldArithmetic<T>::operator+=(T val)
{
  t_bitFieldEflags    flags;

  flags.word = 0;
  return (operatorAddEqual(val, flags));
}

/*****************************/

/*
** TODO: Faire des tests sur les flags et les resultats plus pousse.
*/

template <typename T>
BitFieldArithmetic<T> &BitFieldArithmetic<T>::operatorAddEqual(BitFieldArithmetic<T> const &src,
                                                               t_bitFieldEflags &flags)
{
  return (this->_manageOverlap(src, flags, &BitFieldArithmetic<T>::_operatorAddEqual));
}

template <typename T>
BitFieldArithmetic<T> &BitFieldArithmetic<T>::operatorAddEqual(T val, t_bitFieldEflags &flags)
{
  bitfield_t<T>            _tmp_mem;
  BitFieldArithmetic<T>        _bitFieldVal(&_tmp_mem.mem[0]);

  _bitFieldVal = val;
  return (this->operatorAddEqual(_bitFieldVal, flags));
}

//     ADDITION SIGN BITS (for Overflow/Underflow)
//  num1sign num2sign sumsign
// ---------------------------
//         0 0 0
//  *OVER* 0 0 1 (adding two positives should be positive)
//         0 1 0
//         0 1 1
//         1 0 0
//         1 0 1
//  *OVER* 1 1 0 (adding two negatives should be negative)
//         1 1 1

/*
** Flags Affected
** The OF, SF, ZF, AF, CF, and PF flags are set according to the result.
*/
template <typename T>
BitFieldArithmetic<T> &BitFieldArithmetic<T>::_operatorAddEqual(BitFieldArithmetic<T> const &src,
                                                                t_bitFieldEflags &flags)
{
  unsigned int          i;
  bitField::e_bitState  oldCarry;
  bitField::e_bitState  carry1;
  bitField::e_bitState  carry2;
  bitField::e_bitState  result1;
  bitField::e_bitState  result2;
  bitField::t_byte      *memItDst;
  bitField::t_byte const *memItSrc;
  unsigned int          emulatedBitNb;
  /* Used for eflags management (OF / CF) */
  bitField::e_bitState  signOp1;
  bitField::e_bitState  signOp2;
  bitField::e_bitState  signResult;

  flags.word = 0;
  assert(this->getMem() && src.getMem() && overlap(*this, src) == false);
  emulatedBitNb = 0;
  oldCarry = bitField::Zero;
  memItDst = this->getStructMem();
  memItSrc = src.getStructMem();
  signOp1 =  this->get(this->getSignBitPos());
  signOp2 = src.get(src.getSignBitPos());
  while (emulatedBitNb < this->getEmulatedBitNb())
  {
    for (i = 0 ; i < 4 ; ++i)
    {
      /* OP1 */
      result1 = _getAddVal(memItDst->getBit(i), memItSrc->getBit(i));
      carry1 = _getAddCarry(memItDst->getBit(i), memItSrc->getBit(i));
      /* OP2 */
      result2 = _getAddVal(result1, oldCarry);
      carry2 = _getAddCarry(result1, oldCarry);
      /* OP3 */
      oldCarry = _getAddVal(carry1, carry2);

      if (memItDst->getBit(i) != result2) /* Only set if needed */
        memItDst->setBit(i, result2);
    }
    memItDst++;
    memItSrc++;
    emulatedBitNb += 4;
    if (emulatedBitNb == 4) /* AF: Set if an arithmetic operation generates a */
      flags.s_byte.AF = oldCarry;  /* carry or a borrow out of bit 3 of the result */
  }
  signResult = this->get(this->getSignBitPos());
  flags.s_byte.CF = oldCarry;
  flags.s_byte.OF = (signOp1 != bitField::One && signOp2 != bitField::One && signResult != bitField::Zero)
    || (signOp1 != bitField::Zero && signOp2 != bitField::Zero && signResult != bitField::One);
  if (flags.s_byte.OF) /* If we have an overflow, used state priority (Undefined > Unknown > One) */
    flags.s_byte.OF = std::max(std::max(signOp1, signOp2), signResult);
  this->_set_PFZFSF_flags(flags);
  return (*this);
}
