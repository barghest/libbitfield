// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//
/// @brief Implementation of the binary division (sign and unsigned)
///        using a bitfield.
///
/// The algorithm is a series of steps, each step having these four substeps:
///   1. Divide: Divide the working portion of the dividend by the divisor.
///   2. Multiply: Multiply the quotient (a single digit) by the divisor.
///   3. Subtract: Subtract the product from the working portion of the dividend.
///   4. Bring down: Copy down the next digit of the dividend to form the new working portion.
///
/// To handle signed binary number division, we first convert both the dividend
/// and the divisor to positive numbers to perform the division, and then correct
/// the signs of the results as needed.
/// If dividend is negative, then two's complement must be applied
/// to the remainder at the end. If the dividend and the divisor have different
/// signs, then the quotient must be negated with 2's complement operation at the end.
///

#ifndef BITFIELD_ARITHMETIC_PART_HH_
# error "Only <BitFieldArithmetic.hh> can be included directly."
#endif /* !BITFIELD_ARITHMETIC_PART_HH_*/

template <typename T>
auto    BitFieldArithmetic<T>::_operatorDivEqualInitRemainder(BitFieldArithmetic<typename std::make_unsigned<T>::type> const &dividend,
                                                              BitFieldArithmetic<typename std::make_unsigned<T>::type> const &divisor,
                                                              BitFieldArithmetic<typename std::make_unsigned<T>::type> &remainder) const -> int
{
  int            dividendCursor;
  unsigned int   leadingZero[2];
  int            divisorSignificativeBitNb;
  int            dividendSignificativeBitNb;

  leadingZero[0] = dividend.getMinLeadingZero();
  leadingZero[1] = divisor.getMinLeadingZero();
  divisorSignificativeBitNb = divisor.getEmulatedBitNb() - leadingZero[1];
  dividendSignificativeBitNb = dividend.getEmulatedBitNb() - leadingZero[0];
  if (divisorSignificativeBitNb <= dividendSignificativeBitNb)
  {
    remainder.copyRange(dividend, 0,
                        dividendSignificativeBitNb - divisorSignificativeBitNb,
                        divisorSignificativeBitNb);
    dividendCursor = dividendSignificativeBitNb - divisorSignificativeBitNb;
  }
  else
    dividendCursor = -1;
  // std::cout << "Dividend :" << dividend << "Divisor : " << divisor << std::endl;
  // std::cout << "Remainder: " << remainder << std::endl;
  // std::cout << "dividendCursor: " << dividendCursor << std::endl;
  // assert(0);
  return (dividendCursor);
}

template <typename T>
auto     BitFieldArithmetic<T>::operator/=(BitFieldArithmetic<T> const &divisor) -> BitFieldArithmetic<T> &
{
  // TODO: gerer l'overlap entre this et divisor (si besoin (pas besoin si code en mode transactionnel))
  bitfield_t<T>            _mem[4];
  BitFieldArithmetic<typename std::make_unsigned<T>::type>    remainder(&_mem[0].mem[0]);
  BitFieldArithmetic<T>        quotien(&_mem[1].mem[0]);
  BitFieldArithmetic<typename std::make_unsigned<T>::type>    divisor_copy(&_mem[2].mem[0]);
  BitFieldArithmetic<typename std::make_unsigned<T>::type>    dividend_copy(&_mem[3].mem[0]);
  int                                dividendCursor;
  // int            ret;
  bitField::e_bitState    isContained;

  quotien.setAllToZero();
  remainder.setAllToZero();
  std::memcpy(divisor_copy.getMem(), divisor.getMem(), divisor_copy.size());
  std::memcpy(dividend_copy.getMem(), this->getMem(), dividend_copy.size());
  if (std::is_signed<T>::value && divisor_copy.getSignBit() == bitField::One) // TODO: gerer composite
    divisor_copy.applyTwoComplement();
  if (std::is_signed<T>::value && dividend_copy.getSignBit() == bitField::One)
    dividend_copy.applyTwoComplement();
  dividendCursor = this->_operatorDivEqualInitRemainder(dividend_copy, divisor_copy, remainder);
  if (dividendCursor >= 0)
  {
    remainder >>= 1;
    do
    {
      remainder <<= 1;
      // remainder &= (T)~0; // 32 --> 0xFF
      remainder |= dividend_copy.get(dividendCursor);
      // std::cout << "Dividend cursor = " << dividendCursor << std::endl;
      isContained = (divisor_copy <= remainder);
      quotien.set(dividendCursor, isContained);
      if (isContained != bitField::Zero)
        remainder -= divisor_copy;
      // std::cout << "remainder" << remainder << std::endl;
      // std::cout << "quotien" << quotien << std::endl;
    } while (--dividendCursor >= 0);
  }
  if (std::is_signed<T>::value && this->getSignBit() != bitField::Zero) // If dividend_copy can be negative
    remainder.applyTwoComplement();
  if (std::is_signed<T>::value && this->getSignBit() != divisor.getSignBit()) // TODO: gerer le cas composite
    quotien.applyTwoComplement();
  // std::cout << "Quotien: " << quotien << std::endl;
  this->operator=(quotien);
  // this->operator=(remainder);
  return (*this);
}

/*
**
*/
template <typename T>
auto    BitFieldArithmetic<T>::operator/=(T divisor) -> BitFieldArithmetic<T> &
{
  bitfield_t<T>             _mem;
  BitFieldArithmetic<T>     _divisor(&_mem.mem[0]);

  _divisor = divisor;
  return (this->operator/=(_divisor));
}

/*
** Flags Affected
** The CF, OF, SF, ZF, AF, and PF flags are undefined.
*/
template <typename T>
auto    BitFieldArithmetic<T>::operatorDivEqual(BitFieldArithmetic<T> const &divisor,
                                                t_bitFieldEflags &flags) -> BitFieldArithmetic<T> &
{
  flags.setAllTo(bitField::Undefined);
  return (this->operator/=(divisor));
}

template <typename T>
auto    BitFieldArithmetic<T>::operatorDivEqual(T divisor, t_bitFieldEflags &flags) -> BitFieldArithmetic<T> &
{
  flags.setAllTo(bitField::Undefined);
  return (this->operator/=(divisor));
}
