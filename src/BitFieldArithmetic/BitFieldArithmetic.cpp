// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//

#include <cassert>
#include <iostream>
#include <algorithm>
#include <cstring>
#include "bitField/BitFieldArithmetic/BitFieldArithmetic.hh"
#include "bitField/ABitField/ABitFieldIterator.hh"

/********************************/

namespace bitField
{
  namespace BitFieldArithmetic
  {

    template <typename T>
    BitFieldArithmetic<T>::BitFieldArithmetic(void *memory)
      : ABitField::ABitField<bitField::Policies::ConstExprSize<T> >()
    {
      this->setMem(memory);
    }

    template <typename T>
    BitFieldArithmetic<T>::~BitFieldArithmetic()
    {
    }

/********************************/


// template <typename T>
// auto        BitFieldArithmetic<T>::size() const -> uint32_t {return (this->constExprSize());}
// template <typename T>
// constexpr auto    BitFieldArithmetic<T>::constExprSize() const -> uint32_t {return (sizeof(T) * 2);}

/********************************/

    template <typename T>
    auto    BitFieldArithmetic<T>::_getBound(T &umin, T &umax) const -> void
    {
      T                 max_value;
      T                 min_value;
      unsigned int      isUnknown;
      unsigned int      emulatedBitNb;
      unsigned int      emulatedBitVal;
      unsigned int      realByte;
      bitField::t_byte const    *mem;

      this->_checkMemory();
      umin = 0;
      umax = 0;
      realByte = 0;
      emulatedBitNb = 0;
      mem = this->getStructMem();
      while (realByte < this->size())
      {
        for (emulatedBitNb = 0 ; emulatedBitNb < 4 ; emulatedBitNb++)
        {
          emulatedBitVal = mem->getBit(emulatedBitNb);
          min_value = emulatedBitVal & 0x1;
          max_value = min_value;
          isUnknown = ((emulatedBitVal & 0x2) >> 1);
          if (isUnknown)
          {
            min_value = 0;
            max_value = 1;
          }
          umin = umin | (min_value << ((realByte * 4) + emulatedBitNb));
          umax = umax | (max_value << ((realByte * 4) + emulatedBitNb));
        }
        realByte++;
        mem++;
      }
    }

    template <typename T>
    auto    BitFieldArithmetic<T>::getMin(void) const -> T
    {
      T        min;
      T        max;

      this->_getBound(min, max);
      return ((min < max ? min : max));
    }

    template <typename T>
    auto    BitFieldArithmetic<T>::getMax(void) const -> T
    {
      T        min;
      T        max;

      this->_getBound(min, max);
      return ((min < max ? max : min));
    }

    template <typename T>
    auto    BitFieldArithmetic<T>::getSignBitPos(void) const -> unsigned int
    {
      this->_checkMemory();
      return (this->getEmulatedBitNb() - 1);
    }

    template <typename T>
    auto    BitFieldArithmetic<T>::getSignBit(void) const -> bitField::e_bitState
    {
      this->_checkMemory();
      return (this->get(this->getSignBitPos()));
    }

    template <typename T>
    auto    BitFieldArithmetic<T>::isComposite(void) const -> bool
    {
      T        mask;
      T const    *mem;
      bool        ret;

      this->_checkMemory();
      mem = (decltype(mem))this->getMem();
      if (sizeof(T) == 1) mask = (T)0xAA;           /* 0b1010 1010 */
      else if (sizeof(T) == 2) mask = (T)0xAAAA;    /* 0b1010 1010 ... */
      else if (sizeof(T) == 4) mask = (T)0xAAAAAAAA;/* 0b1010 1010 ... */
#if defined(__x86_64__) || defined(__ia64__) || defined(_WIN64)
      else if (sizeof(T) == 8) mask = (T)0xAAAAAAAAAAAAAAAA; /* 0b1010 1010 ... */
#endif /* 64b */
      // We mask over 0xAA.... because it allows us to extract the bits corresponding
      // to a Unknown/Undefined bit quickly. (00 => Zero, 01 => One, 10 => Unknown, 11 -> Undefined)
      ret = (((mem[0] & mask) | (mem[1] & mask)) != 0);
      return (ret);
    }

    template <typename T>
    auto    BitFieldArithmetic<T>::isScalar(void) const -> bool
    {
      return (this->isComposite() == false);
    }

    template <typename T>
    auto    BitFieldArithmetic<T>::contain(T val) const -> bitField::e_bitState
    {
      unsigned int          i;
      unsigned char         bit_nb[4] = {0};
      bitField::e_bitState  st;

      this->_checkMemory();
      for (i = 0 ; i < this->getEmulatedBitNb() ; i++)
      {
        st = this->get(i);
        bit_nb[st]++;
        if (st == !(val & 1)) // Can't be contained if 'st' is scalar and the opposite of val
          return (bitField::Zero);
        val = val >> 1;
      }
      if (bit_nb[3])
        return (bitField::Undefined);
      else if (bit_nb[2])
        return (bitField::Unknown);
      else
        return (bitField::One);
    }

    template <typename T>
    auto    BitFieldArithmetic<T>::packBits(T &ret) const -> int
    {
      int   bitNb;

      this->_checkMemory();
      if (this->getMem() && this->isScalar())
      {
        ret = 0;
        bitNb = this->getEmulatedBitNb();
        while (bitNb > 0)
        {
          bitNb = bitNb - 1;
          ret <<= 1;
          ret |= this->get(bitNb);
        }
        return (0);
      }
      return (-1);
    }

    template <typename T>
    auto    BitFieldArithmetic<T>::toInt(void) const throw (std::logic_error) -> T
    {
      T     ret;
      int   err;

      err = this->packBits(ret);
      if (err == -1)
        throw std::logic_error ("Emulated type is not scalar");
      return (ret);
    }

    template <typename T>
    auto    BitFieldArithmetic<T>::applyOneComplement(void) -> void
    {
      unsigned int    i;

      this->_checkMemory();
      for (i = 0 ; i < this->size() ; i++)
        this->getStructMem()[i].applyOneComplement();
    }

    template <typename T>
    auto    BitFieldArithmetic<T>::applyTwoComplement(void) -> void
    {
      this->applyOneComplement();
      this->operator+=(1);
    }

    /*!
    ** Truth table for the merge function
    ** TODO: Note: check if we really need 'X' between '?' and (0 or 1)
    ** -------------------
    ** + | 0 | 1 | ? | U |      0 ==> Zero
    ** -------------------      1 ==> One
    ** 0 | 0 | X | X | U |      ? ==> Unknown
    ** -------------------      U ==> Undefined
    ** 1 |///| 1 | X | U |      X ==> Merge strategy (preference)
    ** -------------------
    ** ? |///|///| ? | U |
    ** -------------------
    ** U |///|///|///| U |
    ** -------------------
    */
    template <typename T>
    auto    BitFieldArithmetic<T>::merge(T val, bitField::e_bitState st) -> BitFieldArithmetic<T> &
    {
      unsigned int        i;

      this->_checkMemory();
      for (i = 0 ; i < this->getEmulatedBitNb() ; i++)
      {
        if (this->get(i) != bitField::Undefined && this->get(i) != (val & 1))
          this->set(i, st);
        val = val >> 1;
      }
      return (*this);
    }

    /*
    ** TODO: tester les fonctions merge()
    */
    template <typename T>
    auto    BitFieldArithmetic<T>::merge(BitFieldArithmetic<T> const &val,
                                         bitField::e_bitState st) -> BitFieldArithmetic<T> &
    {
      unsigned int        i;

      this->_checkMemory();
      for (i = 0 ; i < this->getEmulatedBitNb() ; i++)
      {
        if (this->get(i) != bitField::Undefined && this->get(i) != val.get(i))
          this->set(i, std::max(st, val.get(i)));
      }
      return (*this);
    }

    template <typename T>
    auto    BitFieldArithmetic<T>::nbValues(void) const -> size_t
    {
      unsigned int  i;
      size_t        nb_values;

      this->_checkMemory();
      nb_values = 1;
      for (i = 0 ; i < this->getEmulatedBitNb() ; i++)
      {
        if (this->get(i) != bitField::Zero && this->get(i) != bitField::One)
          nb_values *= 2;
      }
      return (nb_values);
    }

    /*
    ** This function fills mem with each possible values up to nb_item or the
    ** maximum number of values contained. To get those values it use a small
    ** algorithm in 3/4 steps:
    ** Exemple with the composite value     01?0 000U:
    ** 1/ Init:
    **    Get the composite bit mask    --> 0010 0001
    **    Get "or" and "and" mask (or)  --> 1101 1110 (used before for the addition)
    **                           (and)  --> 0010 0001 (used to get back lost bits from this)
    ** 2/
    ** Loop (turn 0):
    ** * Set + increment memory (mem[0] = 0100 0000)
    ** * Get next value
    ** * * 1/ Mask the current value with the 'or' mask
    **        --> 0100 0000 | 1101 1110 ==> 1101 1110
    ** * * 2/ Increment (carry will be pushed to the next zero bit)
    **        --> 1101 1110 + 1 ==> 1101 1111
    ** * * 3/ Reset scalar bits and get them back from this.
    **        --> 1101 1111 & 0010 0001 ==> 0000 0001
    **        --> 0000 0001 | 0100 0000 ==> 0100 0001
    **
    ** Loop (turn 1):
    ** * Set + increment memory (mem[1] = 0100 0001)
    ** * Get next value
    ** * * 1/ Mask the current value
    **        --> 0100 0001 | 1101 1110 ==> 1101 1111
    ** * * 2/ Increment
    **        --> 1101 1111 + 1 ==> 1110 0000
    ** * * 3/ Reset
    **        --> 1110 0000 & 0010 0001 ==> 0010 0000
    **        --> 0010 0000 | 0100 0000 ==> 0110 0000
    **
    ** Loop (turn 2):
    ** * Set + increment memory (mem[2] = 0110 0000)
    ** * Get next value
    ** * * 1/ Mask the current value
    **        --> 0110 0000 | 1101 1110 ==> 1111 1110
    ** * * 2/ Increment
    **        --> 1111 1110 + 1 ==> 1111 1111
    ** * * 3/ Reset
    **        --> 1111 1111 & 0010 0001 ==> 0010 0001
    **        --> 0010 0001 | 0100 0000 ==> 0110 0001
    ** Etc
    ** Result:  01?0 000U (original composite type)
    ** mem[0] = 0100 0000
    ** mem[1] = 0100 0001
    ** mem[2] = 0110 0000
    ** mem[3] = 0110 0001
    */
    template <typename T>
    auto    BitFieldArithmetic<T>::getValues(T *mem, size_t nb_item) const -> size_t
    {
      unsigned int        i;
      T            andMask;
      T            orMask;
      size_t        endLoop;
      T            maskedThisValue;
      T            val;

      this->_checkMemory();
      maskedThisValue = this->getMin();
      andMask = this->_getCompositeBitsMask();
      orMask = ~andMask;
      if (sizeof(T) == sizeof(size_t) && this->nbValues() == 0) // All possible values
        endLoop = nb_item;
      else
        endLoop = std::max(nb_item, nb_item);
      val = maskedThisValue; // Starting value (min (all composite bits set to '0'))
      for (i = 0 ; i < endLoop ; i++)
      {
        *mem = val;     // Set item
        mem += 1;       // Next item

        val |= orMask;  // Next value
        val += 1;

        val &= andMask; // Filter result
        val |= maskedThisValue; // Reset scalar bits
      }
      return (endLoop);
    }

    template <typename T>
    auto    BitFieldArithmetic<T>::getValues(std::vector<T> &mem) const -> size_t
    {
      this->_checkMemory();
      return (this->getValues((T *)mem.data(), mem.size()));
    }

    template <typename T>
    auto    BitFieldArithmetic<T>::reverse(void) -> BitFieldArithmetic<T> &
    {
      unsigned int        i;
      unsigned int        bitNb;
      unsigned int        lastBitPos;
      bitField::e_bitState    swap_st;

      this->_checkMemory();
      bitNb = this->getEmulatedBitNb();
      lastBitPos = this->getSignBitPos();
      for (i = 0 ; i < (bitNb / 2) ; i++)
      {
        swap_st = this->get(i);
        this->set(i, this->get(lastBitPos - i));
        this->set(lastBitPos - i, swap_st);
      }
      return (*this);
    }

    template <typename T>
    auto    BitFieldArithmetic<T>::getMinLeadingZero(void) const -> unsigned int
    {
      int    i;
      int    leadingZero;

      this->_checkMemory();
      leadingZero = 0;
      i = this->getSignBitPos();
      while (i >= 0 && this->get(i) == bitField::Zero)
      {
        leadingZero++;
        i--;
      }
      return (leadingZero);
    }

    template <typename T>
    auto    BitFieldArithmetic<T>::getMaxLeadingZero(void) const -> unsigned int
    {
      int    i;
      int    leadingZero;

      this->_checkMemory();
      leadingZero = 0;
      i = this->getSignBitPos();
      while (i >= 0 && this->get(i) != bitField::One)
      {
        leadingZero++;
        i--;
      }
      return (leadingZero);
    }

    template <typename T>
    auto    BitFieldArithmetic<T>::copyRange(BitFieldArithmetic<T> const &mem, uint32_t dstEmulatedBit,
                                             uint32_t srcEmulatedBit, uint32_t size) -> void
    {
      uint32_t    byte;
      uint32_t    bit;
      uint32_t    bit_state_offset;

      this->_checkMemory();
      this->_getBitOffset(srcEmulatedBit, byte, bit, bit_state_offset);
      if (byte < mem.size())
        this->copyRange(mem.getStructMem(), dstEmulatedBit, srcEmulatedBit, size);
    }

    template <typename T>
    auto inline    BitFieldArithmetic<T>::_checkMemory(void) const -> void
    {
      if (!this->getMem())
      {
        throw std::runtime_error ("Unable to perform arithmetic: please use setMem(...)" \
                                  "to bind a valid (non null) memory pointer.");
      }
    }

    /*
    ** Comment below are directly taken from the Intel manual pseudo code of the
    ** shift instruction (SHL/SAL/SAR/SHR)
    */
    template <typename T>
    auto    BitFieldArithmetic<T>::_operatorShiftSetFlags(BitFieldArithmetic<T> const &new_this,
                                                          unsigned int shift,
                                                          t_bitFieldEflags &flags,
                                                          bool shift_right) const -> void
    {
      unsigned int          masked_shift;
      bitField::e_bitState  result_msb;

      masked_shift = shift & (sizeof(T) == 8 ? 63 : 31);
      if (shift > 0)
      {
        if (shift < this->getEmulatedBitNb())
        {
          if (shift_right == false) // IF instruction is SAL or SHL
            flags.s_byte.CF = this->get(this->getEmulatedBitNb() - shift); // CF <-- MSB(DEST);
          else // ELSE (* Instruction is SAR or SHR *)
            flags.s_byte.CF = this->get(shift - 1); // // CF <-- LSB(DEST);
        } // Undefined for logical shift (aka unsigned shift) if (shift >= type_size)
        else if (std::is_unsigned<T>::value) // SHR / SHL
          flags.s_byte.CF = bitField::Undefined;
        else // SAR / SAL
          flags.s_byte.CF = (shift_right ? this->getSignBit() : bitField::Zero);

        // std::cout << "Masked Shift by " << masked_shift << ": shift: "<< shift << std::endl;
        if (masked_shift == 1) // IF (COUNT AND countMASK) = 1
        {
          if (shift_right == false) // IF instruction is SAL or SHL
          {
            result_msb = new_this.getSignBit();
            if (bitField::isScalar(flags.s_byte.CF) && bitField::isScalar(result_msb))
              flags.s_byte.OF = (flags.s_byte.CF ^ result_msb);
            else
              flags.s_byte.OF = std::max((bitField::e_bitState)flags.s_byte.CF,
                                         (bitField::e_bitState)result_msb);
          }
          else
          {
            flags.s_byte.OF = this->getSignBit(); // IF instruction is SAR
            if (std::is_signed<T>::value) // ELSE (* Instruction is SHR *)
              flags.s_byte.OF = bitField::Zero;
          }
        }
        else if (masked_shift == 0) // ELSE IF (COUNT AND countMASK) = 0
        {
        } // All flags unchanged
        else
          flags.s_byte.OF = bitField::Undefined;
        // std::cout << "Of:CF set to " << +flags.s_byte.OF << ":" << +flags.s_byte.CF<< std::endl;
        if (masked_shift > 0) // ELSE (* COUNT not 1 or 0 *)
        {
          flags.s_byte.AF = bitField::Undefined; // if (shift != 0) --> Undefined
          new_this._set_PFZFSF_flags(flags);
        }
      }
      return ;
    }

    template <typename T>
    auto    BitFieldArithmetic<T>::_getCompositeBitsMask(void) const -> T
    {
      T            mask;
      unsigned int        i;
      bitField::e_bitState    st;

      mask = 0;
      for (i = this->getEmulatedBitNb() ; i > 0 ; i--)
      {
        mask <<= 1;
        st = this->get(i - 1);
        if (st == bitField::Unknown || st == bitField::Undefined)
          mask |= 1;
      }
      return (mask);
    }

    template <typename T>
    auto    BitFieldArithmetic<T>::_manageOverlap(BitFieldArithmetic<T> const &src,
                                                  t_bitFieldEflags &flags,
                                                  _manageOverlapFctPtr1 fct) -> BitFieldArithmetic<T> &
    {
      if (overlap(*this, src))
      {

        /* Used in case this and src are overlapping and would corrupt the result */
        bitfield_t<T>        _overlap_tmp_mem;
        BitFieldArithmetic<T>    src_copy(&_overlap_tmp_mem.mem[0]);

        src_copy = src;
        return ((this->*fct)(src_copy, flags));
      }
      else
        return ((this->*fct)(src, flags));
    }

    /*
    ** TODO: faire des tests pour verifier que les resultats sur overlap sont bon
    **       pour chaque operation arithmetic.
    */
    template <typename T>
    auto    BitFieldArithmetic<T>::_manageOverlap(BitFieldArithmetic<T> const &val,
                                                  BitFieldArithmetic<T> &high,
                                                  t_bitFieldEflags &flags,
                                                  _manageOverlapFctPtr2 fct) throw (std::invalid_argument) -> BitFieldArithmetic<T> &
    {
      if (overlap(*this, high) == false)
      {
        if (overlap(*this, val) || overlap(high, val)) /* (this or lhigh) and src overlap */
        {
          /* Used in case this/high and val are overlapping and would corrupt the result */
          bitfield_t<T>        _overlap_tmp_mem;
          BitFieldArithmetic<T>    src_copy(&_overlap_tmp_mem.mem[0]);

          src_copy = val;
          return ((this->*fct)(src_copy, high, flags));
        }
        else /* No overlap */
          return ((this->*fct)(val, high, flags));
      }
      else /* This and high overlap */
        throw std::invalid_argument ("BitFieldArithmetic<T>::_manageOverlap(...): "
                                     "this and high are overlaping.");
    }

    template <typename T>
    auto    BitFieldArithmetic<T>::_operatorShiftEqual(BitFieldArithmetic<T> const &shift,
                                                       t_bitFieldEflags &flags,
                                                       _shiftFct shiftSubFct) -> BitFieldArithmetic<T> &
    {
      T                         max;
      bool                      isShiftedValTroncated;
      unsigned int              i;
      size_t                    nb_item;
      bitfield_t<T>             _mem[3];
      BitFieldArithmetic<T>     _shift_copy(&_mem[0].mem[0]);
      BitFieldArithmetic<T>     _this_copy(&_mem[1].mem[0]);
      BitFieldArithmetic<T>     _result(&_mem[2].mem[0]);
      T                         emulatedValues[(sizeof(T) == 8 ? 64 : 32)];
      unsigned int    const     array_size = sizeof(emulatedValues) / sizeof(*emulatedValues);
      t_bitFieldEflags          _flagsShift;
      t_bitFieldEflags          _flagsMerge;
      unsigned int              _step_shift; // Each seperated shift

      _this_copy = *this;
      _flagsShift.word = flags.word;
      _flagsMerge.word = flags.word;
      max = shift.getMax();
      isShiftedValTroncated = (max > (sizeof(T) == 8 ? 63 : 31));
      if (isShiftedValTroncated) // Mask to respect intel shift limitation
      {
        _shift_copy = shift;
        _shift_copy &= (sizeof(T) == 8 ? 63 /* 0x3F */ : 31 /* 0x1F */ );
      }
      BitFieldArithmetic<T>    const _shift_const((isShiftedValTroncated ?
                                                   (_shift_copy.getMem()) : (shift.getMem())));

      nb_item = _shift_const.getValues(&emulatedValues[0], array_size);
      assert(nb_item > 0);
      _result = ((_this_copy.*shiftSubFct)(emulatedValues[0], _flagsMerge));
      for (i = 1; i < nb_item; i++) // For each scalar shift
      {
        assert(i < array_size);
        // Get the next shift possibility and add "isShiftedValTroncated" at bit 7,
        // above the 6b mask in 64b to force the shift to take into account that
        // shift<T> is truncated. This has been done in order to ensure eflags are
        // set correctly to bitField::Undefined if needed.
        _step_shift = (emulatedValues[i] - emulatedValues[i - 1]) | (isShiftedValTroncated << 7);
        (_this_copy.*shiftSubFct)(_step_shift, _flagsShift);
        _result.merge(_this_copy, bitField::Unknown); // TODO: Changer la strategy du merge
        _flagsMerge.merge(_flagsShift);
      }
      // if (shift.contain(0))
      //   flags.merge(_flagsMerge);
      // else
      flags.word = _flagsMerge.word;
      return (this->operator=(_result));
    }

/********************************/

#define BITFIELD_ARITHMETIC_PART_HH_
#include "BitFieldArithmeticAdd.i"
#include "BitFieldArithmeticSub.i"
#include "BitFieldArithmeticMul.i"
#include "BitFieldArithmeticDiv.i"

#include "BitFieldArithmeticLShift.i"
#include "BitFieldArithmeticRShift.i"

#include "BitFieldArithmeticAnd.i"
#include "BitFieldArithmeticOr.i"
#include "BitFieldArithmeticXor.i"
#undef BITFIELD_ARITHMETIC_PART_HH_

/********************************/

    template <typename T>
    auto    BitFieldArithmetic<T>::_set_PFZFSF_flags(t_bitFieldEflags &flags) const -> void
    {
      unsigned int  i;
      unsigned char bitsCount[4] = {0};
      unsigned int  bitNb;

      bitNb = this->getEmulatedBitNb();
      for (i = 0 ; i < 8 ; i++)
        bitsCount[this->get(i)]++;
      if (bitsCount[bitField::Undefined])
      {
        flags.s_byte.PF = bitField::Undefined;
        flags.s_byte.ZF = bitField::Undefined;
      }
      else if (bitsCount[bitField::Unknown])
      {
        flags.s_byte.PF = bitField::Unknown;
        flags.s_byte.ZF = bitField::Unknown;
      }
      else
      {
        flags.s_byte.PF = (bitsCount[bitField::One] % 2) == 0;
        for (i = 8 ; i < bitNb ; i++)
          bitsCount[this->get(i)]++;
        if (bitsCount[bitField::Undefined])
          flags.s_byte.ZF = bitsCount[bitField::Undefined];
        else if (bitsCount[bitField::Unknown])
          flags.s_byte.ZF = bitsCount[bitField::Unknown];
        else
          flags.s_byte.ZF = (bitsCount[bitField::One] == 0);
      }
      flags.s_byte.SF = this->get(this->getSignBitPos());
    }

/********************************/

    template <typename T>
    auto    BitFieldArithmetic<T>::operator=(T val) -> BitFieldArithmetic<T> &
    {
      bitField::t_byte    *memIt;
      unsigned int        emulatedBitNb;

      this->_checkMemory();
      memIt = this->getStructMem();
      if (val)
      {
        emulatedBitNb = 0;
        while (emulatedBitNb < (this->getEmulatedBitNb()))
        {
          /* Ex: Convert (1111 0100) to (01 01 01 01) (00 01 00 00)  */
          /* 0 -> 00 ; 1 --> 01 */
          memIt->s_byte.b0 = (val >> 0) & 0x1;
          memIt->s_byte.b1 = (val >> 1) & 0x1;
          memIt->s_byte.b2 = (val >> 2) & 0x1;
          memIt->s_byte.b3 = (val >> 3) & 0x1;

          memIt++;
          val = val >> 4;
          emulatedBitNb += 4;
        }
      }
      else
        std::memset(memIt, 0, this->size());
      return (*this);
    }

    template <typename T>
    auto    BitFieldArithmetic<T>::operator=(BitFieldArithmetic<T> const &val) -> BitFieldArithmetic<T> &
    {
      this->_checkMemory();
      std::memmove(this->getMem(), val.getMem(), this->size());
      return (*this);
    }

// template <typename T>
// auto    BitFieldArithmetic<T>::operator=(BitFieldArithmetic<T> const *val) -> BitFieldArithmetic<T> &
// {
//   return (this->operator=(*val));
// }

/***************************************************/

/* Forward instanciation */

    template class BitFieldArithmetic <uint8_t>;
    template class BitFieldArithmetic <uint16_t>;
    template class BitFieldArithmetic <uint32_t>;

    template class BitFieldArithmetic <int8_t>;
    template class BitFieldArithmetic <int16_t>;
    template class BitFieldArithmetic <int32_t>;

#if defined(__x86_64__) || defined(__ia64__) || defined(_WIN64)
    template class BitFieldArithmetic <uint64_t>;
    template class BitFieldArithmetic <int64_t>;
#endif /* !64b */

  } // !namespace BitFieldArithmetic
} // !namespace bitField
