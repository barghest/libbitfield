// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//
/// àbrief Implementation of the binary multiplication (signed and unsigned)
///        using a BitField.
///
///  There are several case in the binary Intel multiplication.
///  * The signed case.
///  * The unsigned case.
///
/// 1/ Unsigned
/// ============
///
/// The unsigned case is pretty simple. No sign extension are needed at all
/// and bits from the low part are simply moved in the higher part. In short
/// performing an unsigned multiplication with a uint8_t is equivalent to
/// a multiplication with uint16_t. (promotion to the upper type). If the
/// maximum size type is used it is casted in an upper type anyway.
///
/// Exemple: The number correspond to a size (64 ==> 64bits) / (32 ==> 32bits)
/// (mul syntax ==> [(128)((64)rdx:(64)rax) = (64)rax * (64)an_other_register])
/// (mul syntax ==> [(64)((32)edx:(32)eax)  = (32)eax * (32)an_other_register])
/// etc ... (cf Intel manual for more details)
///
/// -------------------------------------------
///
/// 2/ Signed
/// ============
////
/// The signed case is much more complexe. For simplicity the next part will be
/// splited in two part:
/// * Using normal   arithmetic with only '0' and '1'.
/// * Using barghest arithmetic with 4 bit states (Zero, One, Unknown, Undefined)
///         -> Unknown means  'This bit is set by the user'
///         -> Undefine means 'This bit is NOT set by the user, and is an error'
///         ---> Both can be ether Zero or One.
///
/// 1/ First part (using only '0' and '1')
/// --------------------------------------
///
/// ==> The following explaination is an extract of this article, thanks to him
///     for providing a simple, and working article for the Intel multiplication.
///     Url: """http://pages.cs.wisc.edu/~cs354-1/beyond354/int.mult.html"""
///     Copyright © Karen Miller, 2006
///
///    multiplicand
///  x   multiplier
///  ----------------
///     product
///
/// If we do not sign extend the operands (multiplier and multiplicand),
/// before doing the multiplication, then the wrong answer sometimes results.
/// To make this work, sign extend the partial products to the correct number of bits.
///
/// To result in the least amount of work, classify which do work, and which do not.
///
///     +          -            +            -      (multiplicands)
///   x +        x +          x -          x -      (multipliers)
///   ----       ----         ----         ----
///    OK        sign          |            |
///            extend          | take       |
///           partial          | additive   |
///           products         | inverses   |
///
///                             -            +
///                           x +          x +
///                           ----          ----
///                           sign           OK
///                         extend
///                        partial
///                        products
///
/// Example:
///
///   without                          with correct
///   sign extension                   sign extension
///
///       11100 (-4)                         11100
///     x 00011 (3)                        x 00011
///     ------------                      ---------
///       11100                         1111111100
///      11100                         1111111100
///     ------------                ---------------
///     1010100 (-36)                   1111110100  (-12)
///      WRONG!                            RIGHT!
///
/// Another example:
///
///     without adjustment              with correct adjustment
///
///       11101 (-3)                       11101 (-3)
///     x 11101 (-3)                     x 11101 (-3)
///    -------------                    -------------
///       11101                           (get additive inverse of both)
///     11101
///    11101                               00011 (+3)
///  +11101                              x 00011 (+3)
///  -----------                        -------------
///  1101001001 (wrong!)                   00011
///                                     + 00011
///                                    ---------
///                                       001001 (+9) (right!)
///
/// =====================================================================
///
/// 2/ Second part (using BitField arithmetic (Zero 0, One 1, Unknown ?, Undefined U))
/// --------------------------------------------------------------------------
///
/// While we try to use the method explained above there are several modification
/// and issued:
/// --> Unknown and Undefined bits need to be spread
/// --> Unlike the add or sub operation, the multiplication is completly different
///     when operating with positive or negative numbers.
/// --> We have to stay as precise as possible to avoid false positive.
///
/// Each "sign case" will be explained separatly.
/// Note1: The symbol '0', '1', '?', 'U' will be used to represent respectively
///        a positive, negative, unknown or undefined sign.
/// Note2: Signed multiplication with positive number, with partial products
///        signed extended with '0'(Zero) is equivalent to and unsigned
///        multiplication.
///
/// * ({sign of multiplicand}{sign of multiplier})
/// * 00 --> Using the classical unsigned multiplication algorithm.
///      ===> Standard multiplication
///
/// * 01 --> 2n case of the above example with standard arithmetic
///      --> Take additive inverses and then sign extend partial products
///      ===> 01 -> 10 -> signed mult (sign extend partial products)
///
/// * 0? and 0U --> Same as 01, but we sign extend with '?'/'U' instead of '1'.
///
/// * 10 --> signed mult (sign extend partial products)
/// * 11 --> Last cast, ('-' * '-') -> ('+' * '+')
///      ===> Apply two complement on both -> Standard multiplication
///
/// * 1? and 1U --> This case is a bit more complexe as it involve both '11' and '10'.
///      --> Here there are 2 solution: (second is used)
///          * We can reset to '?'/'U' all bits and say its
///          ok, but this would do more than just discarding precision. It will
///          also discard undefined bits in the number and convert 'U' bits to
///          valid bits.
///          * We can perform both and then merge the result. This will take
///          twice more time but will save precision. Merge the result mean
///          create one representation capable of containing both. This
///          can be explained in short by 'The value of the bit in both case depend
///          on an Unknown/Undefined one. The result is then Unknown/Undefined.
///          See the documentation of the method "merge*()" for more details.
///      ===> see '10' and '11' case -> merge result with '?'(Unknown) or 'U'(Undefined)
///
/// * ?0 ===> Convert to '0?'.
/// * ?1 ===> Convert to '1?'.
/// * U0 ===> Convert to '0U'.
/// * U1 ===> Convert to '1U'.
///
/// * ??, ?U, U? and UU ===> We follow the same principe as '1?' and '1U': execute
///              all possible way and the merge the result. We have 2 different
///              case 00, 10 (for {+ * +} and {- * +}). the 11 ({- * -}) is
///              converted into 00 ({+ * +}) and 10 is the same as 01.
///       ===> see '00' and '10' case --> merge result with '?'(Unknown) or 'U'(Undefined)
///            (same wrapper as 1? and 1U)
///

#ifndef BITFIELD_ARITHMETIC_PART_HH_
# error "Only <BitFieldArithmetic.hh> can be included directly."
#endif /* !BITFIELD_ARITHMETIC_PART_HH_*/

template <typename T>
auto    BitFieldArithmetic<T>::operator*=(BitFieldArithmetic<T> const &val) -> BitFieldArithmetic<T> &
{
  bitfield_t<T>         _tmp_high_mem;
  BitFieldArithmetic<T> _bitFieldVal_high(&_tmp_high_mem.mem[0]);
  t_bitFieldEflags      flags;

  __print_debug("");
  flags.word = 0;
  _bitFieldVal_high = (T)0;
  return (this->operatorMultEqual(val, _bitFieldVal_high, flags));
}

template <typename T>
auto    BitFieldArithmetic<T>::operator*=(T val) -> BitFieldArithmetic<T> &
{
  bitfield_t<T>         _tmp_mem;
  bitfield_t<T>         _tmp_high_mem;
  BitFieldArithmetic<T> _bitFieldVal(&_tmp_mem.mem[0]);
  BitFieldArithmetic<T> _bitFieldVal_high(&_tmp_high_mem.mem[0]);
  t_bitFieldEflags      flags;

  __print_debug("");
  flags.word = 0;
  _bitFieldVal = val;
  _bitFieldVal_high = (T)0;
  return (this->operatorMultEqual(_bitFieldVal, _bitFieldVal_high, flags));
}

template <typename T>
auto    BitFieldArithmetic<T>::__operatorMultPartialProducts(BitFieldArithmetic<T> &old_result_low,
                                                             BitFieldArithmetic<T> &old_result_high,
                                                             BitFieldArithmetic<T> const &src_shifted_low,
                                                             BitFieldArithmetic<T> const &src_shifted_high,
                                                             bitField::e_bitState multiplierBit) -> void
{
  t_bitFieldEflags    addc_flags;

  __print_debug("");
  addc_flags.word = 0;
  if (multiplierBit != bitField::One) // Apply merge with 0000
  {
    bitfield_t<T>           _mem[2];
    BitFieldArithmetic<T>   src_shifted_merged_low(&_mem[0].mem[0]);
    BitFieldArithmetic<T>   src_shifted_merged_high(&_mem[1].mem[0]);

    src_shifted_merged_low = src_shifted_low;
    src_shifted_merged_high = src_shifted_high;
    assert(multiplierBit == bitField::Unknown || multiplierBit == bitField::Undefined);
    src_shifted_merged_low.merge(0, multiplierBit);
    old_result_low.operatorAddEqual(src_shifted_merged_low, addc_flags); /* ADD old_low, low */
    old_result_high += src_shifted_merged_high;        /* ADC old_high, high */
  }
  else
  {
    old_result_low.operatorAddEqual(src_shifted_low, addc_flags); /* ADD old_low, low */
    old_result_high += src_shifted_high;        /* ADC old_high, high */
  }
  old_result_high += addc_flags.s_byte.CF;
}

template <typename T>
auto    BitFieldArithmetic<T>::_operatorMultSetFlags(BitFieldArithmetic<T> const &high,
                                                     t_bitFieldEflags &flags,
                                                     bool is_signed) const -> void
{
  bitField::e_bitState        cf_of;

  __print_debug("");
  cf_of = bitField::Zero;
  if (is_signed == false) /* Unsigned */
    cf_of = high.contain(0);
  else // is signed
  {
    if (this->getSignBit() == bitField::Zero)
      cf_of = high.contain(0);
    else if (this->getSignBit() == bitField::One)// is signed and high is equal to {1111 1111}+
      cf_of = high.contain((T)~0);
    else
      assert(0);
  }
  if (bitField::isScalar(cf_of)) // Not Unknown nor undefined
    cf_of = (bitField::e_bitState)!((bool)cf_of); // Inverse result (If containt 0/~0, no overflow)
  flags.s_byte.CF = flags.s_byte.OF = cf_of;
  flags.s_byte.SF = flags.s_byte.ZF = flags.s_byte.AF = flags.s_byte.PF = bitField::Undefined;
}

/*
** MUL (unsigned): Flags Affected
** The OF and CF flags are set to 0 if the upper half of the result is 0
** otherwise, they are set to 1.
** The SF, ZF, AF, and PF flags are undefined.
**
** IMUL  (signed): Flags Affected
** For the one operand form of the instruction, the CF and OF flags are set when
** significant bits are carried into the upper half of the result and cleared
** when the result fits exactly in the lower half of the result. For the two-and
** three-operand forms of the instruction, the CF and OF flags are set when the
** result must be truncated to fit in the destination operand size and cleared
** when the result fits exactly in the destination operand size.
** The SF, ZF, AF, and PF flags are undefined.
**
** TODO: OPTI: Direct multiplication if scalar value
*/
template <typename T>
auto    BitFieldArithmetic<T>::_operatorMultEqual(BitFieldArithmetic<T> const &multiplier,
                                                  BitFieldArithmetic<T> &high,
                                                  t_bitFieldEflags &flags,
                                                  bool is_signed) -> BitFieldArithmetic<T> &

{
  unsigned int        i;
  unsigned int        leftShiftAmount;
  unsigned int        emulatedBitNb;
  bitField::t_byte const *memItMultiplier;
  bitField::e_bitState   multiplierBit;

  bitfield_t<T>         _mem[2][2];
  BitFieldArithmetic<T> old_result_low(&_mem[0][0].mem[0]);
  BitFieldArithmetic<T> old_result_high(&_mem[0][1].mem[0]);
  BitFieldArithmetic<T> src_shifted_low(&_mem[1][0].mem[0]);
  BitFieldArithmetic<T> src_shifted_high(&_mem[1][1].mem[0]);

  __print_debug("");
  flags.word = leftShiftAmount = emulatedBitNb = 0;
  assert(this->getMem() && multiplier.getMem()
         && overlap(*this, multiplier) == false && overlap(*this, high) == false
         && overlap(high, multiplier) == false
         && (std::is_unsigned<T>::value || multiplier.getSignBit() == bitField::Zero)
         && sizeof(_mem) == (this->size() * 4)); /* Safety check */
  std::memset(_mem, 0, sizeof(_mem));
  memItMultiplier = multiplier.getStructMem();
  src_shifted_low = *this;
  if (std::is_signed<T>::value) /* Sign extend partial products */
    src_shifted_high.setAllTo(this->getSignBit());
  high = src_shifted_high;
  while (emulatedBitNb < this->getEmulatedBitNb())
  {
    for (i = 0 ; i < 4 ; ++i) /* 4 because there are 4 emulated bits per byte */
    {
      multiplierBit = memItMultiplier->getBit(i);
      if (multiplierBit != bitField::Zero)
      {
        src_shifted_low.operatorLShiftEqualExtented(src_shifted_high, leftShiftAmount);
        this->__operatorMultPartialProducts(old_result_low, old_result_high,
                                            src_shifted_low, src_shifted_high,
                                            multiplierBit);
        leftShiftAmount = 0;
      }
      leftShiftAmount++;
    }
    memItMultiplier++;
    emulatedBitNb += 4;
  }
  this->operator=(old_result_low);
  this->_operatorMultSetFlags(old_result_high, flags, is_signed);
  high = old_result_high;
  return (*this);
}

template <typename T>
auto    BitFieldArithmetic<T>::_operatorMultEqual_case_wrapper(BitFieldArithmetic<T> const &multiplier,
                                                               BitFieldArithmetic<T> &high,
                                                               t_bitFieldEflags &flags) -> BitFieldArithmetic<T> &
{
  __print_debug("");
  if (std::is_unsigned<T>::value)
    this->_operatorMultEqual(multiplier, high, flags);
  else
  {
    bitField::e_bitState    s1;
    bitField::e_bitState    s2;
    bitfield_t<T>        _mem;
    BitFieldArithmetic<T>    _multiplier_copy(&_mem.mem[0]);

    s1 = this->getSignBit();
    s2 = multiplier.getSignBit();
    _multiplier_copy = multiplier;
    if ((s1 == bitField::Zero || s1 == bitField::One) && s2 == bitField::Zero) // 00/10
      this->_operatorMultEqual(multiplier, high, flags);
    // The two following case are similar, but I prefer to keep it as simple
    // as possible (it is really easy to make this kind of condition hard to
    // understand) and avoid latter bugs, while letting the compiler optimise it.
    else if (s1 == bitField::Zero && s2 != bitField::Zero) // 01/0?/0U
    {
      _multiplier_copy.operatorMultEqual(*this, high, flags);
      this->operator=(_multiplier_copy);
    }
    else if ((s1 == bitField::Unknown || s1 == bitField::Undefined)
             && (s2 == bitField::Zero || s2 == bitField::One)) // ?0/?1/U0/U1
    {
      _multiplier_copy.operatorMultEqual(*this, high, flags);
      this->operator=(_multiplier_copy);
    }
    else if (s1 == bitField::One && s2 == bitField::One) // 11
    { // Using unsigned to avoid sign borderline case
      // Ex: (int8_t)-128 after 2complement is (int8_t)-128, which is wrong.
      BitFieldArithmetic<typename std::make_unsigned<T>::type>    _unsigned_multiplier_copy;
      BitFieldArithmetic<typename std::make_unsigned<T>::type>    _unsigned_this_copy;
      BitFieldArithmetic<typename std::make_unsigned<T>::type>    _unsigned_high;

      _unsigned_this_copy.setMem(this->getMem());
      _unsigned_multiplier_copy.setMem(_multiplier_copy.getMem());
      _unsigned_high.setMem(high.getMem());

      /* Unsigned */
      _unsigned_multiplier_copy.applyTwoComplement();
      _unsigned_this_copy.applyTwoComplement();
      _unsigned_this_copy._operatorMultEqual(_unsigned_multiplier_copy, _unsigned_high, flags, true);

      /* Signed (old not working version) */
      // _multiplier_copy.applyTwoComplement();
      // this->applyTwoComplement();
      // this->_operatorMultEqual(_multiplier_copy, high, flags);
    }
    else if ((s1 == bitField::Unknown || s1 == bitField::Undefined)
             && (s2 == bitField::Unknown || s2 == bitField::Undefined)) // 1?/1U and ??/?U/U?/UU
    {
      bitfield_t<T>        _mem2;
      BitFieldArithmetic<T>    _this_copy(&_mem2.mem[0]);
      bitField::e_bitState    mergePolicy;

      mergePolicy = std::max(s1, s2);
      _this_copy = *this;
      _multiplier_copy.set(this->getSignBitPos(), bitField::Zero);
      /* 00 case */
      this->set(this->getSignBitPos(), bitField::Zero);
      this->_operatorMultEqual(_multiplier_copy, high, flags); /* First result */
      /* 10 case */
      _this_copy.set(this->getSignBitPos(), bitField::One);
      _this_copy._operatorMultEqual(_multiplier_copy, high, flags); /* Second result */
      /* Merge with state priority (Undefined > Unknown > One > Zero) */
      this->merge(_this_copy, mergePolicy);
    }
    else // Safety else (should never get there)
    {
      assert(0);
    }
  }
  return (*this);
}

/*
** Wrapper for the 3param version method
*/
template <typename T>
auto    BitFieldArithmetic<T>::operatorMultEqual(BitFieldArithmetic<T> const &multiplier,
                                                 t_bitFieldEflags &flags) -> BitFieldArithmetic<T> &
{
  bitfield_t<T>        _mem;
  BitFieldArithmetic<T> high(&_mem.mem[0]);

  high = (T)0; // Avoid warning for non init variable
  __print_debug("");
  return (this->operatorMultEqual(multiplier, high, flags));
}

template <typename T>
auto    BitFieldArithmetic<T>::operatorMultEqual(BitFieldArithmetic<T> const &multiplier,
                                                 BitFieldArithmetic<T> &high,
                                                 t_bitFieldEflags &flags) -> BitFieldArithmetic<T> &
{
  __print_debug("");
  return (this->_manageOverlap(multiplier, high, flags, &BitFieldArithmetic<T>::_operatorMultEqual_case_wrapper));
}

/*
** Wrapper for the 3param version method
*/
template <typename T>
auto    BitFieldArithmetic<T>::operatorMultEqual(T multiplier, t_bitFieldEflags &flags) -> BitFieldArithmetic<T> &
{
  T    tmp_high;

  __print_debug("");
  tmp_high = 0;
  return (this->operatorMultEqual(multiplier, tmp_high, flags));
}

template <typename T>
auto    BitFieldArithmetic<T>::operatorMultEqual(T multiplier, T &high, t_bitFieldEflags &flags) -> BitFieldArithmetic<T> &
{
  bitfield_t<T>         _tmp_mem;
  bitfield_t<T>         _tmp_high;
  BitFieldArithmetic<T> _bitFieldVal(&_tmp_mem.mem[0]);
  BitFieldArithmetic<T> _bitFieldHighVal(&_tmp_high.mem[0]);

  __print_debug("");
  _bitFieldVal = multiplier;
  _bitFieldHighVal = high;
  this->operatorMultEqual(_bitFieldVal, _bitFieldHighVal, flags);
  high = _bitFieldHighVal.toInt();
  return (*this);
}
