// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//
/// @brief Implementation of the binary and (signed and unsigned) using a BitField.
///

#ifndef BITFIELD_ARITHMETIC_PART_HH_
# error "Only <BitFieldArithmetic.hh> can be included directly."
#endif /* !BITFIELD_ARITHMETIC_PART_HH_*/

/*!
** Truth table for the '&'(binary and) operator (OP1 and OP2)
** -------------------
** & | 0 | 1 | ? | U |    0 ==> Zero
** -------------------    1 ==> One
** 0 | 0 | 0 | 0 | 0 |    ? ==> Unknown
** -------------------    U ==> Undefined
** 1 |///| 1 | ? | U |
** -------------------
** ? |///|///| ? | U |
** -------------------
** U |///|///|///| U |
** -------------------
**
** Opti to avoid using a table:
** if (src.bit == Zero || dst.bit == Zero)
**    --> result.bit = Zero
** else // use state priority (Undefined > Unknown > Zero)
**    --> result = max(src.bit, dst.bit)
**
** There are no CF or OF for this operator.
*/

/*
** TODO: Tester les flags
*/
template <typename T>
auto    BitFieldArithmetic<T>::operator&=(BitFieldArithmetic<T> const &src) -> BitFieldArithmetic<T> &
{
  t_bitFieldEflags    flags; /* Unused */

  return (this->operatorAndEqual(src, flags));
}

template <typename T>
auto    BitFieldArithmetic<T>::operator&=(T src) -> BitFieldArithmetic<T> &
{
  t_bitFieldEflags    flags; /* Unused */

  return (this->operatorAndEqual(src, flags));
}

template <typename T>
auto    BitFieldArithmetic<T>::operatorAndEqual(BitFieldArithmetic<T> const &src,
                                                t_bitFieldEflags &flags) -> BitFieldArithmetic<T> &
{
  return (this->_manageOverlap(src, flags, &BitFieldArithmetic<T>::_operatorAndEqual));
}

template <typename T>
auto    BitFieldArithmetic<T>::operatorAndEqual(T val, t_bitFieldEflags &flags) -> BitFieldArithmetic<T> &
{
  T                _tmp_mem[2];
  BitFieldArithmetic<T>        _bitFieldVal(&_tmp_mem[0]);

  _bitFieldVal = val;
  return (this->operatorAndEqual(_bitFieldVal, flags));
}

/*
** Flags Affected (the same for and/or/xor)
** The OF and CF flags are cleared;
** the SF, ZF, and PF flags are set according to the result.
** The state of the AF flag is undefined.
**
** Internal Notes: For optimisation, we only write in memory if the value stored
**                 is different.
*/
template <typename T>
auto    BitFieldArithmetic<T>::_operatorAndEqual(BitFieldArithmetic<T> const &src,
                                                 t_bitFieldEflags &flags) -> BitFieldArithmetic<T> &
{
  unsigned int        i;
  bitField::e_bitState    b1;
  bitField::e_bitState    b2;

  flags.word = 0;
  for (i = 0 ; i < this->getEmulatedBitNb() ; i = i + 1)
  {
    b1 = this->get(i);
    b2 = src.get(i);
    if (b1 != bitField::Zero) // If zero, always zero
    {
      if (b2 == bitField::Zero)
        this->set(i, bitField::Zero);
      else // use state priority (Undefined > Unknown > Zero)
        this->set(i, std::max(b1, b2));
    }
  }
  flags.s_byte.OF = flags.s_byte.CF = bitField::Zero;
  this->_set_PFZFSF_flags(flags);
  flags.s_byte.AF = bitField::Undefined;
  return (*this);
}
