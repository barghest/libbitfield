// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//
/// @brief Implementation of the binary substraction using a BitField.
///

#ifndef BITFIELD_ARITHMETIC_PART_HH_
# error "Only <BitFieldArithmetic.hh> can be included directly."
#endif /* !BITFIELD_ARITHMETIC_PART_HH_*/

#include <cstring>
#include <algorithm>

/*!
** Truth table for
** the '-' operator
**            0 ==> Zero
** -------------------    1 ==> One
**   |   |   |   |   |    ? ==> Unknown
** + | 0 | 1 | ? | U |    U ==> Undefined
** -------------------
**   |  0|  1|  ?|  U| (carry)
** 0 | 0 | 1 | ? | U |
** -------------------
**   |  0|  0|  0|  0| (carry)
** 1 | 1 | 0 | ? | U |
** -------------------
**   |  0|  ?|  ?|  u| (carry)
** ? | ? | ? | ? | U |
** -------------------
**   |  0|  u|  u|  u| (carry)
** U | U | U | U | U |
** -------------------
**
** Different case:
**    { 0  0  1  0, ?, 0  u} (carry line)
** -- {0, 1, 1, ?, ?, U, U}  (result line)
** Formula:
*/

static bitField::e_bitState const _subTruthTable[][2] =
{
  /* {substraction result value, carry} */
  {bitField::Zero, bitField::Zero}, /* 00 .. */
  {bitField::One, bitField::One},
  {bitField::Unknown, bitField::Unknown},
  {bitField::Undefined, bitField::Undefined},

  {bitField::One, bitField::Zero}, /* 01 .. */
  {bitField::Zero, bitField::Zero},
  {bitField::Unknown, bitField::Zero},
  {bitField::Undefined, bitField::Zero},

  {bitField::Unknown, bitField::Zero}, /* 10 .. */
  {bitField::Unknown, bitField::Unknown},
  {bitField::Unknown, bitField::Unknown},
  {bitField::Undefined, bitField::Undefined},

  {bitField::Undefined, bitField::Zero}, /* 11 .. */
  {bitField::Undefined, bitField::Undefined},
  {bitField::Undefined, bitField::Undefined},
  {bitField::Undefined, bitField::Undefined},
};

template <typename T>
auto    BitFieldArithmetic<T>::_getSubVal(bitField::e_bitState op1,
                                          bitField::e_bitState op2) const -> bitField::e_bitState
{
  return (_subTruthTable[(op1 << 2) | op2][0]);
}

template <typename T>
auto    BitFieldArithmetic<T>::_getSubCarry(bitField::e_bitState op1,
                                            bitField::e_bitState op2) const -> bitField::e_bitState
{
  return (_subTruthTable[(op1 << 2) | op2][1]);
}

/*
** TODO: tester les flags
*/
template <typename T>
auto    BitFieldArithmetic<T>::_operatorSubSpreadCarry(unsigned int byte,
                                                       unsigned int bit,
                                                       bitField::e_bitState oldCarry,
                                                       t_bitFieldEflags &flags) -> void
{
  unsigned int          nextByte;
  unsigned int          nextBit;
  bitField::e_bitState  value;
  bitField::e_bitState  carry;

  if ((byte * 4 + bit) == this->getEmulatedBitNb() && oldCarry > flags.s_byte.CF) /* CF */
    flags.s_byte.CF = oldCarry;
  if ((byte * 4 + bit) == 4 && oldCarry > flags.s_byte.AF) /* Auxiliary Carry Flag (AF) */
    flags.s_byte.AF = oldCarry;
  if (oldCarry != bitField::Zero && byte < this->size())
  {
    nextBit = (bit + 1) & 0x3;
    nextByte = byte + (((bit + 1) >> 2) & 0x1);
    value = _getSubVal(this->getStructMem()[byte].getBit(bit), oldCarry);
    carry = _getSubCarry(this->getStructMem()[byte].getBit(bit), oldCarry);
    this->getStructMem()[byte].setBit(bit, value);
    _operatorSubSpreadCarry(nextByte, nextBit, carry, flags);
  }
}

template <typename T>
auto    BitFieldArithmetic<T>::_operatorSubRecur(BitFieldArithmetic<T> const &src,
                                                 unsigned int byte,
                                                 unsigned int bit,
                                                 t_bitFieldEflags &flags) -> void
{
  unsigned int          nextByte;
  unsigned int          nextBit;
  bitField::e_bitState  value;
  bitField::e_bitState  carry;
  bitField::e_bitState  srcBit;
  bitField::e_bitState  dstBit;

  nextBit = (bit + 1) & 0x3;
  nextByte = byte + (((bit + 1) >> 2) & 0x1);
  if (byte < this->size())
  {
    /* (bit_dst - oldcarry) - bit_src */
    dstBit = this->getStructMem()[byte].getBit(bit);
    srcBit = src.getStructMem()[byte].getBit(bit);

    value = _getSubVal(dstBit, srcBit);
    carry = _getSubCarry(dstBit, srcBit);
    this->getStructMem()[byte].setBit(bit, value);
    _operatorSubSpreadCarry(nextByte, nextBit, carry, flags);
    _operatorSubRecur(src, nextByte, nextBit, flags);
  }
}


//    SUBTRACTION SIGN BITS (For overflow/Underflow)
//  num1sign num2sign sumsign
// ---------------------------
//        0 0 0
//        0 0 1
//        0 1 0
// *OVER* 0 1 1 (subtracting a negative is the same as adding a positive)
// *OVER* 1 0 0 (subtracting a positive is the same as adding a negative)
//        1 0 1
//        1 1 0
//        1 1 1

/*
** Flags Affected
** The OF, SF, ZF, AF, PF, and CF flags are set according to the result.
** TODO: faire des tests plus pousses pour tester les flags de chaque operation
*/
template <typename T>
auto    BitFieldArithmetic<T>::_operatorSubEqual(BitFieldArithmetic<T> const &src,
                                                 t_bitFieldEflags &flags) -> BitFieldArithmetic<T> &
{
  bitField::e_bitState    s1; /* Op1 sign */
  bitField::e_bitState    s2; /* Op2 sign */
  bitField::e_bitState    s3; /* Result sign */

  flags.word = 0;
  s1 = this->get(this->getSignBitPos());
  s2 = src.get(src.getSignBitPos());
  this->_operatorSubRecur(src, 0, 0, flags);
  this->_set_PFZFSF_flags(flags);
  s3 = this->get(this->getSignBitPos());
  flags.s_byte.OF = (s1 != bitField::One && s2 != bitField::Zero && s3 != bitField::Zero)
    || (s1 != bitField::Zero && s2 != bitField::One && s3 != bitField::One);
  if (flags.s_byte.OF) /* If we have an overflow, used state priority (Undefined > Unknown > One) */
    flags.s_byte.OF = std::max(std::max(s1, s2), s3);
  return (*this);
}

/***************************/

template <typename T>
auto    BitFieldArithmetic<T>::operator-=(BitFieldArithmetic<T> const &src) -> BitFieldArithmetic<T> &
{
  bitfield_t<T>            _tmp_mem;
  BitFieldArithmetic<T>        _bitFieldVal(&_tmp_mem.mem[0]);

  _bitFieldVal = src;
  _bitFieldVal.applyTwoComplement();
  return (this->operator+=(_bitFieldVal));
}

template <typename T>
auto    BitFieldArithmetic<T>::operator-=(T val) -> BitFieldArithmetic<T> &
{
  /*
  ** We can not use unari minus operator on unsigned number, we force
  ** the use of signed two byte complement for the following purpose:
  ** As (a - b) has a result equal to (a + -b) we use 2 byte complement
  ** and the operator+() (which is faster) on the casted sign type.
  */
  return (this->operator+=((~val) + 1));
}

/**************************/

template <typename T>
auto    BitFieldArithmetic<T>::operatorSubEqual(T val,
                                                t_bitFieldEflags &flags) -> BitFieldArithmetic<T> &
{
  bitfield_t<T>         _tmp_mem;
  BitFieldArithmetic<T> _bitFieldVal(&_tmp_mem.mem[0]);

  _bitFieldVal = val;
  return (this->operatorSubEqual(_bitFieldVal, flags));
}

template <typename T>
auto    BitFieldArithmetic<T>::operatorSubEqual(BitFieldArithmetic<T> const &src,
                                                t_bitFieldEflags &flags) -> BitFieldArithmetic<T> &
{
  return (this->_manageOverlap(src, flags, &BitFieldArithmetic<T>::_operatorSubEqual));
}
