// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//

# include "bitField/BitFieldArithmetic/BitFieldArithmeticEflags.hh"

namespace bitField
{
  namespace BitFieldArithmetic
  {

    /*
    ** Truth table (A == B):
    ** -------------------
    **  \B| 0 | 1 | ? | U |
    **  A\|   |   |   |   |
    ** -------------------
    **  0 | 1 | 0 | ? | U |
    ** -------------------
    **  1 | 0 | 1 | ? | U |
    ** -------------------
    **  ? | ? | ? | ? | U |
    ** -------------------
    **  U | U | U | U | U |
    ** -------------------
    */
    static bitField::e_bitState const        _truthTable_A_eq_B[] =
    {
      bitField::One,        bitField::Zero,         bitField::Unknown,      bitField::Undefined,
      bitField::Zero,       bitField::One,          bitField::Unknown,      bitField::Undefined,
      bitField::Unknown,    bitField::Unknown,      bitField::Unknown,      bitField::Undefined,
      bitField::Undefined,  bitField::Undefined,    bitField::Undefined,    bitField::Undefined,
    };

    /*
    ** Truth table (A != B):
    ** -------------------
    **  \B| 0 | 1 | ? | U |
    **  A\|   |   |   |   |
    ** -------------------
    **  0 | 0 | 1 | ? | U |
    ** -------------------
    **  1 | 1 | 0 | ? | U |
    ** -------------------
    **  ? | ? | ? | ? | U |
    ** -------------------
    **  U | U | U | U | U |
    ** -------------------
    */
    static bitField::e_bitState const        _truthTable_A_neq_B[] =
    {
      bitField::Zero,       bitField::One,          bitField::Unknown,      bitField::Undefined,
      bitField::One,        bitField::Zero,         bitField::Unknown,      bitField::Undefined,
      bitField::Unknown,    bitField::Unknown,      bitField::Unknown,      bitField::Undefined,
      bitField::Undefined,  bitField::Undefined,    bitField::Undefined,    bitField::Undefined,
    };

    /*
    ** Truth table (A=0 && B=0).
    ** -------------------
    ** \   |   |   |   |   |
    **  \B | 0 | 1 | ? | U |
    **  A\ |   |   |   |   |
    ** --------------------
    **  0  | 1 | 0 | ? | U |
    ** --------------------
    **  1  | 0 | 0 | 0 | 0 |
    ** --------------------
    **  ?  | ? | 0 | ? | U |
    ** --------------------
    **  U  | U | 0 | U | U |
    ** --------------------
    */
    static bitField::e_bitState const        _truthTable_A0_and_B0[] =
    {
      bitField::One,        bitField::Zero,    bitField::Unknown,   bitField::Undefined,
      bitField::Zero,       bitField::Zero,    bitField::Zero,      bitField::Zero,
      bitField::Unknown,    bitField::Zero,    bitField::Unknown,   bitField::Undefined,
      bitField::Undefined,  bitField::Zero,    bitField::Undefined, bitField::Undefined,
    };

    /*
    ** Truth table (A=1 && B=1).
    ** -------------------
    ** \   |   |   |   |   |
    **  \B | 0 | 1 | ? | U |
    **  A\ |   |   |   |   |
    ** --------------------
    **  0  | 0 | 0 | 0 | 0 |
    ** --------------------
    **  1  | 0 | 1 | ? | U |
    ** --------------------
    **  ?  | 0 | ? | ? | U |
    ** --------------------
    **  U  | 0 | U | U | U |
    ** --------------------
    */
    static bitField::e_bitState const        _truthTable_A_and_B[] =
    {
      bitField::Zero,    bitField::Zero,        bitField::Zero,         bitField::Zero,
      bitField::Zero,    bitField::One,         bitField::Unknown,      bitField::Undefined,
      bitField::Zero,    bitField::Unknown,     bitField::Unknown,      bitField::Undefined,
      bitField::Zero,    bitField::Undefined,   bitField::Undefined,    bitField::Undefined,
    };

    /*
    ** Truth table (A=1 || B=1).
    ** -------------------
    ** \   |   |   |   |   |
    **  \B | 0 | 1 | ? | U |
    **  A\ |   |   |   |   |
    ** --------------------
    **  0  | 0 | 1 | ? | U |
    ** --------------------
    **  1  | 1 | 1 | 1 | 1 |
    ** --------------------
    **  ?  | ? | 1 | ? | U |
    ** --------------------
    **  U  | U | 1 | U | U |
    ** --------------------
    */
    static bitField::e_bitState const        _truthTable_A_or_B[] =
    {
      bitField::Zero,       bitField::One,    bitField::Unknown,    bitField::Undefined,
      bitField::One,        bitField::One,    bitField::One,        bitField::One,
      bitField::Unknown,    bitField::One,    bitField::Unknown,    bitField::Undefined,
      bitField::Undefined,  bitField::One,    bitField::Undefined,  bitField::Undefined,
    };

/*******************/

    static inline auto _getTwoDimIndex(bitField::e_bitState a, bitField::e_bitState b) -> unsigned int
    {
      return (static_cast<unsigned int>((a << 2) | b));
    }

    auto eflags_isFalse(bitField::e_bitState flag) -> bitField::e_bitState {
      return (_truthTable_A_eq_B[_getTwoDimIndex(flag, bitField::Zero)]);
    }
    auto eflags_isTrue(bitField::e_bitState flag) -> bitField::e_bitState {
      return (_truthTable_A_eq_B[_getTwoDimIndex(flag, bitField::One)]);
    }

    auto eflags_eq(bitField::e_bitState a, bitField::e_bitState b) -> bitField::e_bitState {
      return (_truthTable_A_eq_B[_getTwoDimIndex(a, b)]);
    }
    auto eflags_neq(bitField::e_bitState a, bitField::e_bitState b) -> bitField::e_bitState {
      return (_truthTable_A_neq_B[_getTwoDimIndex(a, b)]);
    }
    auto eflags_and(bitField::e_bitState a, bitField::e_bitState b) -> bitField::e_bitState {
      return (_truthTable_A_and_B[_getTwoDimIndex(a, b)]);
    }
    auto eflags_or(bitField::e_bitState a, bitField::e_bitState b) -> bitField::e_bitState {
      return (_truthTable_A_or_B[_getTwoDimIndex(a, b)]);
    }

/***********************************************************/
/* Unsigned */

    /*
    ** JA Jump short if above (CF=0 and ZF=0).
    */
    auto eflags_JA(t_bitFieldEflags const &eflags) -> bitField::e_bitState{
      return (_truthTable_A0_and_B0[_getTwoDimIndex((bitField::e_bitState)eflags.s_byte.ZF,
                                                    (bitField::e_bitState)eflags.s_byte.CF)]);
    }
    /*
    ** JB Jump short if below (CF=1).
    */
    auto eflags_JB(t_bitFieldEflags const &eflags) -> bitField::e_bitState {
      return (eflags_eq((bitField::e_bitState)eflags.s_byte.CF, bitField::One));
    }
    /*
    ** JAE Jump short if above or equal (CF=0).
    */
    auto eflags_JAE(t_bitFieldEflags const &eflags) -> bitField::e_bitState {
      return (eflags_eq((bitField::e_bitState)eflags.s_byte.CF, bitField::Zero));
    }
    /*
    ** JBE Jump short if below or equal (CF=1 or ZF=1).
    */
    auto eflags_JBE(t_bitFieldEflags const &eflags) -> bitField::e_bitState {
      return (eflags_or((bitField::e_bitState)eflags.s_byte.CF, (bitField::e_bitState)eflags.s_byte.ZF));
    }


/***********************************************************/
/* Signed */

    /*
    ** JG Jump near if greater (ZF=0 and SF=OF).
    */
    auto eflags_JG(t_bitFieldEflags const &eflags) -> bitField::e_bitState
    {
      bitField::e_bitState    ret;
      bitField::e_bitState    isZF_zero;
      bitField::e_bitState    areSF_OF_equal;

      isZF_zero = eflags_eq((bitField::e_bitState)eflags.s_byte.ZF, bitField::Zero);
      areSF_OF_equal = eflags_eq((bitField::e_bitState)eflags.s_byte.SF,
                                 (bitField::e_bitState)eflags.s_byte.OF);
      ret = eflags_and((bitField::e_bitState)isZF_zero,
                       (bitField::e_bitState)areSF_OF_equal);
      return (ret);
    }
    /*
    ** JL Jump short if less (SF != OF).
    */
    auto eflags_JL(t_bitFieldEflags const &eflags) -> bitField::e_bitState {
      return (eflags_neq((bitField::e_bitState)eflags.s_byte.SF, (bitField::e_bitState)eflags.s_byte.OF));
    }
    /*
    ** JGE Jump short if greater or equal (SF=OF).
    */
    auto eflags_JGE(t_bitFieldEflags const &eflags) -> bitField::e_bitState {
      return (eflags_eq((bitField::e_bitState)eflags.s_byte.SF, (bitField::e_bitState)eflags.s_byte.OF));
    }
    /*
    ** JLE Jump short if less or equal (ZF=1 or SF != OF).
    */
    auto eflags_JLE(t_bitFieldEflags const &eflags) -> bitField::e_bitState
    {
      bitField::e_bitState    ret;
      bitField::e_bitState    isZF_one;
      bitField::e_bitState    areSF_OF_diff;

      isZF_one = eflags_eq((bitField::e_bitState)eflags.s_byte.ZF, bitField::One);
      areSF_OF_diff = eflags_neq((bitField::e_bitState)eflags.s_byte.SF, (bitField::e_bitState)eflags.s_byte.OF);
      ret = eflags_or((bitField::e_bitState)isZF_one, (bitField::e_bitState)areSF_OF_diff);
      return (ret);
    }

  } // !namespace BitFieldArithmetic
} //! namespace bitField
