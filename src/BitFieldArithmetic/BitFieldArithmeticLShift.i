// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//
/// @brief Implementation of the binary left shift (a <<= b)
///        using a BitField.
///
/// * Intel manual short description (SAL/SAR/SHL/SHR--Shift (Instruction set reference)):
/// Shifts the bits in the first operand (destination operand) to the left or right by the number of bits
/// specified in the second operand (count operand). Bits shifted beyond the destination operand boundary
/// are first shifted into the CF flag, then discarded. At the end of the shift operation, the CF flag
/// contains the last bit shifted out of the destination operand.
///
/// The destination operand can be a register or a memory location. The count operand can be an immediate
/// value or the CL register. The count is masked to 5 bits (or 6 bits if in 64-bit mode and REX.W is used).
/// The count range is limited to 0 to 31 (or 63 if 64-bit mode and REX.W is used).
/// A special opcode encoding is provided for a count of 1.
///
/// * Extra note (not documented in the intel man):
/// When doing left shift (logic and arithmetic), the number use a sign multiplication.
/// So if we do ((int32_t)a << -1), knowing the shift count is mask on 5 bit we get 31. (a << 31)
///

#include <string>

#ifndef BITFIELD_ARITHMETIC_PART_HH_
# error "Only <BitFieldArithmetic.hh> can be included directly."
#endif /* !BITFIELD_ARITHMETIC_PART_HH_*/

template <typename T>
auto    BitFieldArithmetic<T>::operator<<=(BitFieldArithmetic<T> const &src) -> BitFieldArithmetic<T> &
{
  t_bitFieldEflags    flags;

  __print_debug("");
  flags.word = 0; // To make valgrind happy (does not influence the result)
  return (this->operatorLShiftEqual(src, flags));
}

template <typename T>
auto    BitFieldArithmetic<T>::operator<<=(unsigned int shift) -> BitFieldArithmetic<T>    &
{
  t_bitFieldEflags    flags;

  __print_debug("");
  flags.word = 0;
  return (this->operatorLShiftEqual(shift, flags));
}

// template <typename T>
// BitFieldArithmetic<T>    BitFieldArithmetic<T>::operator<<(unsigned int shift) const
// {
//   // BitFieldArithmetic<T>    ret;

//   // ret = this;
//   // ret <<= shift;
//   // return (ret);
//   assert(0);
//   return (*this);
// }

// template <typename T>
// BitFieldArithmetic<T>    BitFieldArithmetic<T>::operator<<(BitFieldArithmetic<T> const &other) const
// {
//   BitFieldArithmetic<T>    ret;

//   assert(0);
//   return (ret);
// }

/*
** TODO: optimisation: Faire une opti pour le cas ou aucun bit n'est scalaire (cas courant)
*/
template <typename T>
auto    BitFieldArithmetic<T>::_operatorLShiftEqual(BitFieldArithmetic<T> const &shift,
                                                    t_bitFieldEflags &flags) -> BitFieldArithmetic<T> &
{
  __print_debug("");
  return (this->_operatorShiftEqual(shift, flags, &BitFieldArithmetic<T>::operatorLShiftEqual));
}

/*
** Proxy function between all high LShift function and the one really
** performing arithmetic (_operatorLShiftEqual<T>()). The overlap is managed.
*/
template <typename T>
auto    BitFieldArithmetic<T>::operatorLShiftEqual(BitFieldArithmetic<T> const &shift,
                                                   t_bitFieldEflags &flags) -> BitFieldArithmetic<T> &
{
  __print_debug("");
  return (this->_manageOverlap(shift, flags, &BitFieldArithmetic<T>::_operatorLShiftEqual));
}

/*
** Reverse the bit order of this, perform a right shift and then set flags.
*/
template <typename T>
auto    BitFieldArithmetic<T>::operatorLShiftEqual(unsigned int shift,
                                                   t_bitFieldEflags &flags) -> BitFieldArithmetic<T> &
{
  bitfield_t<T>                            _mem;
  BitFieldArithmetic<typename std::make_unsigned<T>::type>    unsigned_this_copy(&_mem.mem[0]);
  BitFieldArithmetic<T>                        new_this(&_mem.mem[0]);

  __print_debug("lshift by " + std::to_string(shift));
  new_this = *this;
  unsigned_this_copy.reverse();
  unsigned_this_copy >>= shift;
  unsigned_this_copy.reverse();
  this->_operatorLShiftSetFlags(new_this, shift, flags);
  this->operator=(new_this);
  return (*this);
}

/*
** TODO: tester le CF pour SAL et SAR (comment est defini la valeur)
**
** Flags Affected (SAL/SAR/SHL/SHR--Shift) (Intel manual)
** The CF flag contains the value of the last bit shifted out of the destination
** operand; it is undefined for SHL and SHR instructions where the count is
** greater than or equal to the size (in bits) of the destination operand.
**
** The OF flag is affected only for 1-bit shifts, otherwise, it is undefined.
**
** The SF, ZF, and PF flags are set according to the result. If the count is 0,
** the flags are not affected.
**
** For a non-zero count, the AF flag is undefined.
**
** OF flag details:
** For left shifts, the OF flag is set to 0 if the most-significant bit of the
** result is the same as the CF flag (that is, the top two bits of the original
** operand were the same); otherwise, it is set to 1.
*/
template <typename T>
auto    BitFieldArithmetic<T>::_operatorLShiftSetFlags(BitFieldArithmetic<T> const &new_this,
                                                       unsigned int shift,
                                                       t_bitFieldEflags &flags) const -> void
{
  this->_operatorShiftSetFlags(new_this, shift, flags, false /* bool right_shift */);
  // bitField::e_bitState    result_msb;

  // if (shift > 0)
  // {
  //   flags.s_byte.CF = bitField::Undefined;
  //   if (shift <= this->getEmulatedBitNb() && shift < this->getEmulatedBitNb())
  //     flags.s_byte.CF = this->get(this->getEmulatedBitNb() - shift);

  //   if (shift == 1) // The OF flag is affected only for 1-bit shifts
  //   {
  //     result_msb = new_this.getSignBit();
  //     if (bitField::isScalar(flags.s_byte.CF)
  //       && bitField::isScalar(result_msb))
  //     flags.s_byte.OF = (flags.s_byte.CF != result_msb);
  //     else
  //     flags.s_byte.OF = std::max((bitField::e_bitState)flags.s_byte.CF,
  //                       (bitField::e_bitState)result_msb);
  //   }
  //   else
  //     flags.s_byte.OF = bitField::Undefined;
  //   flags.s_byte.AF = bitField::Undefined; // if (shift != 0) --> Undefined
  //   new_this._set_PFZFSF_flags(flags);
  // }
  // return ;
}

/*
** Internal notes:
**   An assert has been used in order to add a safety NULL and overlap check.
**
**   Intel assembly mask the shift size to 5bits on 32b and 6bits on 64b.
**   As we only need to get bits moved beyond the last bit and not
**   recode a custom shift for large type, we use the same limit.
**   Keep in mind that this function can at most work with two 64b emulated
**   variable, this correspond to perform a shift on a 256bytes memory.
**   (64 emulated size * 2 ==> 128 emulated size ==> 256 real size)
**
**   We are converting high (signed) to _unsigned_high (unsigned) to force
**   the right shift to be logical (and not arithmetic), so zero are always inserted.
** TODO: faire le rshiftExtended
*/
template <typename T>
auto    BitFieldArithmetic<T>::operatorLShiftEqualExtented(BitFieldArithmetic<T> &high,
                                                           unsigned int shift) throw (std::invalid_argument) -> BitFieldArithmetic<T> &
{
  unsigned int    rshift;
  T        mask;
  bitfield_t<T>                            _tmp_mem;
  BitFieldArithmetic<typename std::make_unsigned<T>::type>    _upper_bits(&_tmp_mem.mem[0]);
  BitFieldArithmetic<typename std::make_unsigned<T>::type>    _unsigned_high(high.getMem());
  BitFieldArithmetic<typename std::make_unsigned<T>::type>    _unsigned_this(this->getMem());

  __print_debug("");
  if (shift > 0) // Optimisation
  {
    if (overlap(*this, high) == false)
    {
      assert(high.getMem() && this->getMem());
      // Remainder: Intel has a mask for 32b and 64b type, not 8 and 16 (like the following line)
      // mask = this->getEmulatedBitNb() - 1; // Ex: 16 -> [0-15] -> 0b01111
      mask = (sizeof(T) == 8 ? 63 /* 0x3F */ : 31 /* 0x1F */);
      shift &= mask;
      rshift = ((this->getEmulatedBitNb()) - shift) & mask;
      _upper_bits.operator=(_unsigned_this);
      _upper_bits.operator>>=(rshift);

      _unsigned_high.operator<<=(shift);
      _unsigned_high.operator|=(_upper_bits);

      this->operator<<=(shift);
    }
    else /* This and high overlap */
      throw std::invalid_argument ("BitFieldArithmetic<T>::_manageOverlap(...): "
                                   "this and high are overlaping.");
  }
  return (*this);
}
