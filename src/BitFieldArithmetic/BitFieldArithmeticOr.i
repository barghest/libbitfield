// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//
/// @brief Implementation of the binary or (signed and unsigned) using a BitField.
///

#ifndef BITFIELD_ARITHMETIC_PART_HH_
# error "Only <BitFieldArithmetic.hh> can be included directly."
#endif /* !BITFIELD_ARITHMETIC_PART_HH_*/

/*!
** Truth table for the '|'(binary or) operator (OP1 and OP2)
** -------------------
** or| 0 | 1 | ? | U |    0 ==> Zero
** -------------------    1 ==> One
** 0 | 0 | 1 | ? | U |    ? ==> Unknown
** -------------------    U ==> Undefined
** 1 |///| 1 | 1 | 1 |
** -------------------
** ? |///|///| ? | U |
** -------------------
** U |///|///|///| U |
** -------------------
**
** Opti to avoid using a table:
** if (src.bit == One || dst.bit == One)
**    --> result.bit = One
** else // use state priority (Undefined > Unknown > Zero)
**    --> result.bit = max(src.bit, dst.bit)
**
** There are no CF or OF for this operator.
*/

/*
** TODO: Tester les flags
*/
template <typename T>
auto    BitFieldArithmetic<T>::operator|=(BitFieldArithmetic<T> const &src) -> BitFieldArithmetic<T> &
{
  t_bitFieldEflags    flags; /* Unused */

  return (this->operatorOrEqual(src, flags));
}

template <typename T>
auto    BitFieldArithmetic<T>::operator|=(T src) -> BitFieldArithmetic<T> &
{
  t_bitFieldEflags    flags; /* Unused */

  return (this->operatorOrEqual(src, flags));
}

template <typename T>
auto    BitFieldArithmetic<T>::operatorOrEqual(BitFieldArithmetic<T> const &src,
                                               t_bitFieldEflags &flags) -> BitFieldArithmetic<T> &
{
  return (this->_manageOverlap(src, flags, &BitFieldArithmetic<T>::_operatorOrEqual));
}

template <typename T>
auto    BitFieldArithmetic<T>::operatorOrEqual(T val, t_bitFieldEflags &flags) -> BitFieldArithmetic<T> &
{
  bitfield_t<T>            _tmp_mem;
  BitFieldArithmetic<T>        _bitFieldVal(&_tmp_mem.mem[0]);

  _bitFieldVal = val;
  return (this->operatorOrEqual(_bitFieldVal, flags));
}

/*
** Flags Affected (the same for and/or/xor)
** The OF and CF flags are cleared;
** the SF, ZF, and PF flags are set according to the result.
** The state of the AF flag is undefined.
**
** Internal Notes: For optimisation, we only write in memory if the value stored
**                 is different.
*/
template <typename T>
auto    BitFieldArithmetic<T>::_operatorOrEqual(BitFieldArithmetic<T> const &src,
                                                t_bitFieldEflags &flags) -> BitFieldArithmetic<T> &
{
  unsigned int        i;
  bitField::e_bitState    b1;
  bitField::e_bitState    b2;

  flags.word = 0;
  for (i = 0 ; i < this->getEmulatedBitNb() ; i = i + 1)
  {
    b1 = this->get(i);
    b2 = src.get(i);
    if (b1 != bitField::One) // If one, always one
    {
      if (b2 == bitField::One)
        this->set(i, bitField::One);
      else // Use state priority (Undefined > Unknown > Zero)
        this->set(i, std::max(b1, b2));
    }
  }
  flags.s_byte.OF = flags.s_byte.CF = bitField::Zero;
  this->_set_PFZFSF_flags(flags);
  flags.s_byte.AF = bitField::Undefined;
  return (*this);
}
