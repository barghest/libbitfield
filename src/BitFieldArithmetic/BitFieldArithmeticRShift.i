// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//
/// @brief Implementation of the binary left shift (a <<= b)
///        using a BitField.
///
/// * Intel manual short description (SAL/SAR/SHL/SHR--Shift (Instruction set reference)):
/// Shifts the bits in the first operand (destination operand) to the left or right by the number of bits
/// specified in the second operand (count operand). Bits shifted beyond the destination operand boundary
/// are first shifted into the CF flag, then discarded. At the end of the shift operation, the CF flag
/// contains the last bit shifted out of the destination operand.
///
/// The destination operand can be a register or a memory location. The count operand can be an immediate
/// value or the CL register. The count is masked to 5 bits (or 6 bits if in 64-bit mode and REX.W is used).
/// The count range is limited to 0 to 31 (or 63 if 64-bit mode and REX.W is used).
/// A special opcode encoding is provided for a count of 1.
///
/// * Extra note (not documented in the intel man):
/// When doing left shift (logic and arithmetic), the number use a sign multiplication.
/// So if we do ((int32_t)a << -1), knowing the shift count is mask on 5 bit we get 31. (a << 31)
///

// #include <type_traits>

#ifndef BITFIELD_ARITHMETIC_PART_HH_
# error "Only <BitFieldArithmetic.hh> can be included directly."
#endif /* !BITFIELD_ARITHMETIC_PART_HH_*/

template <typename T>
auto    BitFieldArithmetic<T>::operator>>=(BitFieldArithmetic<T> const &src) -> BitFieldArithmetic<T> &
{
  t_bitFieldEflags    flags;

  __print_debug("");
  return (this->operatorRShiftEqual(src, flags));
}

template <typename T>
auto    BitFieldArithmetic<T>::operator>>=(unsigned int shift) -> BitFieldArithmetic<T>    &
{
  t_bitFieldEflags    flags;

  __print_debug("");
  return (this->operatorRShiftEqual(shift, flags));
}

/*
** Flags Affected (SAL/SAR/SHL/SHR--Shift) (Intel manual)
** The CF flag contains the value of the last bit shifted out of the destination
** operand; it is undefined for SHL and SHR instructions where the count is
** greater than or equal to the size (in bits) of the destination operand.
**
** The OF flag is affected only for 1-bit shifts (see "Description" above);
** otherwise, it is undefined.
**
** The SF, ZF, and PF flags are set according to the result. If the count is 0,
** the flags are not affected.
**
** For a non-zero count, the AF flag is undefined.
*/
// TODO: tester les shift intertype (char avec un shift de 7/8/9, short avec 7/8/9 et 15/16/17, etc)

// template <typename T>
// BitFieldArithmetic<T>    BitFieldArithmetic<T>::operator>>(unsigned int shift) const
// {
//   // BitFieldArithmetic<T>    ret;

//   // ret = this;
//   // ret <<= shift;
//   // return (ret);
//   __print_debug("");
//   (void)shift;
//   assert(0);
//   return (*this);
// }

// template <typename T>
// BitFieldArithmetic<T>    BitFieldArithmetic<T>::operator>>(BitFieldArithmetic<T> const &other) const
// {
//   BitFieldArithmetic<T>    ret;

//   // TODO: ajouter l'overlap
//   __print_debug("");
//   (void)other;
//   assert(0);
//   return (ret);
// }

template <typename T>
auto    BitFieldArithmetic<T>::_operatorRShiftEqual(BitFieldArithmetic<T> const &shift,
                                                    t_bitFieldEflags &flags) -> BitFieldArithmetic<T> &
{
  return (this->_operatorShiftEqual(shift, flags, &BitFieldArithmetic<T>::operatorRShiftEqual));
  // T            max;
  // bool            isShiftedValTroncated;
  // unsigned int        i;
  // size_t        nb_item;
  // T            _mem[3][2];
  // BitFieldArithmetic<T>    _shift_copy(&_mem[0][0]);
  // BitFieldArithmetic<T>    _this_copy(&_mem[1][0]);
  // BitFieldArithmetic<T>    _result(&_mem[2][0]);
  // T            emulatedValues[(sizeof(T) == 8 ? 64 : 32)];
  // unsigned int    const    array_size = sizeof(emulatedValues) / sizeof(*emulatedValues);
  // t_bitFieldEflags    _flagsShift;
  // t_bitFieldEflags    _flagsMerge;
  // unsigned int        _step_shift; // Each seperated shift

  // _this_copy = *this;
  // _flagsShift.word = flags.word;
  // _flagsMerge.word = flags.word;
  // max = shift.getMax();
  // isShiftedValTroncated = (max > (sizeof(T) == 8 ? 63 : 31));
  // if (isShiftedValTroncated) // Mask to respect intel shift limitation
  // {
  //   _shift_copy = shift;
  //   _shift_copy &= (sizeof(T) == 8 ? 63 /* 0x3F */ : 31 /* 0x1F */ );
  // }
  // BitFieldArithmetic<T>    const _shift_const((isShiftedValTroncated ?
  //                       (_shift_copy.getMem()) : (shift.getMem())));

  // nb_item = _shift_const.getValues(&emulatedValues[0], array_size);
  // assert(nb_item > 0);
  // _result = (_this_copy.operatorRShiftEqual(emulatedValues[0], _flagsMerge));
  // for (i = 1; i < nb_item; i++) // For each scalar shift
  // {
  //   assert(i < array_size);
  //   // Get the next shift possibility and add "isShiftedValTroncated" at bit 7,
  //   // above the 6b mask in 64b to force the shift to take into account that
  //   // shift<T> is truncated. This has been done in order to ensure eflags are
  //   // set correctly to bitField::Undefined if needed.
  //   _step_shift = (emulatedValues[i] - emulatedValues[i - 1]) | (isShiftedValTroncated << 7);
  //   _this_copy.operatorRShiftEqual(_step_shift, _flagsShift);
  //   _result.merge(_this_copy, bitField::Unknown); // TODO: Changer la strategy du merge
  //   _flagsMerge.merge(_flagsShift);
  // }
  // flags.word = _flagsMerge.word;
  // return (this->operator=(_result));
}

/*
** Proxy function between all high RShift function and the one really
** performing arithmetic (_operatorRShiftEqual<T>()). The overlap is managed.
*/
template <typename T>
auto    BitFieldArithmetic<T>::operatorRShiftEqual(BitFieldArithmetic<T> const &src,
                                                   t_bitFieldEflags &flags) -> BitFieldArithmetic<T> &
{
  __print_debug("");
  return (this->_manageOverlap(src, flags, &BitFieldArithmetic<T>::_operatorRShiftEqual));
}

template <typename T>
auto    BitFieldArithmetic<T>::operatorRShiftEqual(unsigned int shift,
                                                   t_bitFieldEflags &flags) -> BitFieldArithmetic<T> &
{
  unsigned int                shift_unmasked;
  bitfield_t<T>                _this_copy_mem;
  BitFieldArithmetic<T>            _this_copy(&_this_copy_mem.mem[0]);

  __print_debug("rshift by " + std::to_string(shift));
  if (shift)
  {
    shift_unmasked = shift;
    shift &= (sizeof(T) == 8 ? 63 : 31);
    _this_copy = *this;
    _this_copy._operatorRShift_native(shift);
    this->_operatorRShiftSetFlags(_this_copy, shift_unmasked, flags);
    return (this->operator=(_this_copy));
  }
  return (*this);
}

/*
** Flags Affected (SAL/SAR/SHL/SHR--Shift) (Intel manual)
** The CF flag contains the value of the last bit shifted out of the destination
** operand; it is undefined for SHL and SHR instructions where the count is
** greater than or equal to the size (in bits) of the destination operand.
**
** The OF flag is affected only for 1-bit shifts, otherwise, it is undefined.
**
** The SF, ZF, and PF flags are set according to the result. If the count is 0,
** the flags are not affected.
**
** For a non-zero count, the AF flag is undefined.
**
** OF flag details:
** (Signed) For the SAR instruction, the OF flag is cleared for all 1-bit shifts.
** (Unsigned) For the SHR instruction, the OF flag is set to the most-significant
** bit of the original operand.
*/
template <typename T>
auto    BitFieldArithmetic<T>::_operatorRShiftSetFlags(BitFieldArithmetic<T> const &new_this,
                                                       unsigned int shift,
                                                       t_bitFieldEflags &flags) const -> void
{
  this->_operatorShiftSetFlags(new_this, shift, flags, true /* bool right_shift */);
}

template <typename T>
auto    BitFieldArithmetic<T>::_operatorRShift_native(unsigned int shift) -> void
{
  enum bitField::e_bitState    signBitValue;
  unsigned int            lshift_val;
  unsigned int            halfTypeBitNb;
  typename std::make_unsigned<T>::type    *umem;

  __print_debug("");
  assert(shift <= (sizeof(T) == 8 ? 63 : 31));
  signBitValue = this->getSignBit();
  shift = std::min(shift, this->getEmulatedBitNb());
  umem = reinterpret_cast<decltype(umem)>(this->getMem());
  halfTypeBitNb = this->getEmulatedBitNb() / 2;
  lshift_val = halfTypeBitNb - shift;
  umem[0] >>= shift;
  umem[0] >>= shift;
  if (lshift_val < halfTypeBitNb)
    umem[0] |= (umem[1] << lshift_val) << lshift_val;
  else
  {
    lshift_val = shift - halfTypeBitNb;
    umem[0] |= (umem[1] >> lshift_val) >> lshift_val;
  }
  umem[1] >>= shift;
  umem[1] >>= shift;
  if (std::is_signed<T>::value) // Insert sign bits for rshift
  {
    if (shift < this->getEmulatedBitNb())
      this->setRangeTo(this->getEmulatedBitNb() - shift, shift, signBitValue);
    else // Shift greater than type size but below 5/6 bits mask
      this->setAllTo(signBitValue);
  }
}
