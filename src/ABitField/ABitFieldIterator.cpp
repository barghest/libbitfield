// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//
/// @brief Implementation of the ABitField::Iterator and ABitFieldConstIterator
///

#include <cassert>
#include <stdexcept>
#include "bitField/ABitField/ABitFieldIterator.hh"

namespace bitField
{
  namespace ABitField
  {

    ABitFieldIterator::ABitFieldIterator(void *mem, unsigned int realSize)
      : _beginBit(0), _endBit(realSize * 4), _currentBit(0)
    {
      if (realSize == 0)
        throw std::logic_error("ABitFieldIterator::ABitFieldIterator: Invalid size specified");
      _bitfield = static_cast<decltype(_bitfield)>(mem);
    }

    auto    ABitFieldIterator::front(void) const -> ABitFieldIterator
    {
      return (ABitFieldIterator(_bitfield, _endBit));
    }

    auto    ABitFieldIterator::first(void) const -> bitField::e_bitState
    {
      return ((bitField::e_bitState)_bitfield[0].s_byte.b0);
    }

    auto    ABitFieldIterator::last(void) const -> bitField::e_bitState
    {
      return ((bitField::e_bitState)_bitfield[((_endBit - 1) / 4)].s_byte.b3);
    }

    auto    ABitFieldIterator::get(void) const -> bitField::e_bitState
    {
      if (_currentBit >= _endBit)
        throw std::out_of_range("ABitFieldIterator::set()");
      return ((bitField::e_bitState)_bitfield[_currentBit / 4].getBit(_currentBit % 4));
    }

    auto    ABitFieldIterator::set(bitField::e_bitState state) -> void
    {
      if (_currentBit >= _endBit)
        throw std::out_of_range("ABitFieldIterator::set()");
      return (_bitfield[_currentBit / 4].setBit(_currentBit % 4, state));
    }

    auto    ABitFieldIterator::operator+(unsigned int bitNb) -> ABitFieldIterator &
    {
      if (_currentBit + bitNb > _endBit)
        throw std::out_of_range("ABitFieldIterator::operator+()");
      _currentBit += bitNb;
      return (*this);
    }

    auto    ABitFieldIterator::operator-(unsigned int bitNb) -> ABitFieldIterator &
    {
      if (bitNb > _currentBit)
        throw std::out_of_range("ABitFieldIterator::operator-()");
      _currentBit -= bitNb;
      return (*this);
    }

    auto    ABitFieldIterator::operator++(void) -> ABitFieldIterator &
    {
      return (operator+(1));
    }
    auto    ABitFieldIterator::operator++(int nb) -> ABitFieldIterator &
    {
      (void)nb;
      return (operator+(1));
    }

    auto    ABitFieldIterator::operator--(void) -> ABitFieldIterator &
    {
      return (operator-(1));
    }
    auto    ABitFieldIterator::operator--(int nb) -> ABitFieldIterator &
    {
      (void)nb;
      return (operator-(1));
    }

    auto    ABitFieldIterator::operator==(ABitFieldIterator const &other) const -> bool
    {
      size_t    memPtrThis;
      size_t    memPtrOther;

      memPtrThis = (size_t)_bitfield + _currentBit;
      memPtrOther = (size_t)other._bitfield + other._currentBit;
      return (memPtrThis == memPtrOther);
    }

    auto    ABitFieldIterator::operator!=(ABitFieldIterator const &other) const -> bool
    {
      return (!this->operator==(other));
    }

  } // !namespace ABitField
} // !namespace bitField
