// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//
/// @brief ABitField of bit implementation
///

#include <cstring>    // std::memset
#include <cassert>    // assert
#include <sstream>    // std::stringstream
#include <iostream>   // std::cout // etc
#include <iomanip>    // std::setfill
#include <stdexcept>  // Exception

#include "bitField/bitFieldLog.hh"
#include "bitField/ABitField/ABitField.hh"
#include "bitField/ABitField/ABitFieldIterator.hh"

namespace bitField
{
  namespace ABitField
  {

    template <class sizePolicies>
    ABitField<sizePolicies>::ABitField()
    {
      bitField::initLoggingFilter(); /* TODO: remove after finishing logging */
      BOOST_LOG_TRIVIAL(debug) << "ABitField<" << policyName() << ">::" << __func__ << "(void)";
      this->_memory = nullptr;
    }

    template <class sizePolicies>
    ABitField<sizePolicies>::~ABitField()
    {
      BOOST_LOG_TRIVIAL(debug) << "ABitField<" << policyName() << ">::" << __func__ << "(void)";
    }

    template <class sizePolicies>
    auto    ABitField<sizePolicies>::getBitNb(void) const -> uint32_t {return (this->size() * 8);}
    template <class sizePolicies>
    auto    ABitField<sizePolicies>::getEmulatedBitNb(void) const -> uint32_t {return (this->emulatedSize() * 8);}

/******************************/

    template <class sizePolicies>
    auto    ABitField<sizePolicies>::begin(void) const -> typename ABitField<sizePolicies>::iterator
    {
      BOOST_LOG_TRIVIAL(debug) << "ABitField<" << policyName() << ">::" << __func__ << "(void)";
      ABitField<sizePolicies>::iterator    it(this->getMem(), this->size());

      return (it);
    }

    template <class sizePolicies>
    auto    ABitField<sizePolicies>::end(void) const -> typename ABitField<sizePolicies>::iterator
    {
      BOOST_LOG_TRIVIAL(debug) << "ABitField<" << policyName() << ">::" << __func__ << "(void)";
      void            *endPtr;

      endPtr = this->getStructMem();
      endPtr = (void *)((size_t)endPtr + this->size());
      return (ABitField<sizePolicies>::iterator(endPtr, 0));
    }

/******************************/

    template <class sizePolicies>
    auto    ABitField<sizePolicies>::_getBitOffset(uint32_t emulatedBitNb,
                                                   uint32_t &byte, uint32_t &bit,
                                                   uint32_t &bit_state_offset) const -> void
    {
      byte = emulatedBitNb / 4; /* 4 emulated bit per real byte */
      bit = emulatedBitNb % 4;
      bit_state_offset = bit * 2; /* 0/2/4/6/... */
    }

/******************************/

    template <class sizePolicies>
    auto    ABitField<sizePolicies>::get(uint32_t emulatedBitNb) const -> enum bitField::e_bitState
    {
      BOOST_LOG_TRIVIAL(debug) << "ABitField<" << policyName() << ">::" << __func__ << "(" << emulatedBitNb << ")";
      uint32_t    byte;
      uint32_t    bit;
      uint32_t    bit_state_offset;
      int         state;

      state = bitField::Undefined;
      this->_getBitOffset(emulatedBitNb, byte, bit, bit_state_offset);
      if (byte < this->size())
        state = (this->_memory[byte].byte >> bit_state_offset) & 0x3;
      else /* To remove latter (force good use) */
      {
        throw std::out_of_range("ABitField<" + policyName() + ">::get("
                                + std::to_string(emulatedBitNb)
                                + "): emulatedBitNb is out of range.");
      }
      return ((enum bitField::e_bitState)state);
    }

    template <class sizePolicies>
    auto    ABitField<sizePolicies>::get(bitField::t_byte const *mem,
                                         uint32_t emulatedBitNb) const -> enum bitField::e_bitState
    {
      BOOST_LOG_TRIVIAL(debug) << "ABitField<" << policyName() << ">::" << __func__ << "(..., " << emulatedBitNb << ")";
      uint32_t    byte;
      uint32_t    bit;
      uint32_t    bit_state_offset;
      int         state;

      this->_getBitOffset(emulatedBitNb, byte, bit, bit_state_offset);
      // if (byte < this->size()) // Its up to the user to ensure mem is not out of bound
      state = (mem[byte].byte >> bit_state_offset) & 0x3;
      // else
      //   assert(0);
      return ((enum bitField::e_bitState)state);
    }

/******************************/

    template <class sizePolicies>
    auto    ABitField<sizePolicies>::getMem(uint32_t emulatedByteNb) const -> void *
    {
      BOOST_LOG_TRIVIAL(debug) << "ABitField<" << policyName() << ">::" << __func__ << "(" << emulatedByteNb << ")";
      uint32_t    realOffset;

      realOffset = emulatedByteNb * 2;
      if (this->_memory && realOffset < this->size())
        return (&this->_memory[realOffset].byte);
      else if (this->_memory && realOffset >= this->size() && this->size())
        throw std::out_of_range("ABitField<" + policyName() + ">::getMem("
                                + std::to_string(emulatedByteNb)
                                + "): emulatedByteNb is out of range."
                                + "(Max == " + std::to_string(this->emulatedSize() - 1) + ")");
      else if (this->_memory == NULL && this->size() == 0)
        throw std::out_of_range("ABitField<" + policyName() + ">::getMem(...): "
                                "no memory binded to this class, use setMem().");
      else if (this->size())
        throw std::out_of_range("ABitField<" + policyName() + ">::getMem("
                                + std::to_string(emulatedByteNb)
                                + "): The size is non zero but the memory is null !");
      else
      {
        throw std::out_of_range("ABitField<" + policyName() + ">::getMem("
                                + std::to_string(emulatedByteNb)
                                + "): this->size() returned "
                                + std::to_string(this->size())
                                + " but the memory is not equal to nullptr");
      }
      return (nullptr);
    }

    template <class sizePolicies>
    auto    ABitField<sizePolicies>::getStructMem(void) const -> bitField::t_byte *
    {
      BOOST_LOG_TRIVIAL(debug) << "ABitField<" << policyName() << ">::" << __func__ << "(void)";
      return (this->_memory);
    }

    template <class sizePolicies>
    auto    ABitField<sizePolicies>::setMem(void *mem) -> ABitField<sizePolicies> &
    {
      BOOST_LOG_TRIVIAL(debug) << "ABitField<" << policyName() << ">::" << __func__ << "(" << mem << ")";
      this->_memory = (decltype(this->_memory))(mem);
      if (this->_memory == nullptr)
        this->setSize(0);
      return (*this);
    }

/******************************/

    template <class sizePolicies>
    auto    ABitField<sizePolicies>::setToUndefined(uint32_t emulatedBitNb) -> void {set(emulatedBitNb, bitField::Undefined);}
    template <class sizePolicies>
    auto    ABitField<sizePolicies>::setToUnknown(uint32_t emulatedBitNb) -> void {set(emulatedBitNb, bitField::Unknown);}
    template <class sizePolicies>
    auto    ABitField<sizePolicies>::setToZero(uint32_t emulatedBitNb) -> void {set(emulatedBitNb, bitField::Zero);}
    template <class sizePolicies>
    auto    ABitField<sizePolicies>::setToOne(uint32_t emulatedBitNb) -> void {set(emulatedBitNb, bitField::One);}
    template <class sizePolicies>
    auto    ABitField<sizePolicies>::set(uint32_t emulatedBitNb, bitField::e_bitState state) -> void
    {
      BOOST_LOG_TRIVIAL(debug) << "ABitField<" << policyName() << ">::" << __func__
                               << "(" << emulatedBitNb << ", " << state << ")";
      uint32_t    byte;
      uint32_t    bit;
      uint32_t    bit_state_offset;
      uint32_t    new_bit_state;
      uint32_t    reset_emulate_bit_state;

      this->_getBitOffset(emulatedBitNb, byte, bit, bit_state_offset);
      if (byte < this->size())
      {
        /* (11 11 11 00), (11 11 00 11), (11 00 11 11) or (00 11 11 11) */
        reset_emulate_bit_state = (~0 ^ (0x03 << bit_state_offset));
        new_bit_state = static_cast<uint32_t>(state) << bit_state_offset;
        this->_memory[byte].byte &= reset_emulate_bit_state;
        this->_memory[byte].byte |= new_bit_state;
      }
      else /* To remove latter (force good use) */
      {
        throw std::out_of_range("ABitField<" + policyName() + ">::set("
                                + std::to_string(emulatedBitNb)
                                + ", ...): emulatedBitNb is out of range.");
      }
    }

/******************************/
    template <class sizePolicies>
    auto    ABitField<sizePolicies>::_setUnalignedRangeTo(uint32_t emulatedBitNb,
                                                          uint32_t size,
                                                          bitField::e_bitState state) -> void
    {
      unsigned int    i;

      for (i = emulatedBitNb ; i < (emulatedBitNb + size) ; ++i)
        this->set(i, state);
    }

    template <class sizePolicies>
    void    ABitField<sizePolicies>::setRangeToUndefined(uint32_t emulatedBitNb, uint32_t size){setRangeTo(emulatedBitNb, size, bitField::Undefined);}
    template <class sizePolicies>
    void    ABitField<sizePolicies>::setRangeToUnknown(uint32_t emulatedBitNb, uint32_t size){setRangeTo(emulatedBitNb, size, bitField::Unknown);}
    template <class sizePolicies>
    void    ABitField<sizePolicies>::setRangeToZero(uint32_t emulatedBitNb, uint32_t size){setRangeTo(emulatedBitNb, size, bitField::Zero);}
    template <class sizePolicies>
    void    ABitField<sizePolicies>::setRangeToOne(uint32_t emulatedBitNb, uint32_t size){setRangeTo(emulatedBitNb, size, bitField::One);}

/*
** Internal notes:
** Do not confuse emulated memory with real memory, this->_memory
** is manage with real memory (2 bits == 1 emulated bit).
*/
    template <class sizePolicies>
    auto    ABitField<sizePolicies>::setRangeTo(uint32_t emulatedBitNb, uint32_t size,
                                                bitField::e_bitState st) -> void
    {
      BOOST_LOG_TRIVIAL(debug) << "ABitField<" << policyName() << ">::" << __func__
                               << "("<< emulatedBitNb << ", " << size << ", " << st << ")";
      uint32_t    byte;
      uint32_t    bit;
      uint32_t    bit_state_offset;
      uint32_t    aligned_bytes_nb;

      bitField::t_byte    value;
      uint32_t    beginBitMod;
      uint32_t    beginBitUnalignedNb;
      uint32_t    endBitUnalignedAt;
      uint32_t    endBitUnalignedNb;

      beginBitUnalignedNb = 0;
      if ((emulatedBitNb + size) <= this->getEmulatedBitNb())
      {
        beginBitMod = emulatedBitNb % 4;
        this->_getBitOffset(emulatedBitNb, byte, bit, bit_state_offset);
        /* Step1: Set starting unaligned bits  */
        if (beginBitMod != 0)
        {
          beginBitUnalignedNb = 4 - beginBitMod; /* [0-3] */
          this->_setUnalignedRangeTo(emulatedBitNb, beginBitUnalignedNb, st);
          byte = byte + 1;
          // std::cerr << "beginBitUnalignedNb: " << beginBitUnalignedNb << std::endl;
          size = (size > beginBitUnalignedNb ? size - beginBitUnalignedNb : 0);
        }
        if (size)
        {
          /* Step2: Mass copy */
          endBitUnalignedNb = size & 0x3;
          // std::cerr << "endBitUnalignedNb: " << endBitUnalignedNb << std::endl;
          aligned_bytes_nb = (size - endBitUnalignedNb) / 4;
          // std::cerr << "aligned_bytes_nb: " << aligned_bytes_nb << std::endl;
          value.byte = 0 | st | st << 2 | st << 4 | st << 6;
          // std::cerr << "Size type: " << this->size()
          //           << "Size write memset: " << aligned_bytes_nb << std::endl;
          std::memset(&this->_memory[byte], value.byte, aligned_bytes_nb);
          /* Step3: Set ending unaligned bits */
          endBitUnalignedAt = emulatedBitNb + beginBitUnalignedNb + (aligned_bytes_nb * 4);
          this->_setUnalignedRangeTo(endBitUnalignedAt, endBitUnalignedNb, st);
        }
      }
      else /* To remove latter (force good use) */
      {
        throw std::out_of_range("ABitField<" + policyName() + ">::setRangeTo("
                                + std::to_string(emulatedBitNb) + ", "
                                + std::to_string(size) + ", "
                                + std::to_string(st)
                                + "): is out of range. (max ==> "
                                + std::to_string(this->getEmulatedBitNb()) + ").");
      }
    }

/******************************/

    template <class sizePolicies>
    auto    ABitField<sizePolicies>::copyRange(bitField::t_byte const *mem,
                                               uint32_t dstEmulatedBit, uint32_t srcEmulatedBit,
                                               uint32_t size) -> void
    {
      BOOST_LOG_TRIVIAL(debug) << "ABitField<" << policyName() << ">::" << __func__ << "()";
      uint32_t    i;

      if (size)
      {
        // assert(this->getEmulatedBitNb() > srcEmulatedBit + (size + 1));
        if (dstEmulatedBit + size <= this->getEmulatedBitNb())
          for (i = 0 ; i < size ; i++)
            this->set(dstEmulatedBit + i, this->get(mem, srcEmulatedBit + i));
        else
          throw std::out_of_range("ABitField<" + policyName() + ">::copyRange(..., ..., "
                                  + std::to_string(srcEmulatedBit) + ", "
                                  + std::to_string(size)
                                  + ",): would result in an out of range.");
      }
    }

// template <class sizePolicies>
// int    ABitField<sizePolicies>::copyRange(ABitField<sizePolicies> const &mem,
//                  uint32_t srcEmulatedBit, uint32_t dstEmulatedBit,
//                  uint32_t size)
// {
//   return (this->copyRange(mem.getMem(), srcEmulatedBit, dstEmulatedBit, size));
// }

/******************************/

    template <class sizePolicies>
    bool    ABitField<sizePolicies>::isUndefined(uint32_t bitNb) const {return (is(bitNb, bitField::Undefined));}
    template <class sizePolicies>
    bool    ABitField<sizePolicies>::isUnknown(uint32_t bitNb) const {return (is(bitNb, bitField::Unknown));}
    template <class sizePolicies>
    bool    ABitField<sizePolicies>::isZero(uint32_t bitNb) const {return (is(bitNb, bitField::Zero));}
    template <class sizePolicies>
    bool    ABitField<sizePolicies>::isOne(uint32_t bitNb) const {return (is(bitNb, bitField::One));}
    template <class sizePolicies>
    bool    ABitField<sizePolicies>::is(uint32_t bitNb, bitField::e_bitState state) const
    {
      BOOST_LOG_TRIVIAL(debug) << "ABitField<" << policyName() << ">::" << __func__ << "(" << state << ")";
      if (this->size())
      {
        assert(this->_memory);
        return ((this->get(bitNb)) == state);
      }
      return (false);
    }

/******************************/
    template <class sizePolicies>
    void    ABitField<sizePolicies>::setAllToUndefined(){this->setAllTo(bitField::Undefined);}
    template <class sizePolicies>
    void    ABitField<sizePolicies>::setAllToUnknown(){this->setAllTo(bitField::Unknown);}
    template <class sizePolicies>
    void    ABitField<sizePolicies>::setAllToZero(){this->setAllTo(bitField::Zero);}
    template <class sizePolicies>
    void    ABitField<sizePolicies>::setAllToOne(){this->setAllTo(bitField::One);}
    template <class sizePolicies>
    void    ABitField<sizePolicies>::setAllTo(enum bitField::e_bitState state)
    {
      BOOST_LOG_TRIVIAL(debug) << "ABitField<" << policyName() << ">::" << __func__ << "(" << state << ")";
      char    value;

      if (this->size())
      {
        assert(this->_memory);
        value = 0 | state | state << 2 | state << 4 | state << 6;
        memset(this->_memory, value, this->size());
      }
    }

/******************************/

    template <class sizePolicies>
    auto    ABitField<sizePolicies>::prettyPrintMemory(bool printHeader,
                                                       bool printMiddleByteDelimiter, bool printByteDelimiter,
                                                       bool printByteOffset,
                                                       bool littleEndian,
                                                       unsigned int bytePerLine,
                                                       char middleByteDelimitedChar,
                                                       char byteDelimiter) const -> std::string
    {
      BOOST_LOG_TRIVIAL(debug) << "ABitField<" << policyName() << ">::" << __func__ << "()";
      std::stringstream    ss;
      std::string        ret;

      (void)prettyPrintMemory(ss, printHeader, printMiddleByteDelimiter, printByteDelimiter,
                              printByteOffset, littleEndian, bytePerLine, middleByteDelimitedChar,
                              byteDelimiter);

      ret = ss.str();
      return (ret);
    }

    template <class sizePolicies>
    auto    ABitField<sizePolicies>::prettyPrintMemory(std::ostream &a,
                                                       bool printHeader,
                                                       bool printMiddleByteDelimiter,
                                                       bool printByteDelimiter,
                                                       bool printByteOffset,
                                                       bool littleEndian,
                                                       unsigned int bytePerLine,
                                                       char middleByteDelimitedChar,
                                                       char byteDelimiter) const -> std::ostream &
    {
      BOOST_LOG_TRIVIAL(debug) << "ABitField<" << policyName() << ">::" << __func__ << "()";
      uint32_t    bit_nb;
      uint32_t    end_loop;
      bool        isFirstByteOnLine;
      auto        flags_backup = a.flags();
      static const char    state_pretty_print[] = {/*[bitField::Zero] = */'0',
                                                   /*[bitField::One] = */'1',
                                                   /*[bitField::Unknown] = */'?',
                                                   /*[bitField::Undefined] = */'U'};

      bit_nb = 0;
      /* Heal header printing */
      if (printHeader)
      {
        a << BITFIELD_PRETTYPRINT_SIZE_INFO_L1 << std::endl;
        a << BITFIELD_PRETTYPRINT_SIZE_INFO_L2 << this->size() << std::endl;
        a << BITFIELD_PRETTYPRINT_SIZE_INFO_L3 << this->emulatedSize() << std::endl << std::endl;
        a << BITFIELD_PRETTYPRINT_HELP_HEADER_L1 << std::endl
          << BITFIELD_PRETTYPRINT_HELP_HEADER_L2 << std::endl
          << BITFIELD_PRETTYPRINT_HELP_HEADER_L3 << std::endl << std::endl;
      }

      a.setf(std::ios_base::hex, std::ios_base::basefield);
      a.setf(std::ios_base::right);
      while ((bit_nb / 8) < this->emulatedSize()) /* Loop over all memory */
      {
        isFirstByteOnLine = (bit_nb / 8) % bytePerLine == 0;
        if (printByteOffset && isFirstByteOnLine) /* Address printing */
        {
          if (bit_nb > 0)
            a << std::endl;
          a << "0x" << std::setfill('0') << std::setw(4)
            << (bit_nb / 8) << ":\t";
        }
        else if (!isFirstByteOnLine && printByteDelimiter)
          a << byteDelimiter;
        end_loop = bit_nb + 8;
        if (littleEndian)
        {
          unsigned int    i;

          i = end_loop;
          while (i > bit_nb) /* Pretty print a single byte */
          {
            if (printMiddleByteDelimiter && end_loop - bit_nb == 4) /* mid-byte delimitor */
              a << middleByteDelimitedChar;
            a << state_pretty_print[this->get(--i)];
          }
          bit_nb = end_loop;
        }
        else
        {
          while (bit_nb < end_loop) /* Pretty print a single byte */
          {
            if (printMiddleByteDelimiter && end_loop - bit_nb == 4) /* mid-byte delimitor */
              a << middleByteDelimitedChar;
            a << state_pretty_print[this->get(bit_nb++)];
          }
        }
      }

      a.setf(flags_backup);
      return (a);
    }


/******************************/
/* Forward instanciation */

    template class ABitField <Policies::VariableSize>;
    template class ABitField <Policies::ConstExprSize<int8_t> >;
    template class ABitField <Policies::ConstExprSize<int16_t> >;
    template class ABitField <Policies::ConstExprSize<int32_t> >;
    template class ABitField <Policies::ConstExprSize<uint8_t> >;
    template class ABitField <Policies::ConstExprSize<uint16_t> >;
    template class ABitField <Policies::ConstExprSize<uint32_t> >;
# if defined(__x86_64__) || defined(__ia64__) || defined(_WIN64)
    template class ABitField <Policies::ConstExprSize<int64_t> >;
    template class ABitField <Policies::ConstExprSize<uint64_t> >;
# endif /* !64b */

  } // !namespace ABitField
} // !namespace bitField
