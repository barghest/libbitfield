// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//

#ifndef SIZE_POLICIES_HH_
# define SIZE_POLICIES_HH_

# include <type_traits>

namespace bitField
{
  namespace Policies
  {

    /*!
    ** Policies are used to customise class. They are here used for optimisation
    ** on the ABitField, BitField and BitFieldArithmetic about the size/getSize
    ** method. ABitField host shared method and the attribute (pointer on memory),
    ** these method need to known the size and emulated size in order to work.
    **
    ** BitField manage the allocation/resize of this attribute (need to have an other
    ** attribute with the size of the memory).
    ** BitFieldArithmetic manage arithmetic on a 8b/16b/32b/64b emulated type.
    **
    ** Policies have been used (instead of virtual) for 2 reason:
    **  --> class BitFieldArithmetic can be ismassively used and adding an
    **      attribute known at compile time would be not necessary while
    **      consuming non needed memory.
    **  --> Using virtual would add a VTable, a pointer (8 byte) and double the
    **      size of the class.
    ** Knowning ABitField need the size()/getSize() method of its child, which are
    ** different, and the fact that we need to use constexpr and remove useless
    ** attribute when possible, policies appears to be a better choice than virtual.
    ** However, the following are drawback of this implementation:
    ** --> The destructor of ABitField can't be virtual in order to
    **     save space for the BitFieldArithmetic class.
    ** --> Inheritage is private, this force the programmer to explicitly
    **     call the destructor of ~BitField() which delete[] the allocated memory.
    ** --> While ABitField, BitField and BitFieldArithmetic shared the same attribute,
    **     it is necessary to redeclare operator<<() each time.
    ** --> There is a need to use using::class::fct for all the ABitField method
    **     in order to allow the user to use them in a child class.
    */

    /*!
    ** This class is used by BitField in order to allow a variable lenght attribute
    ** to be managed.
    */
    class VariableSize
    {
    private:
      /*!
      ** The real size of the memory used and allocated.
      ** See ABitField and BitState for more information.
      */
      uint32_t    _size;
    public:
      /*!
      ** @return The real size of the memory used and allocated.
      */
      auto    size(void) const -> uint32_t
      {
        return (this->_size);
      }
      /*!
      ** @return The size of the memory emulated.
      */
      auto    emulatedSize(void) const -> uint32_t
      {
        return (this->_size / 2);
      }
      /*!
      ** @param size Set the real size attribute by the value contained by size.
      **             The emulated size is implicitly updated.
      */
      auto    setSize(uint32_t p_size) -> void
      {
        this->_size = p_size;
      }
      /*!
      ** @param size Set the emulated size. The real size is implicitly updated.
      */
      auto    setEmulatedSize(uint32_t p_size) -> void
      {
        this->_size = (p_size * 2);
      }

      /*!
      ** @return The name of the policies
      */
      static auto    policyName(void) -> std::string
      {
        return ("VariableSize");
      }
    }; // !class VariableSize

    /*!
    ** This class is used by BitFieldArithmetic<T> in order to fully exploit
    ** the constness of this information. This save memory and speed up the process.
    ** This also allow to allocate stack array like 'char memory[this->size()];"
    ** for memory swap, move and conversion.
    **
    ** Method setSize() and setEmulatedSize() are present for a compatibility
    ** purpose and are not supposed to be used. If it is necessary to modify the
    ** size itself is "class VariableSize" instead.
    */
    template <typename T>
    class ConstExprSize
    {
    public:
      /*!
      ** @return The real size of the memory used and allocated.
      */
      constexpr auto size(void) const -> uint32_t
      {
        return (sizeof(T) * 2);
      }
      /*!
      ** @return The emulated size of the memory.
      */
      constexpr auto    emulatedSize(void) const -> uint32_t
      {
        return (sizeof(T));
      }
      /*!
      ** This method is not supposed to be used. It is only present to compile
      ** the project and offer compatibility with "class VariableSize" policies.
      */
      auto    setSize(size_t) -> void {}
      auto    setEmulatedSize(size_t p_size) -> void;

      /*!
      ** @return The name of the policies
      */
      static auto    policyName(void) -> std::string
      {
        return ("ConstExprSize<T>");
      }

    }; // !class ConstExprSize

  } // !namespace Policies
} // !namespace bitField

#endif /* !SIZE_POLICIES_HH_ */
