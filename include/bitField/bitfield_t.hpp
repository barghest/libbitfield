// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//

#ifndef BITFIELD_T_HH_
# define BITFIELD_T_HH_

# include <array>

namespace bitField
{
  /*!
  ** A structure used to provided a type holding the size necessary to wrap
  ** a native type using a bitField. It allows the use of sizeof() and
  ** creation of container whitout knowing the size prop.
  */
  template <typename T>
  struct bitfield_t
  {
    std::array<uint8_t, (sizeof(T) * 2)>    mem;
    /*!
    ** Return a pointer to the first element of the memory array.
    ** getMem() can be used to avoid adding '&' at the beginning of already
    ** complexe lines.
    */
    auto    getMem(void) -> void * {return (&this->mem[0]);};
    auto    data(void) -> void * {return (&this->mem[0]);};

    auto    getMem(void) const -> void const * {return (&this->mem[0]);};
    auto    data(void) const -> void const * {return (&this->mem[0]);};

    auto    operator=(bitfield_t const &other) -> bitfield_t & = default;
  };

  static_assert(sizeof(bitfield_t<size_t>) == (sizeof(size_t) * 2),
                "struct bitfield_t does not respect the x2 size");

} // !namespace bitField

#endif /* !BITFIELD_T_HH_ */
