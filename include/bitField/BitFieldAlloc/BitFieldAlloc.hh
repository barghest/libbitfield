// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//
/// @brief Class Bit Field prototype
///

#ifndef BIT_FIELD_HH_
# define BIT_FIELD_HH_

# include <stdlib.h>
# include <type_traits>
# include "bitField/ABitField/ABitField.hh"
# include "bitField/Policies/SizePolicies.hpp"

namespace bitField
{
  namespace BitFieldAlloc
  {

    typedef ABitField::ABitField<Policies::VariableSize >    _bitFieldBase;

    /*!
    ** This class is an extension of the ABitField class which allow
    ** to host emulated memory and perform resize operation on it.
    */
    class BitFieldAlloc : private _bitFieldBase
    {
    public:

      using _bitFieldBase::begin;
      using _bitFieldBase::end;

      using _bitFieldBase::getBitNb;
      using _bitFieldBase::getEmulatedBitNb;
      using _bitFieldBase::get;
      using _bitFieldBase::getMem;
      using _bitFieldBase::setMem;
      using _bitFieldBase::getStructMem;

      using _bitFieldBase::set;
      using _bitFieldBase::setToUndefined;
      using _bitFieldBase::setToUnknown;
      using _bitFieldBase::setToZero;
      using _bitFieldBase::setToOne;

      using _bitFieldBase::setRangeToUndefined;
      using _bitFieldBase::setRangeToUnknown;
      using _bitFieldBase::setRangeToZero;
      using _bitFieldBase::setRangeToOne;
      using _bitFieldBase::setRangeTo;

      using _bitFieldBase::copyRange;

      using _bitFieldBase::isUndefined;
      using _bitFieldBase::isUnknown;
      using _bitFieldBase::isZero;
      using _bitFieldBase::isOne;
      using _bitFieldBase::is;

      using _bitFieldBase::setAllToUndefined;
      using _bitFieldBase::setAllToUnknown;
      using _bitFieldBase::setAllToZero;
      using _bitFieldBase::setAllToOne;
      using _bitFieldBase::setAllTo;
      using _bitFieldBase::prettyPrintMemory;
      /* Size Policies */
      using _bitFieldBase::size;
      using _bitFieldBase::emulatedSize;
      using _bitFieldBase::setSize;
      using _bitFieldBase::setEmulatedSize;

      /*!
      ** Allocated a bitField capable of emulating size byte.
      ** The memory is initialise to BitField::Undefined.
      ** @param size The number of emulated byte.
      */
      BITFIELD_EXPORT BitFieldAlloc(size_t size = 0);
      BITFIELD_EXPORT virtual ~BitFieldAlloc();

      /*!
      ** Copy constructor.
      */
      BITFIELD_EXPORT BitFieldAlloc(BitFieldAlloc const &other);
      BITFIELD_EXPORT auto    operator=(BitFieldAlloc const &other) -> BitFieldAlloc &;

      /*!
      ** Return the size in byte of the bitField (real size and emulated size)
      */
      // BITFIELD_EXPORT auto    size(void) const -> uint32_t;
      // BITFIELD_EXPORT auto    emulatedSize() const -> uint32_t;

      /*!
      ** Resize the bitField to the given number of byte.
      ** Ex: If the real size is 2 byte (1 emulated memory byte) and we use
      ** resize(2), then the real size will be 4 byte (2 emulated memory byte).
      ** The contents will be unchanged in the range from the start
      ** of the region up to the minimum of the old and new sizes.
      ** If the new size is larger than the old size, the added memory will
      ** be set to "Undefined". If resize(0) is used, the memory is freed and the
      ** size reset to 0.
      */
      BITFIELD_EXPORT auto        resize(size_t new_emulated_nb_of_byte) -> void;

    private:

      /*!
      ** The number of byte allocated with new [] in _memory is stored in
      ** the class "VariableSize" which act as a size policy given as a templated
      ** parameter on the class ABitField.
      */

    }; // end class BitFieldAlloc
  } // !namespace BitFieldAlloc
} // !namespace bitField

/*!
** Pretty print the content of the memory emulated.
*/
BITFIELD_EXPORT auto    operator<<(std::ostream &a,
                                   bitField::BitFieldAlloc::BitFieldAlloc const &other) -> std::ostream &;

#endif /* !BIT_FIELD_HH_ */
