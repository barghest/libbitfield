// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//

#ifndef BITSTATE_HH_
# define BITSTATE_HH_

#if !defined(BITFIELD_EXPORT) && defined(_MSC_VER)
#  define BITFIELD_EXPORT __declspec(dllexport)
# else
#  define BITFIELD_EXPORT
#endif

# include <algorithm>

namespace bitField
{
  /*!
  ** Define the state of a bit, the MSB (Most Significant Bit) define if the
  ** bit has a single value (only '0', or only '1').
  ** The LSB (Least Significant Bit) is used for the bit value.
  */
  enum e_bitState
  {                 /* MSB/LSB */
    Zero = 0,        /* 00 // Scalar */
    One = 1,        /* 01 // Scalar */
    Unknown = 2,    /* 10 // Composite */
    Undefined = 3,    /* 11 // Composite */
  };
  inline auto    isScalar(unsigned int st) -> bool {return ((st & 2) == 0);}
  inline auto    isComposite(unsigned int st) -> bool {return ((st & 2) == 1);}
  // TODO: test
  inline auto    merge(unsigned int st1, unsigned int st2,
                       e_bitState strategy = Unknown) -> unsigned int
  {
    st1 &= 0x3;
    st2 &= 0x3;
    if (st1 != st2)
    {
      if (st1 != Undefined && st1 != st2)
        st1 = std::max((unsigned int)strategy, st2);
    }
    return (st1);
  }

  /*!
  ** This union is used to set/get pair of bit more easly whitout having to do
  ** binary mask by hand.
  */
  typedef union u_byte
  {
    struct
    {
      unsigned char    b0:2;
      unsigned char    b1:2;
      unsigned char    b2:2;
      unsigned char    b3:2;
    } s_byte;
    unsigned char    byte;
    BITFIELD_EXPORT inline auto    getBit(int b) const -> e_bitState
    {
      unsigned char    ret;

      switch (b)
      {
        case 0: ret  = s_byte.b0; break;
        case 1: ret  = s_byte.b1; break;
        case 2: ret  = s_byte.b2; break;
        case 3: ret  = s_byte.b3; break;
        default: ret = 0; assert(0);
      };
      return ((e_bitState)ret);
    };

    BITFIELD_EXPORT inline auto    setBit(int b, e_bitState val) -> void
    {
      switch (b)
      {
        case 0: s_byte.b0 = (val & 0x3); break;
        case 1: s_byte.b1 = (val & 0x3); break;
        case 2: s_byte.b2 = (val & 0x3); break;
        case 3: s_byte.b3 = (val & 0x3); break;
        default: assert(0);
      };
    };

    /*!
    ** Apply one complement on this byte.
    ** Intern note: If the 2n bit is set, then xor 00, else xor 01.
    */
    BITFIELD_EXPORT inline auto applyOneComplement(void) -> void
    {
      /* 00 --> 01 ; 01 --> 00 ; 10 --> 10 ; 11 --> 11 */
      s_byte.b0 = (s_byte.b0 ^ !(s_byte.b0 >> static_cast<unsigned char>(1)));
      s_byte.b1 = (s_byte.b1 ^ !(s_byte.b1 >> static_cast<unsigned char>(1)));
      s_byte.b2 = (s_byte.b2 ^ !(s_byte.b2 >> static_cast<unsigned char>(1)));
      s_byte.b3 = (s_byte.b3 ^ !(s_byte.b3 >> static_cast<unsigned char>(1)));
    };
  } t_byte;

} // !namespace bitField

static_assert(std::is_pod<bitField::t_byte>::value
              && sizeof(bitField::t_byte) == 1,
              "union u_byte must be a POD of 1 byte");

#endif /* ! BITSTATE_HH_ */
