// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//
/// @brief Definition of a class Iterator on a ABitField class.
///

#ifndef ABIT_FIELD_ITERATOR_HH_
# define ABIT_FIELD_ITERATOR_HH_

# include "bitField/ABitField/ABitField.hh"

namespace bitField
{
  namespace ABitField
  {
    class ABitFieldIterator
    {
    public:
      ABitFieldIterator(void *mem, unsigned int realSize);
//  ABitFieldIterator(ABitFieldIterator const &) = default;
//  ABitFieldIterator(ABitFieldIterator &&) = default;
//  auto    operator=(ABitFieldIterator const &) -> ABitFieldIterator & = default;
//  ~ABitFieldIterator() = default;

      auto    front(void) const -> ABitFieldIterator;

      auto    first(void) const -> bitField::e_bitState;
      auto    last(void) const -> bitField::e_bitState;

      auto    get(void) const -> bitField::e_bitState;
      auto    set(bitField::e_bitState) -> void;

      auto    operator+(unsigned int bitNb) -> ABitFieldIterator &; /* Pre */
      auto    operator-(unsigned int bitNb) -> ABitFieldIterator &; /* Pre */

      auto    operator++(void) -> ABitFieldIterator &; /* Pre */
      auto    operator++(int nb) -> ABitFieldIterator &; /* Post */

      auto    operator--(void) -> ABitFieldIterator &; /* Pre */
      auto    operator--(int nb) -> ABitFieldIterator &; /* Post */

      auto    operator==(ABitFieldIterator const &other) const -> bool;
      auto    operator!=(ABitFieldIterator const &other) const -> bool;

    private:

      unsigned int const    _beginBit;
      unsigned int const    _endBit;
      unsigned int          _currentBit;
      bitField::t_byte      *_bitfield;
    };

  } // !namespace ABitField
} // !namespace bitField

#endif /* !ABIT_FIELD_ITERATOR_HH_ */
