// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//
/// @brief ABitField class definition
///

#ifndef ABIT_FIELD_HH_
# define ABIT_FIELD_HH_

# include <cassert>  // assert()
# include <cstdlib>  // std::memset()
# include <cstdint>  // uint*_t
# include <iosfwd>   // std::cout forward definition
# include <string>   // std::string
# include <array>    // std::array
# include <type_traits>

# include "bitField/BitState.hh"
# include "bitField/bitfield_t.hpp"
# include "bitField/Policies/SizePolicies.hpp"

#if !defined(BITFIELD_EXPORT) && defined(_MSC_VER)
#  define BITFIELD_EXPORT __declspec(dllexport)
# else
#  define BITFIELD_EXPORT
#endif

namespace bitField
{
  namespace ABitField
  {
    class ABitFieldIterator;

    /*!
    ** The ABitField class represent an easy to use array of bit.
    ** This allow a dev to emulate the state of each bit in a range of memory.
    ** The ABitField class distinguished 2 things:
    ** * The real size: Corresponding the memory used by the class itself
    ** * The emulated size: Corresponding to the memory emulated
    ** * * Ex: For 20 real bits, the class can emulated 10 bits.
    **
    ** However, this class is not aimed to directly perform arithmetic. The
    ** user have to create a BitArithmetic class
    ** (8,16,32 or 64 emulated bits), from where he is willing to do so.
    ** Change applied by BitArithmetic are directly done in the memory of ABitField.
    ** (often hosted by "class BitField").
    **
    ** Policies of this class correspond to how the size of the memory hosted is
    ** managed. Two are provided: VariableSize and ConstExprSize which respectively
    ** handle a memory which can be deleted, allocated and resize and a memory
    ** which, in term of size is constant. For more information about implementation
    ** details (like why we were using template policies and not virtual) can be found
    ** in ABitFieldSizePolicies.hpp.
    ** @see ABitFieldSizePolicies.hpp
    ** @see BitField
    ** @see BitFieldArithmetic<T>
    **
    ** Note: This class is not threadSafe.
    */
    template <class sizePolicies>
    class ABitField : private sizePolicies
    {
    public:
      BITFIELD_EXPORT ABitField();
      BITFIELD_EXPORT ~ABitField();

      using sizePolicies::size;
      using sizePolicies::emulatedSize;
      using sizePolicies::policyName;
      /* Only available for variableSize policies */
      using sizePolicies::setSize;
      using sizePolicies::setEmulatedSize;
      /* TODO */
      // ABitField(ABitField const &other);

      typedef ABitFieldIterator        iterator;
      // typedef ABitFieldConstIterator    const_iterator;

      /*!
      **
      */
      BITFIELD_EXPORT iterator        begin(void) const;
      BITFIELD_EXPORT iterator        end(void) const;

      // const_iterator    begin(void) const;
      // const_iterator    end(void) const;

      /*!
      ** Return the size in byte of the bitField (real size)
      */
      // BITFIELD_EXPORT virtual uint32_t        size(void) const;

      /*!
      ** Return the number of byte emulated (emulated size)
      */
      // BITFIELD_EXPORT uint32_t            emulatedSize(void) const;

      /*!
      ** Return the number of bit contained. (size * 8)
      */
      BITFIELD_EXPORT auto    getBitNb(void) const -> uint32_t;

      /*!
      ** Return the number of emulated bit contained (emulatedSize * 8)
      */
      BITFIELD_EXPORT auto    getEmulatedBitNb(void) const -> uint32_t;

      /*!
      ** @return the bitState of the bit number emulatedBitNb, starting at 0.
      ** @exception std::out_of_range If the bit is outside the memory of this.
      */
      BITFIELD_EXPORT auto    get(uint32_t emulatedBitNb) const -> bitField::e_bitState;
      /*!
      ** This method act like get(uint32_t) but does not perform any size check.
      ** @param mem The memory to return the bit state from.
      */
      BITFIELD_EXPORT auto    get(bitField::t_byte const *mem, uint32_t emulatedBitNb) const -> bitField::e_bitState;

      /*!
      ** Return a pointer to the internal memory pointing to
      ** the real byte containing the emulatedByteNb.
      ** @param emulatedByteNb The emulated bit to start at.
      ** @return A non null pointer to the memory.
      ** @exception std::out_of_range if emulatedByteNb is represente a bit out of range.
      */
      BITFIELD_EXPORT auto    getMem(uint32_t emulatedByteNb = 0) const -> void *;

      /*!
      ** Set the internal memory pointer. If memory is equal to nullptr, then
      ** the size() is reset to zero, otherwise it is up to the user to correctly
      ** set the size using setSize().
      ** @return A reference on this. This allow to chain the use (ex: obj.setMem(myPtr).set(5, bitField::Zero))
      */
      BITFIELD_EXPORT auto    setMem(void *mem) -> ABitField<sizePolicies> &;

      /*!
      ** Like BitFieldArithmetic::getMem(), but the pointer is casted onto
      ** a bitField::t_byte which represente more truthfully the memory.
      */
      BITFIELD_EXPORT auto    getStructMem(void) const -> bitField::t_byte    *;

      /*!
      ** Set the bit number emulatedBitNb with the bitState state
      ** @exception std::out_of_range if emulatedBitNb is out of range
      */
      BITFIELD_EXPORT auto    set(uint32_t emulatedBitNb, bitField::e_bitState state) -> void;

      /*!
      ** Set the bit number emulatedBitNb to a predefined value.
      ** @exception std::out_of_range if emulatedBitNb is out of range
      */
      BITFIELD_EXPORT auto    setToUndefined(uint32_t emulatedBitNb) -> void;
      BITFIELD_EXPORT auto    setToUnknown(uint32_t emulatedBitNb) -> void;
      BITFIELD_EXPORT auto    setToZero(uint32_t emulatedBitNb) -> void;
      BITFIELD_EXPORT auto    setToOne(uint32_t emulatedBitNb) -> void;

      /*!
      ** Set the range of bit starting at emulatedBitNb (included)
      ** on size bits, to a predefined or choosen value. If an out_of_bound is raised,
      ** this is kept unchanged.
      ** In intern they follow the following steps:
      ** 1/ Set by hand starting bit not aligned by 8
      ** 2/ Use a memset() like function to set all aligned bytes
      ** 3/ Set by hand ending bit not aligned by 8
      ** @exception std::out_of_range if the copy would write outside of the memory of this
      */
      BITFIELD_EXPORT auto    setRangeToUndefined(uint32_t emulatedBitNb, uint32_t size) -> void;
      BITFIELD_EXPORT auto    setRangeToUnknown(uint32_t emulatedBitNb, uint32_t size) -> void;
      BITFIELD_EXPORT auto    setRangeToZero(uint32_t emulatedBitNb, uint32_t size) -> void;
      BITFIELD_EXPORT auto    setRangeToOne(uint32_t emulatedBitNb, uint32_t size) -> void;
      BITFIELD_EXPORT auto    setRangeTo(uint32_t emulatedBitNb, uint32_t size, bitField::e_bitState) -> void;

      /*!
      ** Copy "size" bits from "mem" to "this" starting at mem[srcEmulatedBit].
      ** In case an out_of_bound occured, "this" is kept unchanged.
      ** ==> this[dst:dst+size] = mem[src:src+size];
      ** Note: There is no overlapping check between internal memory and the 'mem'
      ** parameter.
      ** Warning: While this method raise an out_of_bound if the copy is done out of the
      **          memory of "this", its up to the user to ensure that [mem, mem + size]
      **          is a valid range of memory.
      ** @param mem The bitField to copy From
      ** @param dstEmulatedBit the destination starting bit to copy to.
      ** @param srcEmulatedBit the source starting bit to copy from
      ** @param size the number of bit to copy from src to dst.
      ** @exception std::out_of_range If the copy is out of the memory hosted by "this".
      */
      BITFIELD_EXPORT auto    copyRange(bitField::t_byte const *mem, uint32_t dstEmulatedBit,
                                        uint32_t srcEmulatedBit, uint32_t size) -> void;

      /*!
      ** Getter to compare the state of a specific emulated bit
      ** with a specific state.
      ** @param emulatedBitNb the bit number state to get.
      ** @return true if bit emulatedBitNb is equal to state.
      **         false otherwise, or if this->size() is equal to zero.
      ** @exception std::out_of_range if emulatedBitNb is out of range.
      */
      BITFIELD_EXPORT auto    isUndefined(uint32_t emulatedBitNb) const -> bool;
      BITFIELD_EXPORT auto    isUnknown(uint32_t emulatedBitNb) const -> bool;
      BITFIELD_EXPORT auto    isZero(uint32_t emulatedBitNb) const -> bool;
      BITFIELD_EXPORT auto    isOne(uint32_t emulatedBitNb) const -> bool;
      BITFIELD_EXPORT auto    is(uint32_t emulatedBitNb, bitField::e_bitState) const -> bool;

      /*!
      ** Set all the emulated bit of the bitField to a specific state. If this->size()
      ** is equal to zero, nothing is done.
      */
      BITFIELD_EXPORT auto    setAllToUndefined(void) -> void;
      BITFIELD_EXPORT auto    setAllToUnknown(void) -> void;
      BITFIELD_EXPORT auto    setAllToZero(void) -> void;
      BITFIELD_EXPORT auto    setAllToOne(void) -> void;
      BITFIELD_EXPORT auto    setAllTo(bitField::e_bitState) -> void;

//TODO: Faire des operators de comparaisons d'un bit X a un bit Y
//TODO: Faire des tests unitaire
      /*!
      ** Pretty print the memory hosted by this class and apply preference.
      ** @param a the stream to print the information on.
      ** @param printHeader If true, print a header with a legend and memory size (real and emulated).
      ** @param printMiddleByteDelimiter If true, print a delimiter in the middle of a byte.
      **        Ex: (true) --> "0101-0111 0000-0001" (false) --> "01010111 00000001"
      ** @param middleByteDelimitedChar Set the middle byte delimited.
      ** @param printByteOffset If true, print the offset of the byte at the beginning of the line.
      **        Ex: (true) --> "0x0000: 0101-0111 0000-0001" (false) --> "0101-0111 0000-0001"
      ** @param bytePerLine The number of byte to print per line.
      **        Ex: (1) --> 0x0000: 0101-0111
      **                    0x0001: 0000-0001
      **            (2) --> 0x0000: 0101-0111 0000-0001
      ** @return The output stream given in parameter.
      */
      BITFIELD_EXPORT auto    prettyPrintMemory(std::ostream &a,
                                                bool printHeader = true,
                                                bool printMiddleByteDelimiter = true, bool printByteDelimiter = true,
                                                bool printByteOffset = true,
                                                bool littleEndian = true,
                                                unsigned int bytePerLine = sizeof(size_t),
                                                char middleByteDelimitedChar = '-',
                                                char byteDelimiter = ' ') const -> std::ostream &;

      /*!
      ** Same function as prettyPrintMemory(std::ostream &a, ...), but return a string containing the output
      ** instead of writing it directly to a std::ostream.
      */
      BITFIELD_EXPORT auto    prettyPrintMemory(bool printHeader = true,
                                                bool printMiddleByteDelimiter = true, bool printByteDelimiter = true,
                                                bool printByteOffset = true,
                                                bool littleEndian = true,
                                                unsigned int bytePerLine = sizeof(size_t),
                                                char middleByteDelimitedChar = '-',
                                                char byteDelimiter = ' ') const -> std::string;

    protected:
      /*!
      ** Fill byte, bit and bit_state_offset with corresponding information.
      ** @param emulatedBitNb The bit place in the emulated memory.
      ** @param byte The byte number of the variable in real memory.
      ** @param bit  The emulated bit number in the real real memory.
      ** @param bit_state_offset the emulation bit starting bit (0,2,4 or 6)
      */
      auto    _getBitOffset(uint32_t emulatedBitNb,
                            uint32_t &byte, uint32_t &bit,
                            uint32_t &bit_state_offset) const -> void;

      /*!
      ** Set each bit between (emulatedBitNb) and (emulatedBitNb + size)
      ** to the given state. Note: Each bit are set by hand to allow later
      ** the use of memset() on the aligned segment of the memory. It's not
      ** intended to be fast.
      ** @param emulatedBitNb the bit number (emulated mode) to begin.
      ** @param size the number of bit (emulated mode) to set.
      ** @param state the state of the bit to set.
      */
      auto    _setUnalignedRangeTo(uint32_t emulatedBitNb, uint32_t size,
                                   bitField::e_bitState state) -> void;

      /*!
      ** The bit Field in memory is represented as follow:
      ** Each real bit correspond to 2 emulated bit.
      **    3   2   1   0   ==> Emulated bit (total 4 emulated bits)
      ** ([..][..][..][..]) ==> 4*2 emulation bit ==> 4 emulated bit.
      **   76  54  32  10   ==> Real bit (total 1 real byte)
      ** The different value are defined by the enum bitState.
      */
      bitField::t_byte    *_memory;
    }; // end class ABitField

  } // !namespace ABitField
} // !namespace bitField

# define BITFIELD_PRETTYPRINT_SIZE_INFO_L1 "Class general information:"
# define BITFIELD_PRETTYPRINT_SIZE_INFO_L2 "* Real     size (in byte) ==> "
# define BITFIELD_PRETTYPRINT_SIZE_INFO_L3 "* Emulated size (in byte) ==> "

# define BITFIELD_PRETTYPRINT_HELP_HEADER_L1 "0xEmulatedAddress 0(zero) 1(one) ?(zero or one) U(Undefined)"
# define BITFIELD_PRETTYPRINT_HELP_HEADER_L2 "0x....: byte 0    byte 1    byte 2    byte 3    ..."
# define BITFIELD_PRETTYPRINT_HELP_HEADER_L3 "0x....: b0b2-b4b6 b0b2-b4b6 b0b2-b4b6 b0b2-b4b6 ..."

/*!
** Pretty print the content of the memory emulated.
*/
template <typename T>
BITFIELD_EXPORT auto    operator<<(std::ostream &a, bitField::ABitField::ABitField<T> const &other) -> std::ostream &;

/*
** Declared here and not in ABitField.cpp to avoid to add 9 forward instanciation
** and because this function is pretty light. (really low compilation cost)
*/
template <class sizePolicies>
auto    operator<<(std::ostream &a,
                   bitField::ABitField::ABitField<sizePolicies> const &other) -> std::ostream &
{
  return (other.prettyPrintMemory(a, false, false, true, false));
}

#endif /* !ABIT_FIELD_HH_ */
