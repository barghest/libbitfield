// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//
/// @brief Remove dependancies on boost::log for the user of the lib
///

#ifndef BITFIELD_LOG_HH_
# define BITFIELD_LOG_HH_

// # define DEBUG 1

# include <boost/log/trivial.hpp>

namespace bitField
{
  // enum severityLevel
  // {
  //   trace,        /* Data pretty print */
  //   debug,        /* Function call + parameters details */
  //   info,        /* Diverse information */
  //   warning,        /* Something not good but ok */
  //   error,        /* Something wrong but that do not stop the program */
  //   critical,        /* Something wrong, the program will exit */
  // };

  /*!
  ** Set the filtering level for the whole bitField library.
  */
  auto    initLoggingFilter() -> void;
} // !namespace bitField

// # ifdef DEBUG /* Use boost log */
// #  define BF_LOG(severity, expr) do {BOOST_LOG_TRIVIAL(severity) << expr;} while (0);
// # else  /* Use nothing */
// #  define BF_LOG(severity, expr) do {} while (0);
// # endif /* !DEBUG */

#endif /* !BITFIELD_LOG_HH_ */
