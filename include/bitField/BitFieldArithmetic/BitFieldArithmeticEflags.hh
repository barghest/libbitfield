// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//
/// @brief Definition of the eflags management (Jcc/Eflags/Test)
///

#ifndef BITFIELD_ARITHMETIC_EFLAGS_HH_
# define BITFIELD_ARITHMETIC_EFLAGS_HH_

# include <type_traits>
# include <stdint.h>
# include "bitField/ABitField/ABitField.hh"

namespace bitField
{
  namespace BitFieldArithmetic
  {

/*!
** Manage the storage of eflags used by the BitFieldArithmetic class.
** Each flag correspond to an enum bitField::e_bitState type with the following values:
** * '0' (b00/0) --> The flag is cleared.
** * '1' (b01/1) --> The flag is set.
** * '?' (b10/2) --> The flag status is unknown (either 0 or 1) but depend on operation using initialised bits.
** * 'U' (b11/3) --> The flag status us undefined (either 0 or 1) and depend operation using undefined bits.
**
** Intel documentation on EFLAGS:
** 3.4.3.1      Status Flags
** The status flags (bits 0, 2, 4, 6, 7, and 11) of the EFLAGS register indicate the results
** of arithmetic instructions, such as the ADD, SUB, MUL, and DIV instructions. The status flag functions are:
** CF (bit 0)            Carry flag -- Set if an arithmetic operation generates a carry or a borrow out of
**                                     the most-significant bit of the result; cleared otherwise.
**                                     This flag indicates an overflow condition for unsigned-integer
**                                     arithmetic. It is also used in multiple-precision arithmetic.
** PF (bit 2)            Parity flag -- Set if the least-significant byte of the result contains an
**                                      even number of 1 bits; cleared otherwise.
** AF (bit 4)            Adjust flag -- Set if an arithmetic operation generates a carry or a borrow out
**                                      of bit 3 of the result; cleared otherwise.
**                                      This flag is used in binary-coded decimal (BCD) arithmetic.
** ZF (bit 6)            Zero flag -- Set if the result is zero; cleared otherwise.
** SF (bit 7)            Sign flag -- Set equal to the most-significant bit of the result, which is
**                                    the sign bit of a signed integer.
**                                    (0 indicates a positive value and 1 indicates a negative value.)
** OF (bit 11)           Overflow flag -- Set if the integer result is too large a positive number or
**                                        too small a negative number (excluding the sign-bit) to fit
**                                        in the destination operand; cleared otherwise. This flag indicates an
**                                        overflow condition for signed-integer (two's complement) arithmetic.
**
** Extra notes on how the CF and OF are set:
** Carry Flag
** ----------
**
** The rules for turning on the carry flag in binary/integer math are two:
**
** 1. The carry flag is set if the addition of two numbers causes a carry
**    out of the most significant (leftmost) bits added.
**
**    1111 + 0001 = 0000 (carry flag is turned on)
**
** 2. The carry (borrow) flag is also set if the subtraction of two numbers
**    requires a borrow into the most significant (leftmost) bits subtracted.
**
**    0000 - 0001 = 1111 (carry flag is turned on)
**
** Otherwise, the carry flag is turned off (zero).
** * 0111 + 0001 = 1000 (carry flag is turned off [zero])
** * 1000 - 0001 = 0111 (carry flag is turned off [zero])
**
** In unsigned arithmetic, watch the carry flag to detect errors.
** In signed arithmetic, the carry flag tells you nothing interesting.
**
** Overflow Flag
** -------------
**
** The rules for turning on the overflow flag in binary/integer math are two:
**
** 1. If the sum of two numbers with the sign bits off yields a result number
**    with the sign bit on, the "overflow" flag is turned on.
**
**    0100 + 0100 = 1000 (overflow flag is turned on)
**
** 2. If the sum of two numbers with the sign bits on yields a result number
**    with the sign bit off, the "overflow" flag is turned on.
**
**    1000 + 1000 = 0000 (overflow flag is turned on)
**
** Otherwise, the overflow flag is turned off.
** * 0100 + 0001 = 0101 (overflow flag is turned off)
** * 0110 + 1001 = 1111 (overflow flag is turned off)
** * 1000 + 0001 = 1001 (overflow flag is turned off)
** * 1100 + 1100 = 1000 (overflow flag is turned off)
**
*/
    typedef union u_bitFieldEflags
    {
      struct
      {
        /* uint8_t here is used for size restriction (instead of a bitField::e_bitState) */
        /* (the struct is align on 8bits) */
        uint8_t    CF:2; /* CF (bit 0) Carry flag */
        uint8_t    PF:2; /* PF (bit 2) Parity flag */
        uint8_t    AF:2; /* AF (bit 4) Adjust flag */
        uint8_t    ZF:2; /* ZF (bit 6) Zero flag */
        uint8_t    SF:2; /* SF (bit 7) Sign flag */
        uint8_t    OF:2; /* OF (bit 11) Overflow flag */
      } s_byte;
      uint16_t    word;

      /*!
      ** Merge two u_bitFieldEflags together using bitState priority.
      */
      inline auto    merge(union u_bitFieldEflags const &other) -> void
      {
        bitField::merge(this->s_byte.CF, other.s_byte.CF);
        bitField::merge(this->s_byte.PF, other.s_byte.PF);
        bitField::merge(this->s_byte.AF, other.s_byte.AF);
        bitField::merge(this->s_byte.ZF, other.s_byte.ZF);
        bitField::merge(this->s_byte.SF, other.s_byte.SF);
        bitField::merge(this->s_byte.OF, other.s_byte.OF);
      }

      /*!
      ** Set the value of all the flags to st.
      */
      inline auto    setAllTo(e_bitState st) -> void
      {
        this->s_byte.CF = st;
        this->s_byte.OF = st;
        this->s_byte.ZF = st;
        this->s_byte.SF = st;
        this->s_byte.AF = st;
        this->s_byte.PF = st;
      }

      /*!
      ** Set all eflags to zero
      */
      inline auto    clear(void) -> void
      {
        this->setAllTo(bitField::Zero);
      }

    } t_bitFieldEflags;
    static_assert(sizeof(t_bitFieldEflags) == sizeof(uint16_t) && std::is_pod<t_bitFieldEflags>::value,
                  "s_eflags must have a size of 2 bytes and be a POD");

    BITFIELD_EXPORT auto eflags_isFalse(bitField::e_bitState flag) -> bitField::e_bitState;
    BITFIELD_EXPORT auto eflags_isTrue(bitField::e_bitState flag) -> bitField::e_bitState;
    BITFIELD_EXPORT auto eflags_eq(bitField::e_bitState a, bitField::e_bitState b) -> bitField::e_bitState;
    BITFIELD_EXPORT auto eflags_neq(bitField::e_bitState a, bitField::e_bitState b) -> bitField::e_bitState;
    BITFIELD_EXPORT auto eflags_and(bitField::e_bitState a, bitField::e_bitState b) -> bitField::e_bitState;
    BITFIELD_EXPORT auto eflags_or(bitField::e_bitState a, bitField::e_bitState b) -> bitField::e_bitState;

/*!
** Return a bitField::bitState corresponding to the value of the branching condition.
** The name of those function are taken from the intel manual Jcc condition.
** The terms "less" and "greater" are used for comparisons of signed integers
** and the terms "above" and "below" are used for unsigned integers.
** (Ex: JA mean Jump short if above (using CF and ZF flags))
** @return A bitField::bitState value
**         bitField::Zero: The condition return always false
**          bitField::One: The condition return always true
**      bitField::Unknown: The condition, depending on the state of the
**                          bits of A and B can be true or false.
**    bitField::Undefined: Same as bitField::Unknown, but depend on bits
**                          marked as uninitialised.
*/

/***********************************************************/
/* Unsigned */

/*!
** JA Jump short if above (CF=0 and ZF=0).
*/
    BITFIELD_EXPORT auto eflags_JA(t_bitFieldEflags const &eflags) -> bitField::e_bitState;
/*!
** JB Jump short if below (CF=1).
*/
    BITFIELD_EXPORT auto eflags_JB(t_bitFieldEflags const &eflags) -> bitField::e_bitState;
/*!
** JAE Jump short if above or equal (CF=0).
*/
    BITFIELD_EXPORT auto eflags_JAE(t_bitFieldEflags const &eflags) -> bitField::e_bitState;
/*!
** JBE Jump short if below or equal (CF=1 or ZF=1).
*/
    BITFIELD_EXPORT auto eflags_JBE(t_bitFieldEflags const &eflags) -> bitField::e_bitState;

/***********************************************************/
/* Signed */

/*!
** JG Jump near if greater (ZF=0 and SF=OF).
*/
    BITFIELD_EXPORT auto eflags_JG(t_bitFieldEflags const &eflags) -> bitField::e_bitState;
/*!
** JL Jump short if less (SF != OF).
*/
    BITFIELD_EXPORT auto eflags_JL(t_bitFieldEflags const &eflags) -> bitField::e_bitState;
/*!
** JGE Jump short if greater or equal (SF=OF).
*/
    BITFIELD_EXPORT auto eflags_JGE(t_bitFieldEflags const &eflags) -> bitField::e_bitState;
/*!
** JLE Jump short if less or equal (ZF=1 or SF != OF).
*/
    BITFIELD_EXPORT auto eflags_JLE(t_bitFieldEflags const &eflags) -> bitField::e_bitState;

  } //!namespace BitFieldArithmetic
} // !namespace bitField

#endif /* !BITFIELD_ARITHMETIC_EFLAGS_HH_ */
