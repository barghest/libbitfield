// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//
/// @brief Definition of the class BitFieldArithmetic
///

#ifndef BITFIELD_ARITHMETIC_HH_
# define BITFIELD_ARITHMETIC_HH_

# include <stdlib.h>    // size_t
# include <stdint.h>    // int32 / uint32 / etc
# include <stdexcept>    // exception
# include <vector>    // vector
# include <type_traits>    // std::is_unsigned<> / etc

# include "bitField/ABitField/ABitField.hh"
# include "bitField/BitFieldArithmetic/BitFieldArithmeticEflags.hh"

// # define DEBUG
# ifdef DEBUG
#  define __print_debug(x) (std::cout << __FILE__ << ":" << __LINE__ << " -> " \
                            << __PRETTY_FUNCTION__ << ": " << (x) << std::endl)
# else
#  define __print_debug(x)
# endif /* !DEBUG */

namespace bitField
{
  namespace BitFieldArithmetic
  {

    /*!
    ** The class BitFieldArithmetic is a wrapper on a type <T> stored in memory.
    ** Unlike BitField, it does perform any memory management (allocation/resize/etc),
    ** however it is mostly using in conjonction with BitField.
    ** Exemple: If the programme analysed declare an integer, and split this integer
    ** in 4 uint8_t (RGBA), we will have 1 BitField class hosting 4 emulated bytes,
    ** and 4 BitFieldArithmetic<uint8_t> each performing arithmetic on those uint8_t.
    **
    ** The policy ConstExprSize<T> allow the class to use constexpr and reduce by 2
    ** memory usage by removing a "size" attribute and the need to use virtual in
    ** the base class ABitField. See ABitFieldSizePolicies.hpp for more details.
    **
    ** Note2: This class is working with little endian type (big endian not supported yet)
    ** Note3: This class perform only arithmetic on scalar binary type (8b/16b/32b/64b),
    **         packed BCD and unpacked BCD arithmetic are not supported.
    ** TODO: optimise arithmetic using fast algorithm for number composed only with 0 and 1 (and not ? and U)
    **       multiplication: (booth (signed) / russian peason / (unsigned) Systolic Algorithm / Toom 3-Way)
    **       --> Use native operation directly if no '?' or 'U' !
    ** TODO: gerer l'overflow/underflow/les flags
    ** TODO: Ajouter la documentation
    ** TODO: Faire les tests avec BarghestNativeType sur les valeurs en dur
    ** TODO: si tout est en undefined, juste set les flags.
    ** TODO: OPTI: si moins de N valeurs dans l'intervalle, faire tout en dur et merge
    **
    */
    template <typename T>
    class BitFieldArithmetic : private ABitField::ABitField<Policies::ConstExprSize<T> >
    {
      /*
      ** Allow the use of sign qualifier cast (sign <-> unsigned) for arithmetic
      ** and optimisation.
      */
      friend class BitFieldArithmetic<typename std::make_signed<T>::type>;
      friend class BitFieldArithmetic<typename std::make_unsigned<T>::type>;

      static_assert(std::is_arithmetic<T>::value, "T must be an arithmetic type");
      static_assert(std::is_same<T, int8_t>::value || std::is_same<T, uint8_t>::value
                    || std::is_same<T, int16_t>::value || std::is_same<T, uint16_t>::value
                    || std::is_same<T, int32_t>::value || std::is_same<T, uint32_t>::value
                    || std::is_same<T, int64_t>::value || std::is_same<T, uint64_t>::value
                    , "T type is not supported");
    public:
      typedef ABitField::ABitField<Policies::ConstExprSize<T> >    _bitFieldArithmeticBase;
      using _bitFieldArithmeticBase::begin;
      using _bitFieldArithmeticBase::end;

      using _bitFieldArithmeticBase::getBitNb;
      using _bitFieldArithmeticBase::getEmulatedBitNb;
      using _bitFieldArithmeticBase::get;
      using _bitFieldArithmeticBase::getMem;
      using _bitFieldArithmeticBase::setMem;
      using _bitFieldArithmeticBase::getStructMem;

      using _bitFieldArithmeticBase::set;
      using _bitFieldArithmeticBase::setToUndefined;
      using _bitFieldArithmeticBase::setToUnknown;
      using _bitFieldArithmeticBase::setToZero;
      using _bitFieldArithmeticBase::setToOne;

      using _bitFieldArithmeticBase::setRangeToUndefined;
      using _bitFieldArithmeticBase::setRangeToUnknown;
      using _bitFieldArithmeticBase::setRangeToZero;
      using _bitFieldArithmeticBase::setRangeToOne;
      using _bitFieldArithmeticBase::setRangeTo;

      using _bitFieldArithmeticBase::copyRange;

      using _bitFieldArithmeticBase::isUndefined;
      using _bitFieldArithmeticBase::isUnknown;
      using _bitFieldArithmeticBase::isZero;
      using _bitFieldArithmeticBase::isOne;
      using _bitFieldArithmeticBase::is;

      using _bitFieldArithmeticBase::setAllToUndefined;
      using _bitFieldArithmeticBase::setAllToUnknown;
      using _bitFieldArithmeticBase::setAllToZero;
      using _bitFieldArithmeticBase::setAllToOne;
      using _bitFieldArithmeticBase::setAllTo;
      using _bitFieldArithmeticBase::prettyPrintMemory;
      /* Size policy */
      using _bitFieldArithmeticBase::size;
      using _bitFieldArithmeticBase::emulatedSize;

      // Not supported yet
      // enum    endianess
      // {
      //   LittleEndian,
      //   BigEndian
      // };
      /*!
      ** Default constructor
      */
      BITFIELD_EXPORT BitFieldArithmetic(void *memory = nullptr);
      BITFIELD_EXPORT ~BitFieldArithmetic(void);

      /*!
      ** Return the size in byte of the bitField (real size)
      */
      // BITFIELD_EXPORT auto        size(void) const -> uint32_t;
      // BITFIELD_EXPORT constexpr auto    constExprSize(void) const -> uint32_t;

//  ~BitFieldArithmetic() = default;
      // BitFieldArithmetic(BitFieldArithmetic const &) = default;
      // BitFieldArithmetic(BitFieldArithmetic &&) = default;
      // BitFieldArithmetic    &operator=(BitFieldArithmetic const &) = default;

      /*!
      ** Return the minimum/maximum value allowed by the bitField.
      ** Exemple: 0100 (b3b2b1b0) --> Min = 4 (0100) / Max = 4  (0100)
      **          0?00 (b3b2b1b0) --> Min = 0 (0000) / Max = 4  (0100)
      **          1U0? (b3b2b1b0) --> Min = 8 (1000) / Max = 13 (1011)
      ** @exception std::runtime_error If the emulated memory the class is working on is NULL.
      */
      BITFIELD_EXPORT auto  getMin(void) const -> T;
      BITFIELD_EXPORT auto  getMax(void) const -> T;

      /*!
      ** Return the emulated bit position of the sign bit. ((sizeof(T) * 8) - 1)
      ** @exception std::runtime_error If the emulated memory the class is working on is NULL.
      */
      BITFIELD_EXPORT auto  getSignBitPos(void) const -> unsigned int;

      /*!
      ** Return the value of the sign bit.
      ** @exception std::runtime_error If the emulated memory the class is working on is NULL.
      */
      BITFIELD_EXPORT auto  getSignBit(void) const -> bitField::e_bitState;

      /*!
      ** @return true if the emulated type contain at least on undefined
      **         or unknown bit, false otherwise. (correspond to multiple values)
      ** @exception std::runtime_error If the emulated memory the class is working on is NULL.
      */
      BITFIELD_EXPORT auto  isComposite(void) const -> bool;

      /*!
      ** @return true if the emulated does not contain any undefined or unknown bit,
      **         (correspond to a single value).
      ** @exception std::runtime_error If the emulated memory the class is working on is NULL.
      */
      BITFIELD_EXPORT auto  isScalar(void) const -> bool;

      /*!
      ** Check if the given value is contained.
      ** Exemple: 000? contain both value '0000' and '0001'
      ** @return BitField::Zero If the value is not contained at all.
      **         BitField::One  If "this" contain a unique value equal to val (is scalar)
      **         BitField::Unknown If its contained and depend on only Unknown bit(s)
      **         --> Ex: 0??? contain 0000 and depend on Unknown bit(s)
      **         BitField::Undefined If its contained and depend on at least one
      **                             undefined bit(s)
      **         --> Ex: 0?U? contain 0000 but depend on an Undefined one.
      ** @exception std::runtime_error If the emulated memory the class is working on is NULL.
      */
      BITFIELD_EXPORT auto  contain(T val) const -> bitField::e_bitState;

      /*!
      ** Convert a BitFieldArithmetic type into a native arithmetic type.
      ** If an error occured the reference on the result is left unchanged.
      ** Internal notes: Convert exemple 01 00 01 01 ==> 1011
      ** ('-' = Discard,'x' = packed)    -x -x -x -x ==> xxxx
      ** @param ret The native type to store the value in.
      ** @param mem A pointer to the memory containing the BitField
      ** @return -1 if an error occured, 0 otherwhise.
      **            Error list:
      **            - There are unknown or undefined bits in the BitField
      **            - mem is nullptr
      ** @exception std::runtime_error If the emulated memory the class is working on is NULL.
      */
      BITFIELD_EXPORT auto  packBits(T &ret) const -> int;

      /*!
      ** Same as packBits(), but throw an exception in case the type is composite
      ** (E.g contain multiple values)
      ** @return The scalar type contain.
      ** @exception std::runtime_error If the emulated memory the class is working on is NULL.
      */
      BITFIELD_EXPORT auto  toInt(void) const throw (std::logic_error) -> T;

      /*!
      ** Apply one complement to the type.
      ** Intern note: If the 2n bit is set, then xor 00, else xor 01.
      **              00 --> 01 ; 01 --> 00 ; 10 --> 10 ; 11 --> 11
      **              (Zero)      (One)       (Unknown)   (Undefined)
      ** @exception std::runtime_error If the emulated memory the class is working on is NULL.
      */
      BITFIELD_EXPORT auto  applyOneComplement(void) -> void;

      /*!
      ** Apply two complement to the type.
      ** @exception std::runtime_error If the emulated memory the class is working on is NULL.
      */
      BITFIELD_EXPORT auto  applyTwoComplement(void) -> void;

      /*!
      ** Include the value "val" to "this" and if there is a conflict extend to st.
      ** A conflict occure when the resulting bit is different than the original one,
      ** and does not involve any Undefined bit (where the result is alway Undefined)
      ** The 4 bit state concept allows a type using it to represent multiple
      ** values.
      **        Exemple 1: Merging -> 0000 0001 (int8_t)
      **                           -> 0000 0000 (int8_t)
      **                           ------------
      **                          ==> 0000 000?
      **
      **        Exemple 2: Merging -> 01?0 U011 (int8_t)
      **                           -> 110? ?110 (int8_t)
      **                           ------------
      **           merge with '?' ==> ?1?? U?1? (0 and 1 --> ?, ? and U --> U, etc)
      **           merge with 'U' ==> U1UU UU1U (0 and 1 --> U, ? and U --> U, etc)
      ** @param val The value to merge this with.
      ** @param st The value of the resulting bit if there is a conflit.
      ** @return Reference on "this".
      ** @exception std::runtime_error If the emulated memory the class is working on is NULL.
      */
      BITFIELD_EXPORT auto  merge(T val, bitField::e_bitState st = Unknown) -> BitFieldArithmetic<T> &;
      BITFIELD_EXPORT auto  merge(BitFieldArithmetic<T> const &val,
                                  bitField::e_bitState st = Unknown) -> BitFieldArithmetic<T> &;

      /*!
      ** Return the number of values emulated by the underlaying type/memory.
      ** Warning: In case the maximum size type is used, a value of 'zero' mean
      ** every possible values as the maximum number of element can't be stored
      ** in that type (example: A char contains all values between 0 and 255,
      ** for a total of 256 values. The number 256 can't be stored in a char).
      ** For 32b arch, its an integer size (4 bytes) and for 64b arch its a long
      ** int (size_t), (8 bytes).
      ** TODO: a tester
      ** @exception std::runtime_error If the emulated memory the class is working on is NULL.
      */
      BITFIELD_EXPORT auto  nbValues(void) const -> size_t;

      /*!
      ** Fill an array of elements of size <T> each with each
      ** possible values up to nmemb in mem. Values are stored
      ** from the lowest the the highest.
      ** @param mem The memory to stores the scalar values in.
      ** @param nb_item The number of items of mem.
      ** @return The number of element inserted into mem.
      ** Ex: For 1?0U, the following values are stored:
      **      mem[0] --> 1(0)0(0) // (00)
      **      mem[1] --> 1(0)0(1) // (01)
      **      mem[2] --> 1(1)0(0) // (10)
      **      mem[3] --> 1(1)0(1) // (11)
      ** @exception std::runtime_error If the emulated memory the class is working on is NULL.
      ** TODO: a tester
      */
      BITFIELD_EXPORT auto  getValues(T *mem, size_t nmemb) const -> size_t;
      BITFIELD_EXPORT auto  getValues(std::vector<T> &mem) const -> size_t;

      /*!
      ** Reverse all the bits. The MSB become the LSB.
      ** Exemple: 0123 4567 become 7654 3210
      ** Internal notes: This method is mainly used to avoid code redondancy
      ** with the '<<' and '>>' arithmetic operator.
      ** @return A reference on this.
      ** @exception std::runtime_error If the emulated memory the class is working on is NULL.
      */
      BITFIELD_EXPORT auto  reverse(void) -> BitFieldArithmetic<T> &;

      /*!
      ** @return Count the minimum number of leading zero of the emulated number.
      ** Ex: (int8_t)(0?01 1111) has a minimum of 1 leading zero (X101 1111)
      **             [-] (1)
      ** @exception std::runtime_error If the emulated memory the class is working on is NULL.
      */
      BITFIELD_EXPORT auto  getMinLeadingZero(void) const -> unsigned int;
      /*!
      ** @return Count the maximum number of leading zero of the emulated number.
      ** Ex: (int8_t)(0?01 1111) has a maximum of 3 leading zero (XXX1 1111)
      **             [<->] (3)
      ** @exception std::runtime_error If the emulated memory the class is working on is NULL.
      */
      BITFIELD_EXPORT auto  getMaxLeadingZero(void) const -> unsigned int;

      /*!
      ** @see ABitField<T>::copyRange(...)
      */
      BITFIELD_EXPORT auto  copyRange(BitFieldArithmetic<T> const &mem, uint32_t srcEmulatedBit,
                                      uint32_t dstEmulatedBit, uint32_t size) -> void;

      /*!
      ** Adds this and src and then stores the result in this. (this = this + src)
      ** If an overlap is present between this and src, (ex: add eax, eax)
      ** a stack copy of src is performed before the operation.
      ** @return A reference on "this".
      */
      BITFIELD_EXPORT auto  operator+=(BitFieldArithmetic<T> const &src) -> BitFieldArithmetic<T> &;
      BITFIELD_EXPORT auto  operator+=(T val) -> BitFieldArithmetic<T> &;

      /*!
      ** Same as operator+=() but fill the state of the eflags register.
      ** If an overlap is present between this and src, (ex: add eax, eax)
      ** a stack copy of src is performed before the operation.
      ** Internal notes: operatorAddEqual() is just a wrapper to manage the overlap
      **                 before calling the private method _operatorAddEqual().
      ** @return A reference on "this"
      */
      BITFIELD_EXPORT auto  operatorAddEqual(BitFieldArithmetic<T> const &src,
                                             t_bitFieldEflags &flags) -> BitFieldArithmetic<T> &;
      BITFIELD_EXPORT auto  operatorAddEqual(T val, t_bitFieldEflags &flags) -> BitFieldArithmetic<T> &;

      /*!
      ** Perform a substraction between "this" and "src".
      ** For optimisation purpose, it convert 'a - b' to 'a + -b'
      ** (using the binary complement of two on "b") as it does not change
      ** the result of the operation.
      ** @return A reference on "this"
      */
      BITFIELD_EXPORT auto  operator-=(BitFieldArithmetic<T> const &src) -> BitFieldArithmetic<T> &;
      BITFIELD_EXPORT auto  operator-=(T val) -> BitFieldArithmetic<T> &;

      /*!
      ** Same as operator-=() but fill the state of the eflags register. This function does not
      ** convert 'a - b' to 'a + -b' (using the binary complement of two on "b")
      ** because they have different effects on the EFLAGS register. Instead the
      ** old school binary subtraction algorithm is used.
      ** @return A reference on "this"
      */
      BITFIELD_EXPORT auto  operatorSubEqual(BitFieldArithmetic<T> const &src,
                                             t_bitFieldEflags &flags) -> BitFieldArithmetic<T> &;
      BITFIELD_EXPORT auto  operatorSubEqual(T val, t_bitFieldEflags &flags) -> BitFieldArithmetic<T> &;

      /*!
      ** Perform a binary multiplication between "this" and "src" and store
      ** the result in this.
      ** This version of the multiplication does not keep track of eflags and high
      ** multiplication part. Use operatorMultEqual(...) for this purpose.
      ** @param this (implicit) The low part of the multiplication (AL/AX/EAX/RAX)
      ** @param multiplier The value to multiply "this" by.
      ** @return A reference on "this"
      */
      BITFIELD_EXPORT auto  operator*=(BitFieldArithmetic<T> const &multiplier) -> BitFieldArithmetic<T> &;
      BITFIELD_EXPORT auto  operator*=(T multiplier) -> BitFieldArithmetic<T> &;

      /*!
      ** Perform a binary multiplication between "this" and "val" and store
      ** the result in this. (this *= val;)
      ** The multiplication is done "intel assembly like" using 2 registers to
      ** store the result. Below a copy of the Intel manual about the multiplication.
      ** MUL r/m8  (AX <= AL * r/m8).
      ** MUL r/m16 (DX:AX <= AX * r/m16).
      ** MUL r/m32 (EDX:EAX <= EAX * r/m32).
      ** MUL r/m64 (RDX:RAX <= RAX * r/m64).
      ** @param this (implicit) The low part of the multiplication (AL/AX/EAX/RAX)
      ** @param val The value to multiply "this" by.
      ** @param high The higher part of the multiplication result (AH/DX/EDX/RDX)
      ** @param flags A representation of the Intel CPU eflags register after the multiplication.
      ** @return A reference on "this"
      */
      BITFIELD_EXPORT auto  operatorMultEqual(BitFieldArithmetic<T> const &val,
                                              t_bitFieldEflags &flags) -> BitFieldArithmetic<T> &;
      BITFIELD_EXPORT auto  operatorMultEqual(BitFieldArithmetic<T> const &val,
                                              BitFieldArithmetic<T> &high,
                                              t_bitFieldEflags &flags) -> BitFieldArithmetic<T> &;
      BITFIELD_EXPORT auto  operatorMultEqual(T val, t_bitFieldEflags &flags) -> BitFieldArithmetic<T> &;
      BITFIELD_EXPORT auto  operatorMultEqual(T val, T &high, t_bitFieldEflags &flags) -> BitFieldArithmetic<T> &;

      /*!
      ** Sign and unsigned division. (this = this / src)
      ** @return A reference on "this"
      */
      BITFIELD_EXPORT auto  operator/=(BitFieldArithmetic<T> const &divisor) -> BitFieldArithmetic<T> &;
      BITFIELD_EXPORT auto  operator/=(T divisor) -> BitFieldArithmetic<T> &;

      BITFIELD_EXPORT auto  operatorDivEqual(BitFieldArithmetic<T> const &divisor,
                                             t_bitFieldEflags &flags) -> BitFieldArithmetic<T> &;
      BITFIELD_EXPORT auto  operatorDivEqual(T divisor, t_bitFieldEflags &flags) -> BitFieldArithmetic<T> &;

      /*!
      ** Left shift "this" to the left accoring the possible value represented by src.
      ** A mask width for the shift is 5 bits (0b11111 / 0x1F) 8,16 and 32b types
      ** and 6 bits for 64b ([0-63]) (0b111111 / 0x3F) is applied, according to the
      ** Intel manual.
      ** Exemple: this <<= (val & mask); (or) this <<= (src & mask);
      **
      ** @param src The BitFieldArithmetic<T> containing the value to shift on
      ** @return A reference on "this"
      */
      BITFIELD_EXPORT auto  operator<<=(BitFieldArithmetic<T> const &src) -> BitFieldArithmetic<T> &;

      /*!
      ** Left shift "emulated Bit" number of bit of "this" to the left. The shift
      ** value is masked on 5 or 6 bits for 8,16,32 and 64b types respectively.
      ** Internal notes: As the real size of the BitField is twice the size of the
      **              emulated variable, we are using the native assembly shift
      **              if the emulated variable has an upper type with twice his size.
      **              (Ex: If we emulate an int16_t, we will shift N time an int32_t).
      **              However, if the maximum type size is used, we make it in three steps.
      **              Step1: Copy the lower half part to the upper part
      **              Step2: Shift the upper half part by (N - (half type size))
      **              Step3: Reset the lower half to 0.
      ** @param shift The number of time to shift this on the left.
      ** @return A reference on "this".
      */
      BITFIELD_EXPORT auto  operator<<=(unsigned int shift) -> BitFieldArithmetic<T> &;

      /*!
      ** Similar to operator<<=() but fill "flags" according to the operation.
      ** @param src The number of time to shift this on the left.
      ** @param flags A representation of the Intel eflags register.
      ** @return A reference on "this"
      */
      BITFIELD_EXPORT auto  operatorLShiftEqual(BitFieldArithmetic<T> const &src,
                                                t_bitFieldEflags &flags) -> BitFieldArithmetic<T> &;
      BITFIELD_EXPORT auto  operatorLShiftEqual(unsigned int shift, t_bitFieldEflags &flags) -> BitFieldArithmetic<T> &;

      /*!
      ** Extended version of the left shift. Bits moved away from "this" are moved
      ** into high. High is used as it to allow successive call to this method.
      ** Eflags are not supported by this method.
      **
      ** In C, for a uint8_t this would correspond to :
      **  uint8_t  high, low;        // Declare (low == this)
      **  uint16_t result;
      **
      **  low = some_value;            // Init with random values
      **  high = some_other_value;
      **  result = (uint16_t)low << shift;    // Shift low
      **  high <<= shift;            // Shift high
      **
      **  low = result & 0xFF;        // Get lower part
      **  high |= (result & 0xFF00) >> 8;    // Update "high" with bits from "low"
      */
      BITFIELD_EXPORT auto  operatorLShiftEqualExtented(BitFieldArithmetic<T> &high,
                                                        unsigned int shift) throw (std::invalid_argument) -> BitFieldArithmetic<T> &;
      /*!
      ** TODO: Not implemented yet.
      */
//  BitFieldArithmetic<T> operator<<(BitFieldArithmetic<T> const &src) const;
//  BitFieldArithmetic<T> operator<<(unsigned int val) const;

      /*!
      ** Right shift "emulated Bit" number of bit to the left.
      ** Exemple: this >>= val; (or) this >>= src;
      ** @return A reference on "this"
      */
      BITFIELD_EXPORT auto  operator>>=(BitFieldArithmetic<T> const &src) -> BitFieldArithmetic<T> &;

      /*!
      ** Right shift "val" number of bit to the left.
      ** @param shift The number of time to shift "this" on the right.
      ** @return A reference on "this"
      */
      BITFIELD_EXPORT auto  operator>>=(unsigned int shift) -> BitFieldArithmetic<T> &;

      /*!
      ** Similar to operator>>=() but fill "flags" according to the operation.
      ** @param shift The number of time to shift this on the right.
      ** @param flags A representation of the Intel eflags register.
      ** @return A reference on "this"
      */
      BITFIELD_EXPORT auto  operatorRShiftEqual(BitFieldArithmetic<T> const &shift,
                                                t_bitFieldEflags &flags) -> BitFieldArithmetic<T> &;
      BITFIELD_EXPORT auto  operatorRShiftEqual(unsigned int shift, t_bitFieldEflags &flags) -> BitFieldArithmetic<T> &;

      /*!
      ** TODO: not implemented yet.
      */
      BITFIELD_EXPORT auto  operator>>(BitFieldArithmetic<T> const &src) const -> BitFieldArithmetic<T>;
      BITFIELD_EXPORT auto  operator>>(unsigned int val) const -> BitFieldArithmetic<T>;

      /*!
      ** Set the value of the internal BitField of sizeof(T) to val. The overlap is
      ** correctly spported.
      ** @return A reference on this.
      */
      BITFIELD_EXPORT auto  operator=(T val) -> BitFieldArithmetic<T> &;
      BITFIELD_EXPORT auto  operator=(BitFieldArithmetic<T> const &val) -> BitFieldArithmetic<T> &;
      BITFIELD_EXPORT auto  operator=(BitFieldArithmetic<T> const *val) -> BitFieldArithmetic<T> &;

      // TODO: cast operator
      // template <typename U>
      // BITFIELD_EXPORT operator BitFieldArithmetic<U> &(void);

      /*
      ** The following operator() represent binaries operation.
      */

      /*!
      ** NOTE: This function is not implemented because BitFieldArithmetic<T>
      ** has been design to only handle arithmetic. Unlike "class BitField" it is not
      ** capable of allocating and managing memory and so is not able to return a copy
      ** of his own memory whithout leaks or an extra private attribute.
      ** Also, doing this in arithmetic as a rvalue would generate extra dynamic
      ** allocation and slow down the program a lot.
      **
      ** However the user can use "class BitField" for the allocation and
      ** then use applyOneComplement() and applyTwoComplement() with BitFieldArithmetic<T>.
      */
      // BITFIELD_EXPORT auto  operator~(void) const -> BitFieldArithmetic<T>;
      // BITFIELD_EXPORT auto  operator-(void) const -> BitFieldArithmetic<T>;

      /*!
      ** Performs a bitwise inclusive OR operation between this and src and stores
      ** the result in this.
      ** @return A reference on this.
      */
      BITFIELD_EXPORT auto  operator|=(BitFieldArithmetic<T> const &src) -> BitFieldArithmetic<T> &;
      BITFIELD_EXPORT auto  operator|=(T src) -> BitFieldArithmetic<T> &;

      /*!
      ** Same as operator|=() but fill the state of the eflags register.
      ** @return A reference on this.
      */
      BITFIELD_EXPORT auto  operatorOrEqual(BitFieldArithmetic<T> const &src,
                                            t_bitFieldEflags &flags) -> BitFieldArithmetic<T> &;
      BITFIELD_EXPORT auto  operatorOrEqual(T val, t_bitFieldEflags &flags) -> BitFieldArithmetic<T> &;

      /*!
      ** Performs a bitwise AND operation between this and src and stores the result
      ** in this.
      ** @return A reference on this.
      */
      BITFIELD_EXPORT auto  operator&=(BitFieldArithmetic<T> const &src) -> BitFieldArithmetic<T> &;
      BITFIELD_EXPORT auto  operator&=(T src) -> BitFieldArithmetic<T> &;

      /*!
      ** Same as operator&=() but fill the state of the eflags register.
      ** @return A reference on this.
      */
      BITFIELD_EXPORT auto  operatorAndEqual(BitFieldArithmetic<T> const &src,
                                             t_bitFieldEflags &flags) -> BitFieldArithmetic<T> &;
      BITFIELD_EXPORT auto  operatorAndEqual(T val, t_bitFieldEflags &flags) -> BitFieldArithmetic<T> &;

      /*!
      ** Performs a bitwise XOR operation between this and src and stores the result
      ** in this.
      ** @return A reference on this.
      */
      BITFIELD_EXPORT auto  operator^=(BitFieldArithmetic<T> const &src) -> BitFieldArithmetic<T> &;
      BITFIELD_EXPORT auto  operator^=(T src) -> BitFieldArithmetic<T> &;

      /*!
      ** Same as operator^=() but fill the state of the eflags register.
      ** @return A reference on this.
      */
      BITFIELD_EXPORT auto  operatorXorEqual(BitFieldArithmetic<T> const &src,
                                             t_bitFieldEflags &flags) -> BitFieldArithmetic<T> &;
      BITFIELD_EXPORT auto  operatorXorEqual(T val, t_bitFieldEflags &flags) -> BitFieldArithmetic<T> &;
    private:
      // BitFieldArithmetic();

      /*!
      ** Ensure that the current BitFieldArithmetic class has a valid memory pointer.
      ** @exception std::runtime_error If the memory pointer is NULL.
      */
      auto inline    _checkMemory(void) const -> void;

      /*!
      ** Set the PF, ZF and SF flags according to the result of an arithmetic operation.
      */
      auto  _set_PFZFSF_flags(t_bitFieldEflags &flags) const -> void;

      auto  _operatorDivBringNextBitDown(BitFieldArithmetic<T> &remainder,
                                         BitFieldArithmetic<T> &quotien) -> bitField::e_bitState;


      /*!
      ** A pointer to a method for the generic L/R shift.
      */
      typedef BitFieldArithmetic<T>  &(BitFieldArithmetic<T>::*_shiftFct)(
        unsigned int shift,
        t_bitFieldEflags &flags);
      /*!
      ** A pointer to a method for arithmetic method who need only SRC and eflags.
      ** (All operation except the multiplication and the division)
      */
      typedef BitFieldArithmetic<T>  &(BitFieldArithmetic<T>::*_manageOverlapFctPtr1)(
        BitFieldArithmetic<T> const &val,
        t_bitFieldEflags &flags);
      /*!
      ** A pointer to a method for arithmetic method who need SRC, low, high and eflags.
      ** Its the case for the multiplication and the division operation.
      */
      typedef BitFieldArithmetic<T>  &(BitFieldArithmetic<T>::*_manageOverlapFctPtr2)(
        BitFieldArithmetic<T> const &val,
        BitFieldArithmetic<T> &high,
        t_bitFieldEflags &flags);

      /*!
      ** Wrapper for all arithmetic function. In case src and this overlap (ex: add rax, rax),
      ** it make a full stack copy of src, else src is passed as it.
      ** @return A reference on this (return by the called function)
      */
      auto  _manageOverlap(BitFieldArithmetic<T> const &src,
                           t_bitFieldEflags &flags,
                           _manageOverlapFctPtr1 fct) -> BitFieldArithmetic<T> &;
      /*!
      ** Wrapper for all multiplication and division method.
      ** In case "val" overlap with either "this" or "high" (ex: 'mul rdx' (or) 'mul rax'),
      ** it make a full stack copy of src, else src is passed as it.
      ** While an implicite stack copy is made in case val and this are overlaping,
      ** (legit case) an exception is raised when "high" and "this" are.
      ** @exception std::invalid_argument If this and high are overlaping.
      ** @return A reference on this (return by the called function)
      */
      auto  _manageOverlap(BitFieldArithmetic<T> const &val,
                           BitFieldArithmetic<T> &high,
                           t_bitFieldEflags &flags,
                           _manageOverlapFctPtr2 fct) throw (std::invalid_argument) -> BitFieldArithmetic<T> &;

      /*!
      ** Set intel eflags for left and right arithmetic.
      ** @param this (implicit) The value of this before the operation
      ** @param new_this The result of the shift
      ** @param shift The number of times this has been shifted by
      ** @param flags A struct containing flags to fill
      ** @param shift_right true to set for a right shift, false otherwise.
      ** Internal notes:
      ** --> It is unclear in the Intel documentation if the CF flags is undefined
      ** if the count is >= the size (in bits) before, or after the truncate mask.
      ** So we assume it is undefined if the unmasked shift value is greater than
      ** or equal to the size (in bits) of the destination operand. (template <T>)
      */
      auto  _operatorShiftSetFlags(BitFieldArithmetic<T> const &new_this,
                                   unsigned int shift,
                                   t_bitFieldEflags &flags,
                                   bool shift_right) const -> void;

      /*!
      ** Generic Left/Right shift for composite type. It is used by
      ** _operatorRShiftEqual() and _operatorLShiftEqual().
      ** @param shift The number of time to shift this on the right.
      ** @param flags A representation of the Intel eflags register.
      ** @param shiftSubFct A function shifting "this" by each scalar values of 'shift' by.
      ** Ex: If composite type 'shift' contains "0, 1, 5", it will shift by 0, 1 and 5.
      ** @return A reference on "this"
      */
      auto  _operatorShiftEqual(BitFieldArithmetic<T> const &shift,
                                t_bitFieldEflags &flags,
                                _shiftFct shiftSubFct) -> BitFieldArithmetic<T> &;


      /*!
      ** Unlike operator???Equal() only checking the overlap and making a stack copy
      ** when needed, _operator???Equal() is really doing the operation.
      */
      auto  _operatorAddEqual(BitFieldArithmetic<T> const &src,
                              t_bitFieldEflags &flags) -> BitFieldArithmetic<T> &;
      auto  _operatorSubEqual(BitFieldArithmetic<T> const &src,
                              t_bitFieldEflags &flags) -> BitFieldArithmetic<T> &;
      auto  _operatorLShiftEqual(BitFieldArithmetic<T> const &src,
                                 t_bitFieldEflags &flags) -> BitFieldArithmetic<T> &;
      auto  _operatorRShiftEqual(BitFieldArithmetic<T> const &src,
                                 t_bitFieldEflags &flags) -> BitFieldArithmetic<T> &;
      auto  _operatorAndEqual(BitFieldArithmetic<T> const &src,
                              t_bitFieldEflags &flags) -> BitFieldArithmetic<T> &;
      auto  _operatorOrEqual(BitFieldArithmetic<T> const &src,
                             t_bitFieldEflags &flags) -> BitFieldArithmetic<T> &;
      auto  _operatorXorEqual(BitFieldArithmetic<T> const &src,
                              t_bitFieldEflags &flags) -> BitFieldArithmetic<T> &;

      /*!
      ** Set eflags according to the result of the operation.
      ** @param this (implicit) The value of "this" before the shift.
      ** @param new_this The value of "this" after the shift.
      ** @param shift The number of times the shift this by.
      ** @param flags A structure containing eflags.
      */
      auto  _operatorLShiftSetFlags(BitFieldArithmetic<T> const &new_this,
                                    unsigned int shift,
                                    t_bitFieldEflags &flags) const -> void;
      auto  _operatorRShiftSetFlags(BitFieldArithmetic<T> const &new_this,
                                    unsigned int shift,
                                    t_bitFieldEflags &flags) const -> void;

      auto  _operatorDivEqualInitRemainder(BitFieldArithmetic<typename std::make_unsigned<T>::type> const &dividend,
                                           BitFieldArithmetic<typename std::make_unsigned<T>::type> const &divisor,
                                           BitFieldArithmetic<typename std::make_unsigned<T>::type> &remainder) const -> int;

      /*!
      ** This method, unlike operatorMultEqual() is really performing the multiplication.
      ** operatorMultEqual() is a wrapper managing the overlap if any.
      ** @param multiplier The value to multiply this by.
      ** @param high The higher part of the multiplication result (AH/DX/EDX/RDX)
      ** @param flags A representation of the Intel CPU eflags register after the multiplication.
      ** @param is_signed A boolean used to force the signess for flags management
      **                  in order to manage borderline case (ex: (-128 * -1)).
      **                  This exemple would generate a wrong OF/CF flag value
      **                  when working with sign char. (Because we change "--" to "++"
      **                  and -128 stays -128 event after a 2b complement). In order to
      **                  fixe this, we cast -128 into the unsigned corresponding type
      **                  and force then the sign to handle the sign overflow.
      ** @return A reference on "this".
      */
      auto  _operatorMultEqual(BitFieldArithmetic<T> const &multiplier,
                               BitFieldArithmetic<T> &high,
                               t_bitFieldEflags &flags,
                               bool is_signed = std::is_signed<T>::value) -> BitFieldArithmetic<T> &;

      /*!
      ** Set eflags according to the result of the operation.
      ** @param high The high value of the multiplication after.
      ** @param flags A structure containing eflags.
      ** @param @see _operatorMultEqual
      */
      auto  _operatorMultSetFlags(BitFieldArithmetic<T> const &high,
                                  t_bitFieldEflags &flags,
                                  bool is_signed = std::is_signed<T>::value) const -> void;

      /*!
      ** Manage the partial product addition during the multiplication.
      ** If the bit of the multiplier is '0', nothing is done, if its '1', the
      ** multiplicand is past down, shifted to the left N time and then added to
      ** the value of the other products addition.
      **
      ** If the multiplier contain Unknown/Undefined bit, multiplicand
      ** is merged first with "0000000000..." using an Unknown (if ?) or
      ** Undefined (if U) strategie, and then added to the old_result.
      **
      ** @param old_result_low The low result of all the precedent products addition
      ** @param old_result_high The high result of all the precedent products addition
      ** @param src_shifted_low The low multiplicand part already shifted to the left.
      ** @param src_shifted_high The high multiplicand part already shifter to the left,
      **                         and sign extended if we are working with a signed type.
      ** @param multiplierBit The value of the current multiplier bit.
      */
      auto  __operatorMultPartialProducts(BitFieldArithmetic<T> &old_result_low,
                                          BitFieldArithmetic<T> &old_result_high,
                                          BitFieldArithmetic<T> const &src_shifted_low,
                                          BitFieldArithmetic<T> const &src_shifted_high,
                                          bitField::e_bitState multiplierBit) -> void;

      /*!
      ** Manage all the different case for the unsigned and signed multiplication.
      ** For details information, see "BitFieldArithmeticMul.i"
      ** @return A reference on "this".
      */
      auto  _operatorMultEqual_case_wrapper(BitFieldArithmetic<T> const &multiplier,
                                            BitFieldArithmetic<T> &high,
                                            t_bitFieldEflags &flags) -> BitFieldArithmetic<T> &;

      // /*!
      // ** A Wrapper to left shift a number using the native assembly shift.
      // ** The template <typename U> correspond to the upper type.
      // ** @param shift The number of time "this" has to be shifted on the left.
      // */
      // template <typename U>
      // auto  _operatorLShiftEqual_native(unsigned int shift) -> void;

      // /*!
      // ** A Wrapper to left shift a number using the native assembly shift.
      // ** @param shift The number of time the variable pointed by "mem" has
      // **              to be shifted on the left.
      // ** @param mem A pointer to the memory to be shifted.
      // */
      // template <typename U>
      // auto  _operatorLShift_native(unsigned int shift, U *mem) const -> void;

      /*!
      ** A Wrapper to right shift a number using the native assembly shift.
      ** @param shift The number of times to shift the variable of type T
      **              emulated by mem.
      ** @param mem The variable with a type twice the size of this.
      ** Exemple: If we have a (char)42, the native representation will be   "00101010"
      **                                 The emulated will be        "0000010001000100"
      ** In order to optimise the shift as much as possible, we cast the emulated
      ** representation into a (short)"0000010001000100" and shift by twice. (because
      ** there are twice more bits). If its not possible, we shift once two time.
      ** In case T is signed, we before sign extension after the shift.
      ** Conclusion: ((00101010 >> 1) is equivalent to (0000010001000100 >> 2))
      */
      auto  _operatorRShift_native(unsigned int shift) -> void;

      /*!
      ** @return A mask with Unknown and Undefined bits position.
      **         If the type is scalar, 0 is returned.
      ** Exemple: For            : (char)(0?0U 010U)
      **          It will return : (char)(0101 0001)
      */
      auto  _getCompositeBitsMask(void) const -> T;

      /*!
      ** A generic function able to return the min and max value of this BitField,
      ** it can be represented with the interval [umin, umax].
      ** @param umin The minimum value emulated.
      ** @param umax The maximum value emulated.
      */
      auto  _getBound(T &umin, T &umax) const -> void;


      /*!
      ** Return the value of the operation between op1 and op2.
      ** The value is extracted directly on an internal truth table.
      */
      inline auto _getAddVal(bitField::e_bitState,
                             bitField::e_bitState) const -> bitField::e_bitState;
      inline auto _getSubVal(bitField::e_bitState,
                             bitField::e_bitState) const -> bitField::e_bitState;

      /*!
      ** Return the value of the carry after the operation between op1 and op2
      ** The value is extracted directly on an internal truth table.
      */
      inline auto _getAddCarry(bitField::e_bitState,
                               bitField::e_bitState) const -> bitField::e_bitState;
      inline auto _getSubCarry(bitField::e_bitState,
                               bitField::e_bitState) const -> bitField::e_bitState;

      /*!
      ** Spread the carry of a single bit substraction on upper bits.
      ** It also set the CF and OF flags according to the result of the operation.
      ** @param byte The real byte containing the bit.
      ** @param bit The emulated bit on the real byte.
      ** @param carry The value of the carry to spread on the next bit.
      ** @param flags A reference on the eflags register.
      */
      auto  _operatorSubSpreadCarry(unsigned int byte,
                                    unsigned int bit,
                                    bitField::e_bitState carry,
                                    t_bitFieldEflags &flags) -> void;
      auto  _operatorSubRecur(BitFieldArithmetic<T> const &src,
                              unsigned int byte, unsigned int bit,
                              t_bitFieldEflags &flags) -> void;
    };

/*!
** This class does not need to be a POD but need to use no more
** memory than the internal private pointer (this->_memory).
** (as this class is really widely used and this would cost memory).
** This static_assert() ensure this property.
*/
    static_assert(sizeof(BitFieldArithmetic<int8_t>) == sizeof(void *) // Size of the stored pointer (32b/64b)
                  && sizeof(BitFieldArithmetic<int8_t>) == sizeof(BitFieldArithmetic<uint8_t>)
                  && sizeof(BitFieldArithmetic<int16_t>) == sizeof(BitFieldArithmetic<uint16_t>)
                  && sizeof(BitFieldArithmetic<int32_t>) == sizeof(BitFieldArithmetic<uint32_t>)
#if defined(__x86_64__) || defined(__ia64__) || defined(_WIN64)
                  && sizeof(BitFieldArithmetic<int64_t>) == sizeof(BitFieldArithmetic<uint64_t>)
#endif /* !64b */
                  ,
                  "BitFieldArithmetic should not have a VTable because it is massively used,"
                  " and this would increase memory usage. Instead template policies are used."
                  " This class can only use 4/8 bytes of memory (for 32b or 64b system)");
/***************************************************/

/*!
** Check if two BitFieldArithmetic have overlaping memory.
** @return true if there is an overlap, false otherwise.
*/
    template <typename T>
    BITFIELD_EXPORT auto inline    overlap(BitFieldArithmetic<T> const &a,
                                           BitFieldArithmetic<T> const &b) -> bool
    {
      uintptr_t    ptr1;
      uintptr_t    ptr2;
      uintptr_t    abs;
      uintptr_t    size;

      ptr1 = reinterpret_cast<uintptr_t>(a.getMem());
      ptr2 = reinterpret_cast<uintptr_t>(b.getMem());
      size = a.size();
      abs = (ptr1 > ptr2 ? (ptr1 - ptr2) : (ptr2 - ptr1));
      return (abs < size);
    }

    template <typename T>
    BITFIELD_EXPORT auto    operator<(BitFieldArithmetic<T> const &a,
                                      BitFieldArithmetic<T> const &b) -> bitField::e_bitState
    {
      uint8_t               _mem[a.size()];
      BitFieldArithmetic<T> subResult(&_mem[0]);
      t_bitFieldEflags      eflags;

      subResult = a;
      subResult.operatorSubEqual(b, eflags);
      if (std::is_unsigned<T>::value)
        return (eflags_JB(eflags));
      else
        return (eflags_JL(eflags));
    }

    template <typename T>
    BITFIELD_EXPORT auto    operator<=(BitFieldArithmetic<T> const &a,
                                       BitFieldArithmetic<T> const &b) -> bitField::e_bitState
    {
      uint8_t               _mem[a.size()];
      BitFieldArithmetic<T> subResult(&_mem[0]);
      t_bitFieldEflags      eflags;

      subResult = a;
      subResult.operatorSubEqual(b, eflags);
      if (std::is_unsigned<T>::value)
        return (eflags_JBE(eflags));
      else
        return (eflags_JLE(eflags));
    }

    template <typename T>
    BITFIELD_EXPORT auto    operator>(BitFieldArithmetic<T> const &a,
                                      BitFieldArithmetic<T> const &b) ->  bitField::e_bitState
    {
      uint8_t               _mem[a.size()];
      BitFieldArithmetic<T> subResult(&_mem[0]);
      t_bitFieldEflags      eflags;

      subResult = a;
      subResult.operatorSubEqual(b, eflags);
      if (std::is_unsigned<T>::value)
        return (eflags_JA(eflags));
      else /* (ZF=0 and SF=OF) */
        return (eflags_JG(eflags));
    }

    template <typename T>
    BITFIELD_EXPORT auto    operator>=(BitFieldArithmetic<T> const &a,
                                       BitFieldArithmetic<T> const &b) -> bitField::e_bitState
    {
      uint8_t               _mem[a.size()];
      BitFieldArithmetic<T> subResult(&_mem[0]);
      t_bitFieldEflags      eflags;

      subResult = a;
      subResult.operatorSubEqual(b, eflags);
      if (std::is_unsigned<T>::value)
        return (eflags_JAE(eflags));
      else
        return (eflags_JGE(eflags));
    }

/*
** Declared here and not in BitFieldArithmetic.cpp to avoid to add 8 forward instanciation
** and because this function is pretty light. (really low compilation cost)
*/
    template <typename T>
    BITFIELD_EXPORT auto    operator<<(std::ostream &a,
                                       bitField::BitFieldArithmetic::BitFieldArithmetic<T> const &other) -> std::ostream &
    {
      return (other.prettyPrintMemory(a, false, false, true, false, true));
    }

  } // !BitFieldArithmetic
} // !bitfield

/*
** TODO: ajouter l'operator de comparaison "=="
*/

#endif /* !BITFIELD_ARITHMETIC_HH_ */
