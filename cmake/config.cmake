## Copyright (c) 2016, Jean-Baptiste Laurent
## All rights reserved.
##
## Redistribution and use in source and binary forms, with or without
## modification, are permitted provided that the following conditions are met:
##
## 1. Redistributions of source code must retain the above copyright notice, this
##    list of conditions and the following disclaimer.
## 2. Redistributions in binary form must reproduce the above copyright notice,
##    this list of conditions and the following disclaimer in the documentation
##    and/or other materials provided with the distribution.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
## ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
## WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
## DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
## ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
## (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
## LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
## ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
## (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## The views and conclusions contained in the software and documentation are those
## of the authors and should not be interpreted as representing official policies,
## either expressed or implied, of the Barghest Project.
##
###########################################
# cmake cache configuration

set(LTO_ENABLED ON CACHE BOOL "Enable / disable link time optimization")

############################################
# C++ configuration

include_directories(include)

# C++ configuration : warnings

set(GCC_CLANG_WARNINGS "${GCC_CLANG_WARNINGS} -Wall -Wextra")
set(GCC_CLANG_WARNINGS "${GCC_CLANG_WARNINGS} -Winit-self")
set(GCC_CLANG_WARNINGS "${GCC_CLANG_WARNINGS} -Wformat=2")
set(GCC_CLANG_WARNINGS "${GCC_CLANG_WARNINGS} -Wmissing-include-dirs")
set(GCC_CLANG_WARNINGS "${GCC_CLANG_WARNINGS} -Wswitch-default")
set(GCC_CLANG_WARNINGS "${GCC_CLANG_WARNINGS} -Wswitch-enum")
set(GCC_CLANG_WARNINGS "${GCC_CLANG_WARNINGS} -Wuninitialized")
set(GCC_CLANG_WARNINGS "${GCC_CLANG_WARNINGS} -Wstrict-overflow")
set(GCC_CLANG_WARNINGS "${GCC_CLANG_WARNINGS} -Wwrite-strings")
set(GCC_CLANG_WARNINGS "${GCC_CLANG_WARNINGS} -Winvalid-pch")
set(GCC_CLANG_WARNINGS "${GCC_CLANG_WARNINGS} -Wfloat-equal")
set(GCC_CLANG_WARNINGS "${GCC_CLANG_WARNINGS} -Wconversion")
set(GCC_CLANG_WARNINGS "${GCC_CLANG_WARNINGS} -Wsign-conversion")
set(GCC_CLANG_WARNINGS "${GCC_CLANG_WARNINGS} -Wcast-align")
set(GCC_CLANG_WARNINGS "${GCC_CLANG_WARNINGS} -Wmissing-braces")
set(GCC_CLANG_WARNINGS "${GCC_CLANG_WARNINGS} -Wmissing-declarations")
set(GCC_CLANG_WARNINGS "${GCC_CLANG_WARNINGS} -Wuninitialized")
set(GCC_CLANG_WARNINGS "${GCC_CLANG_WARNINGS} -Wmissing-format-attribute")
set(GCC_CLANG_WARNINGS "${GCC_CLANG_WARNINGS} -Wredundant-decls")
set(GCC_CLANG_WARNINGS "${GCC_CLANG_WARNINGS} -Wshadow")
# set(WARNINGS "${GCC_CLANG_WARNINGS} -Winline")

# set(CLANG_WARNINGS "${CLANG_WARNINGS} -Weverything")
set(CLANG_WARNINGS "${CLANG_WARNINGS} -Wdeprecated-implementations")
set(CLANG_WARNINGS "${CLANG_WARNINGS} -Wfour-char-constants")
set(CLANG_WARNINGS "${CLANG_WARNINGS} -Wimplicit-atomic-properties")
set(CLANG_WARNINGS "${CLANG_WARNINGS} -Wmissing-noreturn")
set(CLANG_WARNINGS "${CLANG_WARNINGS} -Wnewline-eof")
set(CLANG_WARNINGS "${CLANG_WARNINGS} -Wshorten-64-to-32")
set(CLANG_WARNINGS "${CLANG_WARNINGS} -Wunreachable-code")

set(GCC_WARNINGS "${GCC_WARNINGS} -Wdouble-promotion")
set(GCC_WARNINGS "${GCC_WARNINGS} -Wvector-operation-performance")
set(GCC_WARNINGS "${GCC_WARNINGS} -Wnormalized=nfkc")
set(GCC_WARNINGS "${GCC_WARNINGS} -Wlogical-op")
set(GCC_WARNINGS "${GCC_WARNINGS} -Waddress")
set(GCC_WARNINGS "${GCC_WARNINGS} -Wundef")      
set(GCC_WARNINGS "${GCC_WARNINGS} -Wtrampolines")
set(GCC_WARNINGS "${GCC_WARNINGS} -Wzero-as-null-pointer-constant")
set(GCC_WARNINGS "${GCC_WARNINGS} -Wunsafe-loop-optimizations")

set(MSVC_WARNINGS "${MSVC_WARNINGS} /Wall")
set(MSVC_WARNINGS "${MSVC_WARNINGS} /WL")

if(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
  set(CXX_WARNINGS "${CXX_WARNINGS} ${GCC_CLANG_WARNINGS} ${GCC_WARNINGS}")
elseif(CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
  set(CXX_WARNINGS "${CXX_WARNINGS} ${GCC_CLANG_WARNINGS} ${CLANG_WARNINGS}")
elseif(CMAKE_CXX_COMPILER_ID STREQUAL "MSVC")
  set(CXX_WARNINGS "${CXX_WARNINGS} ${MSVC_WARNINGS}")
endif(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")

# C++ configuration : flags

set(GCC_CLANG_FLAGS "${GCC_CLANG_FLAGS} -std=c++11")
set(GCC_CLANG_FLAGS "${GCC_CLANG_FLAGS} -pedantic")
# set(GCC_CLANG_FLAGS "${GCC_CLANG_FLAGS} -FPIC")

# set(CLANG_FLAGS "${CLANG_FLAGS} -v")

set(MSVC_FLAGS "/O2")

if(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
  set(CXX_FLAGS "${CXX_FLAGS} ${GCC_CLANG_FLAGS} ${GCC_FLAGS}")
elseif(CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
  set(CXX_FLAGS "${CXX_FLAGS} ${GCC_CLANG_FLAGS} ${CLANG_FLAGS}")
elseif(CMAKE_CXX_COMPILER_ID STREQUAL "MSVC")
  set(CXX_FLAGS "${CXX_FLAGS} ${MSVC_FLAGS}")
endif(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${CXX_FLAGS} ${CXX_WARNINGS}")

############################################
# ASM configuration

# the cmake directive include_directories is not compatible with NASM include path search system
# so we have to give it raw to nasm
set(CMAKE_ASM_NASM_FLAGS "${CMAKE_ASM_NASM_FLAGS} -w+all -i ${CMAKE_SOURCE_DIR}/include/")
set(CMAKE_ASM_NASM_FLAGS_DEBUG "${CMAKE_ASM_NASM_FLAGS_DEBUG} -O0")
set(CMAKE_ASM_NASM_FLAGS_RELEASE "${CMAKE_ASM_NASM_FLAGS_RELEASE}")

############################################
# Linkinking configuration

if(LTO_ENABLED)
  set(GCC_CLANG_LTO "-flto")
  set(GCC_CLANG_LINK_FLAGS_RELEASE "-O3 -march=native -fno-stack-protector")
  set(MSVC_LTO "/LTCG")
endif(LTO_ENABLED)

if(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
  set(LINK_FLAGS_RELEASE "${LINK_FLAGS_RELEASE} ${GCC_CLANG_LINK_FLAGS_RELEASE}")
  set(LTO "${GCC_CLANG_LTO}")
elseif(CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
  set(LINK_FLAGS_RELEASE "${LINK_FLAGS_RELEASE} ${GCC_CLANG_LINK_FLAGS_RELEASE}")
  set(LTO "${GCC_CLANG_LTO}")
elseif(CMAKE_CXX_COMPILER_ID STREQUAL "MSVC")
  set(LTO "${MSVC_LTO}")
endif(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")

set(CMAKE_EXE_LINKER_FLAGS    "${CMAKE_EXE_LINKER_FLAGS}    ${LINK_FLAGS} ${LTO}")
set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} ${LINK_FLAGS} ${LTO}")
set(CMAKE_STATIC_LINKER_FLAGS "${CMAKE_STATIC_LINKER_FLAGS} ${LINK_FLAGS} ${LTO}")
set(CMAKE_EXE_LINKER_FLAGS_RELEASE    "{CMAKE_EXE_LINKER_FLAGS_RELEASE}    ${CMAKE_EXE_LINKER_FLAGS}    ${LINK_FLAGS_RELEASE}")
set(CMAKE_SHARED_LINKER_FLAGS_RELEASE "{CMAKE_SHARED_LINKER_FLAGS_RELEASE} ${CMAKE_SHARED_LINKER_FLAGS} ${LINK_FLAGS_RELEASE}")
set(CMAKE_STATIC_LINKER_FLAGS_RELEASE "{CMAKE_STATIC_LINKER_FLAGS_RELEASE} ${CMAKE_STATIC_LINKER_FLAGS} ${LINK_FLAGS_RELEASE}")

set(LINK_FLAGS "${LINK_FLAGS} ${LTO}")
set(LINK_FLAGS_DEBUG "${LINK_FLAGS_RELEASE} ${LINK_FLAGS}")
set(LINK_FLAGS_RELEASE "${LINK_FLAGS_RELEASE} ${LINK_FLAGS}")
